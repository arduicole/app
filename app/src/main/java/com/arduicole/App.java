package com.arduicole;

import androidx.multidex.MultiDexApplication;

import com.arduicole.background.WorkerAlertes;
import com.arduicole.net.NetTask;
import com.arduicole.parametres.Parametres;

public class App extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        Parametres.Initialiser(getApplicationContext());
        NetTask.init(getApplicationContext());
        WorkerAlertes.planifierVerification(getApplicationContext());
    }
}
