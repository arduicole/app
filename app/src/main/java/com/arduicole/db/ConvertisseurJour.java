package com.arduicole.db;

import androidx.room.TypeConverter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class ConvertisseurJour {
    @TypeConverter
    public byte[] toBytes(Jour jour) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            for (double temp : jour.temperatures)
                dos.writeDouble(temp);
            dos.flush();
        } catch (Exception e) {
            throw new UnknownError("La conversion de jour en byte[] n'a pas fonctionné");
        }
        return bos.toByteArray();
    }

    @TypeConverter
    public Jour fromBytes(byte[] bytes) {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        DataInputStream dis = new DataInputStream(bis);
        Jour jour = new Jour();
        try {
            jour.temperatures[0] = dis.readDouble();
            jour.temperatures[1] = dis.readDouble();
            jour.temperatures[2] = dis.readDouble();
            jour.temperatures[3] = dis.readDouble();
        } catch (Exception e) {
            throw new UnknownError("Le byte[] n'a pas pu être reconverti en Jour");
        }
        return jour;
    }
}
