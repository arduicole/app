package com.arduicole.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(
        tableName = "consigne",
        indices = {@Index(value = {"nom"}, unique = true)}
)
@TypeConverters({ConvertisseurJour.class, ConvertisseurHeuresConsigne.class})
public class Consigne {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "nom")
    @NonNull
    public String Nom = "";

    @Expose                                    // Gson
    @SerializedName("ensoleille")              // Gson
    @ColumnInfo(name = "soleil")               // Room
    @NonNull                                   // Room
    public Jour Soleil = new Jour();

    @Expose                                    // Gson
    @SerializedName("mi-nuageux")              // Gson
    @ColumnInfo(name = "mi_nuageux")           // Room
    @NonNull                                   // Room
    public Jour MiNuageux = new Jour();

    @Expose                                    // Gson
    @SerializedName("nuageux")                 // Gson
    @ColumnInfo(name = "nuageux")              // Room
    @NonNull                                   // Room
    public Jour Nuageux = new Jour();

    @Expose                                    // Gson
    @SerializedName("heures")                  // Gson
    @ColumnInfo(name = "heures")               // Room
    @NonNull                                   // Room
    public HeuresConsigne Heures = new HeuresConsigne();

    @Expose                                    // Gson
    @SerializedName("variation-critique")      // Gson
    @ColumnInfo(name = "variation_critique")   // Room
    public double VariationCritique = 0.0;

    @Expose                                    // Gson
    @SerializedName("hysteresis")              // Gson
    @ColumnInfo(name = "variation_acceptable") // Room
    public double VariationAcceptable = 0.0;

    public Consigne() {

    }

    public Consigne(String nom) {
        Nom = nom;
    }

    public Jour[] toArray() {
        return new Jour[]{Soleil, MiNuageux, Nuageux};
    }

    /**
     * Retourne la température de la consigne pour l'ensoleillement donné et
     * l'heure actuelle
     */
    public double temperatureActuelle(TypeJour tj) {
        return toArray()[tj.getValue()].temperatures[Heures.periode()];
    }
}
