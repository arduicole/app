package com.arduicole.db;

/**
 * Représente le mode d'opération d'une serre
 */
public enum Mode {
    Manuel(0),
    Calibration(1),
    ControleDistance(2),
    Cycle(3),
    Asservissement(4);
    public static final String[] MODES = {
            "manuel",
            "calibration",
            "controle-distance",
            "cycle",
            "asservissement",
    };

    private int value;

    public int getValue() {
        return value;
    }

    public static Mode fromInt(int value) {
        return Mode.values()[value];
    }

    Mode(int value) {
        this.value = value;
    }
}
