package com.arduicole.db;

import android.os.AsyncTask;

public abstract class DBTask<Param, Dao, Resultat> extends AsyncTask<Param, Void, Resultat> {
    protected Dao mDao;

    protected DBTask(Dao dao) {
        mDao = dao;
    }

    protected Resultat doInBackground(Param... params) {
        Resultat resultat = null;
        try {
            for (Param param : params)
                resultat = tache(param);
        } catch (Exception e) {
            resultat = erreur(e);
        }
        return resultat;
    }

    protected Resultat erreur(Exception e) {
        return null;
    }

    protected abstract Resultat tache(Param param);
}
