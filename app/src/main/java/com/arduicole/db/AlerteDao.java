package com.arduicole.db;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import java.util.List;

/**
 * Ensemble de méthodes utilisées pour interagir avec les alertes dans la base de données
 */
@Dao
@TypeConverters({ConvertisseurNiveauUrgence.class})
public abstract class AlerteDao {
    /**
     * @return La liste de toutes les alertes pour une serre donnée
     */
    @Query("SELECT * FROM alerte WHERE serre=:serre ORDER BY date DESC")
    public abstract LiveData<List<Alerte>> alertesSerre(int serre);

    /**
     * @return La liste des alertes qui n'ont pas été consultées par l'utilisateur
     */
    @Query("SELECT * FROM alerte WHERE consultee=0")
    public abstract List<Alerte> alertesNonConsultees();

    /**
     * @return S'il y a au moins une alerte non consultée pour la serre donnée
     */
    @Query("SELECT count(*) FROM alerte WHERE consultee=0 AND serre=:serre LIMIT 1")
    public abstract LiveData<Boolean> aAlerteNonConsultee(int serre);


    /**
     * @return La liste d'alertes non consultées pour toutes les serres
     */
    @Query("SELECT * FROM alerte WHERE consultee=0 AND urgence=2 ORDER BY date DESC")
    public abstract LiveData<List<Alerte>> alertesUrgentes();

    /**
     * @return L'alerte avec le uid donné ou nul
     */
    @Query("SELECT * FROM alerte WHERE uid=:uid")
    public abstract Alerte uid(int uid);

    /**
     * @return Si une alerte correspond aux caractéristiques données
     */
    @Query("SELECT count(*) FROM alerte WHERE alerte.titre=:titre " +
            "AND alerte.message=:message AND alerte.serre=:idSerre " +
            "AND alerte.date=:date AND alerte.urgence=:urgence " +
            "LIMIT 1")
    public abstract boolean alerteCorrespondante(String titre, String message,
                                                 int idSerre, long date,
                                                 Alerte.NiveauUrgence urgence);

    /**
     * @return Si une alerte correspond à celle donnée (ceci ignore l'ID de l'alerte)
     */
    public boolean alerteCorrespondante(@NonNull Alerte alerte) {
        return alerteCorrespondante(alerte.Titre, alerte.Message,
                alerte.Serre, alerte.Date, alerte.Urgence);
    }

    /**
     * Insère une nouvelle alerte dans la base de données
     */
    @Insert
    public abstract void inserer(Alerte alerte);

    /**
     * Insère une liste d'alertes dans la base de données
     */
    @Insert
    public abstract long[] inserer(List<Alerte> alertes);

    /**
     * Met à jour l'alerte donnée dans la base de données
     */
    @Update
    public abstract void actualiser(Alerte alerte);

    /**
     * Marque l'alerte avec l'identifiant donné comme consultée
     */
    @Query("UPDATE alerte SET consultee=1 WHERE uid=:uid")
    public abstract void marquerConsultee(int uid);

    /**
     * Marque toutes les alertes de la serre donnée comme consultées
     */
    @Query("UPDATE alerte SET consultee=1 WHERE serre=:serre AND consultee=0")
    public abstract void toutesMarquerConsultees(int serre);

    /**
     * Supprime l'alerte donnée dans la base de données
     */
    @Delete
    public abstract void supprimer(Alerte alerte);

    /**
     * Supprime toutes les alertes dont la date est inférieure ou égale à celle donnée et qui sont de la serre donnée
     */
    @Query("DELETE FROM alerte WHERE serre = :serre AND date <= :date")
    public abstract void supprimer(long date, int serre);
}
