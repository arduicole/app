package com.arduicole.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Alerte produite par une serre
 */
@Entity(
        tableName = "alerte",
        indices = {@Index(value = {"serre"})},
        foreignKeys = {@ForeignKey(entity = Serre.class, parentColumns = {"id"}, childColumns = {"serre"}, onDelete = ForeignKey.CASCADE, onUpdate = ForeignKey.CASCADE)}
)
@TypeConverters(ConvertisseurNiveauUrgence.class)
public class Alerte implements Serializable {
    // Identifiant unique de l'alerte dans la base de données
    @PrimaryKey(autoGenerate = true)
    public int uid;

    // Référence à une serre
    @SerializedName("serre")
    @ColumnInfo(name = "serre")
    public int Serre;

    // Titre de l'alerte
    @Expose
    @SerializedName("titre")
    @ColumnInfo(name = "titre")
    @NonNull
    public String Titre = "";

    // Date à laquelle l'alerte est survenue.
    // Unix epoch en ms.
    @Expose
    @SerializedName("date")
    @ColumnInfo(name = "date")
    public long Date = 0;

    // Date à laquelle l'alerte a été reçue.
    // Unix epoch en ms.
    @ColumnInfo(name = "date_recue")
    public long DateRecue = 0;

    // Message contenu dans l'alerte.
    @Expose
    @SerializedName("message")
    @ColumnInfo(name = "message")
    @NonNull
    public String Message = "";

    // Niveau de sévérité de l'alerte.
    @Expose
    @SerializedName("urgence")
    @ColumnInfo(name = "urgence")
    @NonNull
    public NiveauUrgence Urgence = NiveauUrgence.Basse;

    // Si l'alerte a été consultée.
    @ColumnInfo(name = "consultee")
    public boolean Consultee = false;

    public Alerte() {

    }

    /**
     * Niveau de sévérité d'une alerte.
     */
    public enum NiveauUrgence {
        Basse(0),
        Moyenne(1),
        Elevee(2);

        // Noms des niveaux d'urgence.
        public static final String[] NIVEAUX_URGENCE = {"basse", "moyenne", "élevée"};

        private int value;

        public int getValue() {
            return value;
        }

        public static NiveauUrgence fromInt(int value) {
            return NiveauUrgence.values()[value];
        }

        NiveauUrgence(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return NIVEAUX_URGENCE[value];
        }
    }
}
