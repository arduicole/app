package com.arduicole.db;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.security.cert.X509Certificate;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.RESTRICT;
import static com.arduicole.db.TypeJour.Ensoleille;

@Entity(
        tableName = "serre",
        indices = {@Index(value = {"nom"}, unique = true),
                @Index(value = {"consigne"}),
                @Index(value = {"url", "port"}, unique = true),
                @Index(value = {"topic_fcm",}, unique = true)},
        foreignKeys = {@ForeignKey(entity = Consigne.class, parentColumns = {"uid"}, childColumns = {"consigne"}, onDelete = RESTRICT, onUpdate = CASCADE)}
)
@TypeConverters({ConvertisseurTypeJour.class, ConvertisseurCertificats.class})
public class Serre implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "nom")
    private String Nom = "";

    @NonNull
    @ColumnInfo(name = "url")
    private String Url = "";

    @ColumnInfo(name = "port")
    private short Port;

    @ColumnInfo(name = "ssid_pa")
    public String SsidPA;

    @ColumnInfo(name = "url_pa")
    public String UrlPA;

    @ColumnInfo(name = "port_pa")
    public Short PortPA;

    @ColumnInfo(name = "ssid_lan")
    public String SsidLAN;

    @ColumnInfo(name = "url_lan")
    public String UrlLAN;

    @ColumnInfo(name = "port_lan")
    public Short PortLAN;

    @ColumnInfo(name = "certificat")
    public X509Certificate Certificat;

    @Nullable
    @ColumnInfo(name = "consigne")
    private Integer Consigne;

    @NonNull
    @ColumnInfo(name = "type_jour")
    public TypeJour TypeJour = Ensoleille;

    // Date/Heure de la dernière alerte reçue.
    // Unix epoch en ms.
    @NonNull
    @ColumnInfo(name = "derniere_alerte")
    public long DerniereAlerte = 0;

    // Date/Heure du début des erreurs de connexion à la serre.
    @ColumnInfo(name = "debut_deconnexion")
    public Long DebutDeconnexion = null;

    // Topic FCM duquel des alertes sont reçues.
    // Si cette valeur est nulle, cette serre va seulement recevoir
    // les alertes de WorkerAlrtes.
    @ColumnInfo(name = "topic_fcm")
    public String Topic = null;

    public int getId() {
        return id;
    }

    public @NonNull
    String getNom() {
        return Nom;
    }

    public short getPort() {
        return Port;
    }

    public String getUrl() {
        return Url;
    }

    public Integer getConsigne() {
        return Consigne;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public void setNom(String pNom) {
        Nom = pNom;
    }

    public void setUrl(String pURL) {
        Url = pURL;
    }

    public void setPort(short pPort) {
        Port = pPort;
    }

    public void setConsigne(Integer consigne) {
        Consigne = consigne;
    }

    public Serre() {
    }

    public Serre(Serre serre) {
        setId(serre.getId());
        setNom(serre.getNom());
        setUrl(serre.getUrl());
        setPort(serre.getPort());
        setConsigne(serre.getConsigne());
        TypeJour = serre.TypeJour;
    }

    public Serre(String pNom, String pURL, short pPort) {
        setNom(pNom);
        Url = pURL;
        setPort(pPort);
    }
}
