package com.arduicole.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Serre.class, Consigne.class, Cycle.class, Alerte.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SerreDao serreDao();

    public abstract ConsigneDao consigneDao();

    public abstract CycleDao cycleDao();

    public abstract AlerteDao alerteDao();

    private static AppDatabase db;
    public final static String NOM = "controleur_serre";

    public static AppDatabase instance(Context context) {
        if (db == null)
            db = Room.databaseBuilder(context, AppDatabase.class, NOM).allowMainThreadQueries().build();
        return db;
    }
}
