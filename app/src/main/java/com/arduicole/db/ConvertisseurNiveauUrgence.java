package com.arduicole.db;

import androidx.room.TypeConverter;

public class ConvertisseurNiveauUrgence {
    @TypeConverter
    public int toInt(Alerte.NiveauUrgence niveauUrgence) {
        return niveauUrgence.getValue();
    }

    @TypeConverter
    public Alerte.NiveauUrgence fromInt(int niveauUrgence) {
        return Alerte.NiveauUrgence.fromInt(niveauUrgence);
    }
}
