package com.arduicole.db;

public enum TypeJour {
    Ensoleille(0),
    MiNuageux(1),
    Nuageux(2);
    public static final String[] TYPES_JOURS = {
            "ensoleille",
            "mi-nuageux",
            "nuageux",
    };

    private int value;

    public int getValue() {
        return value;
    }

    public static TypeJour fromInt(int value) {
        return TypeJour.values()[value];
    }

    private TypeJour(int value) {
        this.value = value;
    }
}
