package com.arduicole.db;

import androidx.room.TypeConverter;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class ConvertisseurCertificats {
    @TypeConverter
    public static byte[] toBytes(X509Certificate cert) {
        try {
            return cert.getEncoded();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @TypeConverter
    public static X509Certificate fromBytes(byte[] cert) {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            return (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(cert));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
