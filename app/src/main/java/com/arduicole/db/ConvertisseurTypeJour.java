package com.arduicole.db;

import androidx.room.TypeConverter;

public class ConvertisseurTypeJour {
    @TypeConverter
    public int toInt(TypeJour typeJour) {
        return typeJour.getValue();
    }

    @TypeConverter
    public TypeJour fromInt(int typeJour) {
        return TypeJour.values()[typeJour];
    }
}
