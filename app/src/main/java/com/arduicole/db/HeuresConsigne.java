package com.arduicole.db;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;

/**
 * Heures des consignes d'une journée.
 * Toutes les valeurs des heures sont représentées en nombre
 * de minutes depuis le début de la journée.
 */
public class HeuresConsigne {
    @Expose
    @SerializedName("prejour")
    public short PreJour;

    @Expose
    @SerializedName("jour")
    public short Jour;

    @Expose
    @SerializedName("prenuit")
    public short PreNuit;

    @Expose
    @SerializedName("nuit")
    public short Nuit;

    public HeuresConsigne() {
        PreJour = 240; // 4h
        Jour = 360; // 6h
        PreNuit = 1080; // 18h
        Nuit = 1200; // 20h
    }

    public HeuresConsigne(@NonNull short[] heures) {
        if (heures.length != 4)
            throw new IllegalArgumentException("4 heures doivent absolument être fournies");

        PreJour = heures[0];
        Jour = heures[1];
        PreNuit = heures[2];
        Nuit = heures[3];
    }

    /**
     * @return Une liste des heures des consignes dans l'ordre suivant: pré-jour, jour, pré-nuit, nuit
     */
    public short[] toArray() {
        return new short[]{PreJour, Jour, PreNuit, Nuit};
    }

    /**
     * @param valeur Valeur dont le nombre de minutes doit être calculé
     * @return Le nombre de minutes, entre 0 et 59 inclusivement, de la valeur donnée
     */
    public static short minutes(short valeur) {
        return (short) (valeur % 60);
    }

    /**
     * @param valeur Valeur dont le nombre d'heures doit être calculé
     * @return Le nombre d'heures, entre 0 et 24 inclusivement, de la valeur donnée
     */
    public static short heures(short valeur) {
        return (short) ((valeur / 60) % 24);
    }

    /**
     * @param heure  Heure de la journée entre 0 et 23 inclusivement
     * @param minute Minute de l'heure entre 0 et 59 inclusivement
     * @return Une heure compatible avec les champs d'un objet de cette classe
     */
    public static short calculerHeure(short heure, short minute) {
        return (short) ((heure * 60) + minute);
    }

    /**
     * Retourne l'index de la période de l'heure actuelle.
     * Cet index peut être utilisé pour indexé l'array retourné par {@link #toArray}.
     *
     * @return L'index de la période de l'heure actuelle
     */
    public int periode() {
        Calendar c = Calendar.getInstance();
        short heure = (short) c.get(Calendar.HOUR_OF_DAY);
        short minute = (short) c.get(Calendar.MINUTE);

        return periode(heure, minute);
    }

    /**
     * Retourne l'index de la période de l'heure donnée.
     * Cet index peut être utilisé pour indexé l'array retourné par {@link #toArray}.
     *
     * @return L'index de la période de l'heure actuelle
     */
    public int periode(final short heure, final short minute) {
        short[] heures = toArray();

        // Pour chaque période de la journée
        for (int i = com.arduicole.db.Jour.Nuit; i >= com.arduicole.db.Jour.PreJour; i--) {
            short h = heures[i];
            short m = minutes(h);
            h = heures(h);

            // Si l'heure est dans la période de la journée.
            if (heure > h || (heure == h && minute >= m))
                // Retourner la température associée á cette période.
                return i;
        }

        // Retourner la température associée á la période de la nuit.
        return com.arduicole.db.Jour.Nuit;
    }
}
