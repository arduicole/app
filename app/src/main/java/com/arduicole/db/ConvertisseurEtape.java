package com.arduicole.db;

import androidx.room.TypeConverter;

import com.arduicole.Etape;

public class ConvertisseurEtape {
    @TypeConverter
    public int toString(Etape etape) {
        return etape.getEtape();
    }

    @TypeConverter
    public Etape fromString(int valeur) {
        Etape etape = new Etape(valeur);
        return etape;
    }
}
