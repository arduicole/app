package com.arduicole.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public abstract class SerreDao {
    @Query("SELECT * FROM serre")
    public abstract LiveData<List<Serre>> toutesLive();

    @Query("SELECT * FROM serre")
    public abstract List<Serre> toutes();

    @Query("SELECT t_consigne.soleil, t_consigne.mi_nuageux, t_consigne.nuageux, t_consigne.nom, " +
            "t_consigne.uid, t_consigne.variation_acceptable, t_consigne.variation_critique, " +
            "t_consigne.heures " +
            "FROM serre LEFT JOIN consigne as t_consigne " +
            "WHERE serre.id = :idSerre AND t_consigne.uid = serre.consigne " +
            "LIMIT 1")
    public abstract LiveData<Consigne> consigne(int idSerre);

    @Query("SELECT * FROM serre " +
            "WHERE serre.topic_fcm = :topic " +
            "LIMIT 1")
    public abstract Serre trouverTopic(String topic);

    @Insert
    public abstract void inserer(Serre serre);

    @Update
    public abstract void actualiser(Serre serre);

    @Query("UPDATE serre SET consigne = :consigneUid " +
            "WHERE id = :id")
    public abstract void actualiserConsigne(int id, int consigneUid);

    public void actualiserTypeJour(int id, TypeJour tj) {
        _actualiserTypeJour(id, tj.getValue());
    }

    @Query("UPDATE serre SET type_jour = :tj " +
            "WHERE id = :id")
    protected abstract void _actualiserTypeJour(int id, int tj);

    @Query("SELECT * FROM serre WHERE nom=:nom")
    public abstract boolean dupliqueNom(String nom);

    @Query("SELECT * FROM serre WHERE URL=:url AND port=:port")
    public abstract boolean dupliquePort(String url, short port);

    @Query("SELECT * FROM serre WHERE id!=:idExclu AND nom=:nom")
    public abstract boolean dupliqueNomModifier(Integer idExclu, String nom);

    @Query("SELECT * FROM serre WHERE id!=:idExclu AND URL=:url AND port=:port")
    public abstract boolean dupliquePortModifier(Integer idExclu, String url, short port);

    @Delete
    public abstract void supprimer(Serre serre);
}
