package com.arduicole.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public abstract class CycleDao {

    @Query("SELECT * FROM cycle")
    public abstract LiveData<List<Cycle>> toutes();

    @Query("SELECT * FROM cycle WHERE id=:id")
    public abstract Cycle id(Integer id);

    @Query("SELECT count(*) FROM cycle WHERE nom = :nom")
    public abstract boolean existeNom(String nom);

    @Query("SELECT count(*) FROM cycle WHERE id != :id AND  nom = :nom")
    public abstract boolean existeNomModifier(Integer id, String nom);

    @Insert
    public abstract void inserer(Cycle cycle);

    @Update
    public abstract void actualiser(Cycle cycle);

    @Delete
    public abstract void supprimer(Cycle cycle);
}
