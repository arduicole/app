package com.arduicole.db;

public class Jour {
    // Noms de toutes les propriétés en JSON.
    public static final String[] PROPS = {
            "prejour", "jour", "prenuit", "nuit"
    };

    public static final int PreJour = 0;
    public static final int Jour = 1;
    public static final int PreNuit = 2;
    public static final int Nuit = 3;

    public double[] temperatures = {0.0, 0.0, 0.0, 0.0};
}
