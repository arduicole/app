package com.arduicole.db;

import androidx.room.TypeConverter;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class ConvertisseurHeuresConsigne {
    @TypeConverter
    public static byte[] toBytes(HeuresConsigne valeur) {
        byte[] resultat = new byte[8];
        ShortBuffer buf = ByteBuffer.wrap(resultat).asShortBuffer();
        buf.put(valeur.toArray());
        return resultat;
    }

    @TypeConverter
    public static HeuresConsigne fromBytes(byte[] valeur) {
        ShortBuffer buf = ByteBuffer.wrap(valeur).asShortBuffer();
        short[] heures = new short[4];
        buf.get(heures);
        return new HeuresConsigne(heures);
    }
}
