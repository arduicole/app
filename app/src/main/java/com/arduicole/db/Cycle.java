package com.arduicole.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.arduicole.Etape;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity( //room
        tableName = "cycle",
        indices = {@Index(value = {"nom"}, unique = true)}
)
@TypeConverters(ConvertisseurEtape.class)
public class Cycle {

    @PrimaryKey(autoGenerate = true)              //room
    private Integer id;
    @NonNull                                      //room
    @ColumnInfo(name = "nom")                     //room
    private String Nom;
    @ColumnInfo(name = "repetition")              //room
    @Expose                                       //Gson
    @SerializedName("repetitions")                //Gson
    private int Repetition;
    @ColumnInfo(name = "duree_chauffage")         //room
    @Expose                                       //Gson
    @SerializedName("duree-chauffage")            //Gson
    private int DureeChauffage;
    @ColumnInfo(name = "duree_ventilation")       //room
    @Expose                                       //Gson
    @SerializedName("duree-ventilation")          //Gson
    private int DureeVentilation;
    @ColumnInfo(name = "premiere_etape")          //room
    @Expose                                       //Gson
    @SerializedName("premiere-etape")             //Gson
    public Etape PremiereEtape = new Etape();

    public Cycle() {
        PremiereEtape.setEtape(Etape.EtapeCycle.CHAUFFAGE.ordinal());
    }

    public Cycle(Integer id, String nom, int repetition, int dureeChauffage, int dureeVentilation, Etape premiereEtape) {
        this.id = id;
        Nom = nom;
        Repetition = repetition;
        DureeChauffage = dureeChauffage;
        DureeVentilation = dureeVentilation;
        PremiereEtape = premiereEtape;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public void setRepetition(int repetition) {
        Repetition = repetition;
    }

    public void setDureeChauffage(int dureeChauffage) {
        DureeChauffage = dureeChauffage;
    }

    public void setDureeVentilation(int dureeVentilation) {
        DureeVentilation = dureeVentilation;
    }

    public Integer getId() {
        return id;
    }

    public String getNom() {
        return Nom;
    }

    public int getRepetition() {
        return Repetition;
    }

    public int getDureeChauffage() {
        return DureeChauffage;
    }

    public int getDureeVentilation() {
        return DureeVentilation;
    }

}
