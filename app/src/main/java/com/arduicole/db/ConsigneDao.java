package com.arduicole.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import java.util.List;

@Dao
@TypeConverters({ConvertisseurJour.class, ConvertisseurHeuresConsigne.class})
public abstract class ConsigneDao {
    @Query("SELECT * FROM consigne")
    public abstract LiveData<List<Consigne>> toutes();

    @Query("SELECT * FROM consigne WHERE uid = :uid LIMIT 1")
    public abstract Consigne id(int uid);

    @Query("SELECT count(*) FROM consigne WHERE nom = :nom")
    public abstract boolean existe(String nom);

    @Query("SELECT * FROM consigne " +
            "WHERE variation_critique = :variationCritique " +
            "AND variation_acceptable = :variationAcceptable " +
            "AND heures = :heures " +
            "AND soleil = :soleil " +
            "AND mi_nuageux = :miNuageux " +
            "AND nuageux = :nuageux")
    protected abstract List<Consigne> _consignesEquivalentes(
            double variationCritique, double variationAcceptable,
            HeuresConsigne heures,
            Jour soleil, Jour miNuageux, Jour nuageux
    );

    public List<Consigne> consignesEquivalentes(Consigne consigne) {
        return _consignesEquivalentes(
                consigne.VariationCritique, consigne.VariationAcceptable,
                consigne.Heures,
                consigne.Soleil, consigne.MiNuageux, consigne.Nuageux
        );
    }

    @Insert
    public abstract long inserer(Consigne consigne);

    @Update
    public abstract void actualiser(Consigne consigne);

    @Delete
    public abstract void supprimer(Consigne consigne);
}
