package com.arduicole;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.arduicole.databinding.ActivityDetailsSerreBinding;
import com.arduicole.db.Alerte;
import com.arduicole.db.Serre;
import com.arduicole.tabs.TabsDetailsSerre;

import java.security.InvalidParameterException;

public class DetailsSerreActivity extends FragmentActivity {
    public static final String PARAM_SERRE = "com.arduicole.SERRE";
    public static final String PARAM_ALERTE = "com.arduicole.ALERTE";

    private ActivityDetailsSerreBinding binding;
    private Serre serre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailsSerreBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtenir la serre.
        serre = (Serre) getIntent().getSerializableExtra(PARAM_SERRE);
        if (serre == null) {
            throw new InvalidParameterException("Aucune serre n'a été passée en paramètre");
        }
        Alerte alerte = (Alerte) getIntent().getSerializableExtra(PARAM_ALERTE);

        // Configurer les onglets.
        binding.viewPager.setAdapter(new TabsDetailsSerre(this, getSupportFragmentManager(), serre));
        binding.tabs.setupWithViewPager(binding.viewPager);

        if (alerte != null) {
            // Afficher les alertes.
            binding.viewPager.setCurrentItem(3, false);
        }

        // Assigner le nom de la serre au titre
        binding.nomSerre.setText(serre.getNom());
    }
}
