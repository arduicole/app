package com.arduicole;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Progression {
    @Expose                                       //Gson
    @SerializedName("repetition")                 //Gson
    private int Repetition;
    @Expose                                       //Gson
    @SerializedName("etape")                      //Gson
    private String Etape;
    @Expose                                      //Gson
    @SerializedName("duree-chauffage")           //Gson
    private int DureeChauffage;
    @Expose                                      //Gson
    @SerializedName("duree-ventilation")         //Gson
    private int DureeVentilation;


    public Progression() {
    }

    public Progression(int repetitionProgression, String etapeProgression, int dureeChauffage, int dureeVentilation) {
        Repetition = repetitionProgression;
        Etape = etapeProgression;
        DureeChauffage = dureeChauffage;
        DureeVentilation = dureeVentilation;
    }

    public void setRepetition(int repetitionProgression) {
        Repetition = repetitionProgression;
    }

    public void setEtape(String etapeProgression) {
        Etape = etapeProgression;
    }

    public void setDureeChauffage(int dureeChauffage) {
        DureeChauffage = dureeChauffage;
    }

    public void setDureeVentilation(int dureeVentilation) {
        DureeVentilation = dureeVentilation;
    }

    public int getRepetition() {
        return Repetition;
    }

    public String getEtape() {
        return Etape;
    }

    public int getDureeChauffage() {
        return DureeChauffage;
    }

    public int getDureeVentilation() {
        return DureeVentilation;
    }
}
