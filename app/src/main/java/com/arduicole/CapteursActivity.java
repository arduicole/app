package com.arduicole;

import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.arduicole.databinding.ActivityCapteursBinding;
import com.arduicole.db.Serre;
import com.arduicole.net.TaskCapteurs;
import com.arduicole.parametres.Parametres;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class CapteursActivity extends AppCompatActivity {
    // Unité des capteurs à afficher.
    public static String UNITE = "CapteursActivity.UNITE";
    // Serre qui possède les capteurs.
    public static String SERRE = "CapteursActivity.SERRE";
    Handler handler;
    private ActivityCapteursBinding binding;
    // Unité des capteurs à afficher.
    private Capteur.Unite unite;
    // Serre des capteurs.
    private Serre serre;
    private AdapterCapteur adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityCapteursBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Bundle args = getIntent().getExtras();

        // Obtenir la serre.
        serre = (Serre) args.getSerializable(SERRE);
        if (serre == null)
            throw new IllegalArgumentException("Il manque l'argument " + SERRE);

        // Obtenir l'unité des capteurs.
        unite = (Capteur.Unite) args.getSerializable(UNITE);
        if (unite == null)
            throw new IllegalArgumentException("Il manque l'argument " + UNITE);

        // Assigner le titre de l'unité des capteurs à afficher.
        setTitle(unite == Capteur.Unite.Celsius ? "Température" : "Humidité");

        adapter = new AdapterCapteur(this);

        binding.listeCapteurs.setAdapter(adapter);
        binding.listeCapteurs.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        // Afficher les capteurs et leurs mesures.
        rafraichirPeriodiquement();

        super.onResume();
    }

    @Override
    protected void onPause() {
        // Arrêter le rafraîchissement périodique.
        if (handler != null)
            handler.removeCallbacksAndMessages(null);

        super.onPause();
    }

    /**
     * Rafraîchi périodiquement l'affichage des capteurs
     */
    void rafraichirPeriodiquement() {
        afficherCapteurs();

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Initialiser un interval à 30 secondes.
                int interval = 30_000;
                // Si on peut rafraîchir les données
                if (Parametres.Rafraichir()) {
                    // Rafraîchir les données.
                    afficherCapteurs();

                    // Assigner l'interval de rafraîchissement à l'interval.
                    interval = Parametres.IntervalRafraichissement();
                }

                // Re-planifier la tache pour plus tard.
                handler.postDelayed(this, interval);
            }
        }, Parametres.IntervalRafraichissement());
    }

    /**
     * Obtient et affiche les données des capteurs
     */
    void afficherCapteurs() {
        new TaskAfficherCapteurs(binding.minCapteur, binding.moyenneCapteur, binding.maxCapteur, adapter, unite)
                .execute(serre);
    }
}

class TaskAfficherCapteurs extends TaskCapteurs {
    WeakReference<TextView> mMin;
    WeakReference<TextView> mMoyenne;
    WeakReference<TextView> mMax;
    WeakReference<AdapterCapteur> mListeCapteurs;

    Capteur.Unite mUnite;

    TaskAfficherCapteurs(TextView min, TextView moyenne, TextView max, AdapterCapteur listeCapteurs, Capteur.Unite unite) {
        mMin = new WeakReference<>(min);
        mMoyenne = new WeakReference<>(moyenne);
        mMax = new WeakReference<>(max);
        mListeCapteurs = new WeakReference<>(listeCapteurs);
        mUnite = unite;
    }

    @Override
    protected void onPostExecute(Capteur[] capteurs) {
        TextView min = mMin.get();
        TextView moyenne = mMoyenne.get();
        TextView max = mMax.get();
        AdapterCapteur listeCapteurs = mListeCapteurs.get();
        if (min != null && moyenne != null && max != null && listeCapteurs != null) {
            // Calculer le sommaire.
            SommaireMesures sommaire = new SommaireMesures(capteurs, mUnite);

            // Afficher le sommaire.
            afficherMesure(min, sommaire.Minimum);
            afficherMesure(moyenne, sommaire.Moyenne);
            afficherMesure(max, sommaire.Maximum);

            // Obtenir les capteurs pour l'unité.
            ArrayList<Capteur> capteursUnite = new ArrayList<>();
            if (capteurs != null) {
                for (Capteur capteur : capteurs) {
                    if (capteur.unite == mUnite)
                        capteursUnite.add(capteur);
                }
            }

            // Assigner les capteurs à l'adapter.
            listeCapteurs.setCapteurs(capteursUnite);
        }
    }

    void afficherMesure(TextView tv, double mesure) {
        String sMesure = Capteur.FormatterMesure(mesure, mUnite);
        tv.setText(sMesure);
        tv.setError(sMesure == null ? "" : null);
    }
}
