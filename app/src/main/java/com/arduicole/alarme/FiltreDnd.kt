package com.arduicole.alarme

import android.app.NotificationManager
import android.content.Context
import android.media.AudioAttributes
import android.os.Build
import com.arduicole.R
import com.arduicole.parametres.Parametres

/**
 *
 */
class FiltreDnd(val context: Context) {
    /**
     * @return Le filtre ne pas déranger actif ou null
     */
    fun filtre(): Int? {
        var filtre: Int? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val notifMgr = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            filtre = notifMgr.currentInterruptionFilter
        }
        return filtre
    }

    /**
     * @return Si l'alarme doit être activée en fonction du filtre ne pas déranger
     * et des paramètres
     */
    fun activerAlarme(): Boolean {
        var resultat = Parametres.ActiverAlarmeAlertesUrgentes(context)
        if (resultat) {
            val f = filtre()
            if (f != null && f != NotificationManager.INTERRUPTION_FILTER_ALL) {
                val modesDnd = Parametres.ActiverAlarmeAlertesUrgentesDnd(context)
                val valeurs = context.resources.getStringArray(R.array.activer_alarme_dnd_entry_values)
                when (f) {
                    NotificationManager.INTERRUPTION_FILTER_UNKNOWN ->
                        resultat = modesDnd.contains(valeurs[0])
                    NotificationManager.INTERRUPTION_FILTER_PRIORITY ->
                        resultat = modesDnd.contains(valeurs[1])
                    NotificationManager.INTERRUPTION_FILTER_ALARMS ->
                        resultat = modesDnd.contains(valeurs[2])
                    NotificationManager.INTERRUPTION_FILTER_NONE ->
                        resultat = false
                }
            }
        }
        return resultat
    }

    /**
     * @return Le stream audio à utiliser pour l'alarme pour qu'elle
     * soit entendue malgrès le filtre ne pas déranger
     */
    fun streamAudio(): Int {
        // FIXME Ceci ne fonctionne pas pour tous les téléphones. Il est possible que ce
        //       soit dû au filtre NotificationManager.INTERRUPTION_FILTER_UNKNOWN qui bloque
        //       des stream audio qui sont choisis par l'usager.
        //       Pour l'instant, cette méthode retourne donc toujours le stream d'alarmes.
        /*
        var resultat = AudioAttributes.USAGE_NOTIFICATION_RINGTONE
        val f = filtre()
        if (f == NotificationManager.INTERRUPTION_FILTER_ALARMS) {
            resultat = AudioAttributes.USAGE_ALARM
        }
        return resultat
        */
        return AudioAttributes.USAGE_ALARM
    }
}
