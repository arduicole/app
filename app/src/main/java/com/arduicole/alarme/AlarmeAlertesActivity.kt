package com.arduicole.alarme

import android.app.KeyguardManager
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.arduicole.AdapterAlertes
import com.arduicole.R
import com.arduicole.databinding.ActivityAlarmeAlertesBinding
import com.arduicole.db.Alerte
import com.arduicole.db.AlerteDao
import com.arduicole.db.AppDatabase
import com.arduicole.parametres.Parametres


class AlarmeAlertesActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAlarmeAlertesBinding
    private lateinit var adapter: AdapterAlertes

    private lateinit var alerteDao: AlerteDao
    private lateinit var alertes: LiveData<List<Alerte>>

    private val observerAlertes = Observer<List<Alerte?>?> {
        if (it?.isNotEmpty() == true) {
            adapter.setAlertes(it)
        } else {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAlarmeAlertesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Montrer la flèche de retour.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val divListeAlertes = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        divListeAlertes.setDrawable(resources.getDrawable(R.drawable.divider_recyclerview, null))
        binding.listeAlertes.addItemDecoration(divListeAlertes)

        adapter = AdapterAlertes(this)
        registerForContextMenu(binding.listeAlertes)
        binding.listeAlertes.layoutManager = LinearLayoutManager(this)
        binding.listeAlertes.adapter = adapter

        alerteDao = AppDatabase.instance(this).alerteDao()
        alertes = alerteDao.alertesUrgentes()
        alertes.observe(this, observerAlertes)

        // Ouvrir l'écran.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
            val keygardMgr = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager?
            keygardMgr?.requestDismissKeyguard(this, null)
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        }

        demarrerAlarme(applicationContext)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        arreterAlarme()
        super.onDestroy()
    }

    companion object {
        private var mp: MediaPlayer? = null

        /**
         * Créé le [MediaPlayer] pour jouer la sonnerie au URI suivant
         * Le [MediaPlayer] est placé dans la variable [mp].
         * Si le [MediaPlayer] n'a pas pu être crée, [mp] sera `null`.
         */
        private fun creerMp(appContext: Context, uri: Uri) {
            val audioAttributes = AudioAttributes.Builder()
                    .setUsage(FiltreDnd(appContext).streamAudio())
                    .build()
            mp = MediaPlayer.create(appContext, uri, null, audioAttributes, AudioManager.AUDIO_SESSION_ID_GENERATE)
        }

        /**
         * Arrête l'alarme en cours s'il y en a une et libère le [mp] s'il n'est pas `null`.
         */
        private fun arreterAlarme() {
            mp?.apply {
                if (isPlaying) {
                    stop()
                }
                release()
            }
        }

        /**
         * Démarre l'alarme si une alarme n'est pas déjà en cours
         */
        @JvmStatic
        fun demarrerAlarme(context: Context) {
            if (mp?.isPlaying == true) {
                return
            }
            arreterAlarme()

            creerMp(context, Parametres.SonnerieAlertesUrgentes(context))
            if (mp == null) {
                Toast.makeText(context,
                        "Impossible de jouer l'alarme donnée",
                        Toast.LENGTH_LONG).show()
                creerMp(context, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE))
            }
            mp?.apply {
                isLooping = true
                setWakeMode(context, PowerManager.SCREEN_BRIGHT_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP)
                start()
            }
        }
    }
}
