package com.arduicole;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arduicole.apropos.AProposActivity;
import com.arduicole.databinding.ActivityMainBinding;
import com.arduicole.databinding.ModifierSerreBinding;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.DBTask;
import com.arduicole.db.Serre;
import com.arduicole.db.SerreDao;
import com.arduicole.net.TaskObtenirCertificat;
import com.arduicole.parametres.Encryption;
import com.arduicole.parametres.Parametres;
import com.arduicole.parametres.ParametresActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.lang.ref.WeakReference;
import java.security.cert.X509Certificate;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    SerreDao dao;
    private ActivityMainBinding binding;
    private AdapterSerre adapter;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 15);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 15);
        }

        // Initialiser l'encryption.
        try {
            Encryption.Initialiser(this);
        } catch (Exception e) {
            Log.e(Encryption.TAG, null, e);
            Toast.makeText(this,
                    "Le service d'encryption et de décryption des mots de passe n'a pas pu " +
                            "être initialisé. Veuillez tenter de changer votre mot de passe et de " +
                            "le rechanger à sa valeur actuelle afin de règler le problème.",
                    Toast.LENGTH_LONG).show();
        }

        DividerItemDecoration divListeSerres = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divListeSerres.setDrawable(getResources().getDrawable(R.drawable.divider_recyclerview));
        binding.listSerres.addItemDecoration(divListeSerres);

        adapter = new AdapterSerre(this);

        registerForContextMenu(binding.listSerres);
        binding.listSerres.setLayoutManager(new LinearLayoutManager(this));
        binding.listSerres.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {
                //si on veut supprimer la serre
                if (direction == ItemTouchHelper.LEFT) {
                    final Serre serre = adapter.get(viewHolder.getAdapterPosition());
                    // Créer et afficher le dialogue pour supprimer une serre.
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Supprimer")
                            .setMessage("Voulez-vous supprimer la serre: " + serre.getNom())
                            .setCancelable(false)
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new TaskSupprimerSerre(MainActivity.this.dao, MainActivity.this).execute(serre);
                                }
                            }).setNegativeButton("Non", null).show();
                }
                //si on veut modifier la serre
                else {
                    ModifierSerre(adapter.get(viewHolder.getAdapterPosition()));
                }

                //Recharger les serres pour effacer la couleur apres un swipe
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    int height = itemView.getBottom() - itemView.getTop();
                    int width = height / 3;

                    int backgroundRes;
                    RectF backgroundRect;
                    int iconeRes;
                    Rect iconeRect;


                    //si il swipe gauche (modifier)
                    if (dX > 0) {
                        backgroundRes = R.color.colorModifierSerre;
                        backgroundRect = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());

                        iconeRes = R.drawable.ic_edit;
                        iconeRect = new Rect(itemView.getLeft() + width, itemView.getTop() + width, itemView.getLeft() + 2 * (int) width, itemView.getBottom() - width);
                    }
                    //si il swipe droite (supprimer)
                    else {
                        backgroundRes = R.color.colorSupprimerSerre;
                        backgroundRect = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());

                        iconeRes = R.drawable.ic_delete;
                        iconeRect = new Rect(itemView.getRight() - 2 * width, itemView.getTop() + width, itemView.getRight() - width, itemView.getBottom() - width);
                    }

                    // Obtenir les ressources.
                    Resources resources = getApplication().getResources();

                    // Afficher le background.
                    Paint paint = new Paint();
                    int couleur = ResourcesCompat.getColor(resources, backgroundRes, null);
                    paint.setColor(couleur);
                    c.drawRect(backgroundRect, paint);

                    // Afficher l'icone.
                    Drawable icone = ResourcesCompat.getDrawable(resources, iconeRes, null);
                    icone.setBounds(iconeRect);
                    icone.draw(c);

                    // Appliquer les changement.
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }
        });
        itemTouchHelper.attachToRecyclerView(binding.listSerres);

        if (dao == null) {
            // Obtenir les serres et le dao.
            AppDatabase db = AppDatabase.instance(getApplicationContext());
            dao = db.serreDao();
            dao.toutesLive().observe(this, new Observer<List<Serre>>() {
                @Override
                public void onChanged(@Nullable List<Serre> serres) {
                    adapter.setSerres(serres);
                }
            });
        }

        // Lorsqu'on appuie sur le bouton ajouter.
        binding.Ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Afficher le dialogue pour ajouter une serre.
                ModifierSerre(null);
            }
        });

        // Lorsqu'on appuie sur le bouton de rafraîchissement.
        binding.Refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Rafraîchir le visuel des serres.
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onResume() {
        // Planifier les fonctions périodiques.
        rafraichirPeriodiquement();

        super.onResume();
    }

    @Override
    public void onPause() {
        // Arrêter le rafraichissement automatique.
        if (handler != null)
            handler.removeCallbacksAndMessages(null);

        super.onPause();
    }

    /**
     * Rafraîchie périodiquement les données dans l'interface
     */
    void rafraichirPeriodiquement() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Initialiser un interval à 30s.
                int interval = 30_000;

                // S'il faut rafraîchir les données périodiquement
                if (Parametres.Rafraichir()) {
                    // Rafraîchir les données des serres.
                    adapter.notifyDataSetChanged();

                    // Obtenir l'interval.
                    interval = Parametres.IntervalRafraichissement();
                }

                // Répéter la tâche après l'interval.
                handler.postDelayed(this, interval);
            }
        }, Parametres.IntervalRafraichissement());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.option_parametres) {
            Intent intent = new Intent(this, ParametresActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            return true;
        }
        if (id == R.id.option_a_propos) {
            Intent intent = new Intent(this, AProposActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Affiche un dialogue pour modifier une serre si la serre donnée n'est pas nulle,
     * ou pour ajouter une nouvelle serre et la modifier si elle l'est.
     *
     * @param serre La serre à modifier ou null pour insérer une serre
     * @return Un code d'erreur
     */
    private int ModifierSerre(final Serre serre) {
        // Créer et afficher le dialogue pour ajouter ou modifier une serre.
        final ModifierSerreBinding bindingDlg = ModifierSerreBinding.inflate(LayoutInflater.from(MainActivity.this));
        final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                .setView(bindingDlg.getRoot())
                .setCancelable(true)
                .setPositiveButton("Sauvegarder", null)
                .setNegativeButton("Annuler", null)
                .show();

        bindingDlg.btnConfigPa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bindingDlg.configPa.toggle();
            }
        });

        bindingDlg.btnConfigLan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bindingDlg.configLan.toggle();
            }
        });

        final MainTaskObtenirCertificat.HolderCertificat holderCertificat = new MainTaskObtenirCertificat.HolderCertificat();

        if (serre != null) {
            bindingDlg.txtSerre.setText(serre.getNom());
            bindingDlg.topic.setText(serre.Topic);
            bindingDlg.txtURL.setText(serre.getUrl());
            bindingDlg.txtPort.setText(Short.toString(serre.getPort()));

            if (serre.SsidPA != null) {
                bindingDlg.paTxtSSID.setText(serre.SsidPA);
                bindingDlg.paTxtURL.setText(serre.UrlPA);
                bindingDlg.paTxtPort.setText(serre.PortPA.toString());
            }

            if (serre.SsidLAN != null) {
                bindingDlg.lanTxtSSID.setText(serre.SsidLAN);
                bindingDlg.lanTxtURL.setText(serre.UrlLAN);
                bindingDlg.lanTxtPort.setText(serre.PortLAN.toString());
            }

            bindingDlg.txtCertificat.setText(serre.Certificat.getSerialNumber().toString(16));
            holderCertificat.Certificat = serre.Certificat;
        }

        //Si le bouton Enregistrer est cliqué
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Serre copySerre = decoderSerre(serre, bindingDlg.txtSerre, bindingDlg.topic, bindingDlg.txtURL,
                        bindingDlg.txtPort, bindingDlg.paTxtSSID, bindingDlg.paTxtURL, bindingDlg.paTxtPort,
                        bindingDlg.lanTxtSSID, bindingDlg.lanTxtURL, bindingDlg.lanTxtPort,
                        bindingDlg.txtCertificat, holderCertificat.Certificat);

                // Si les informations sont invalides.
                if (copySerre == null)
                    return;

                // Si le topic n'a pas changé, l'abonnement est déjà fait.
                if (serre != null && serre.Topic.equals(copySerre.Topic)) {
                    sauvegarderSerre(copySerre, false, bindingDlg, alertDialog);
                } else {
                    FirebaseMessaging.getInstance().subscribeToTopic(copySerre.Topic)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        sauvegarderSerre(copySerre, serre == null, bindingDlg, alertDialog);
                                    } else {
                                        Toast.makeText(MainActivity.this, "Abonnement au topic FCM raté",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });

        bindingDlg.obtenirCertificat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Serre copySerre = decoderSerre(serre, bindingDlg.txtSerre, bindingDlg.topic, bindingDlg.txtURL,
                        bindingDlg.txtPort, bindingDlg.paTxtSSID, bindingDlg.paTxtURL, bindingDlg.paTxtPort,
                        bindingDlg.lanTxtSSID, bindingDlg.lanTxtURL, bindingDlg.lanTxtPort,
                        null, holderCertificat.Certificat);

                if (copySerre != null) {
                    new MainTaskObtenirCertificat(MainActivity.this,
                            bindingDlg.txtCertificat, holderCertificat)
                            .execute(copySerre);
                }
            }
        });

        return 0;
    }

    /**
     * Sauvegarde une nouvelle serre ou une serre qui a été modifiée.
     *
     * @param serre       Serre à enregistrer
     * @param estNouvelle Si la serre est une nouvelle serre et qu'elle doit donc être insérer dans la base de données plutôt que d'être modifiée dans la base de données
     * @param bindingDlg  Éléments du dialogue
     * @param dlg         Dialogue
     */
    private void sauvegarderSerre(@NonNull Serre serre, boolean estNouvelle,
                                  @NonNull ModifierSerreBinding bindingDlg, @NonNull AlertDialog dlg) {
        // Sinon si la serre n'existe pas
        if (estNouvelle) {
            // Ajouter la serre.
            new TaskInsererSerre(dao, MainActivity.this, dlg,
                    bindingDlg.txtSerre, bindingDlg.txtURL, bindingDlg.paTxtSSID, bindingDlg.topic)
                    .execute(serre);
        }
        // Sinon si la serre existe
        else {
            // Modifier la serre.
            new TaskActualiserSerre(dao, MainActivity.this, dlg,
                    bindingDlg.txtSerre, bindingDlg.txtURL, bindingDlg.paTxtSSID, bindingDlg.topic)
                    .execute(serre);
        }
    }

    private static Serre decoderSerre(Serre serre, EditText nom, EditText topic,
                                      EditText url, EditText port,
                                      EditText paSsid, EditText paUrl, EditText paPort,
                                      EditText lanSsid, EditText lanUrl, EditText lanPort,
                                      EditText cert, X509Certificate nouveauCert) {
        Serre resultat = new Serre();
        if (serre != null) {
            try {
                resultat = new Serre(serre);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Boolean bErreurEntree = false;

        String sSerre = nom.getText().toString().trim();
        String sTopic = topic.getText().toString().trim();
        String sURL = url.getText().toString().trim();
        String sPort = port.getText().toString().trim();

        String sPaSsid = paSsid.getText().toString().trim();
        String sPaUrl = paUrl.getText().toString().trim();
        String sPaPort = paPort.getText().toString().trim();

        String sLanSsid = lanSsid.getText().toString().trim();
        String sLanUrl = lanUrl.getText().toString().trim();
        String sLanPort = lanPort.getText().toString().trim();

        // Assigner le nom s'il est valide sinon afficher une erreur
        if (sSerre.isEmpty()) {
            nom.setError("Le nom ne doit pas être vide");
            bErreurEntree = true;
        } else {
            resultat.setNom(sSerre);
        }

        // Obtenir le topic.
        // Assigner nul au topic si aucun n'a été donné.
        resultat.Topic = sTopic.isEmpty() ? null : sTopic;

        // Assigner l'URL síl n'est pas vide, sinon afficher une erreur.
        if (sURL.isEmpty()) {
            url.setError("URL requis");
            bErreurEntree = true;
        } else {
            resultat.setUrl(sURL);
        }

        // Assigner le port s'il est valide sinon afficher une erreur
        try {
            resultat.setPort(Short.parseShort(sPort));
        } catch (NumberFormatException e) {
            port.setError("Port invalide");
            bErreurEntree = true;
        }

        // Si une configuration a été entrée pour la serre
        if ((sPaSsid.length() + sPaUrl.length() + sPaPort.length()) > 0) {
            if (sPaSsid.isEmpty()) {
                paSsid.setError("Le nom du réseau WiFi de la serre doit être configuré pour utiliser le mode point d'accès");
                bErreurEntree = true;
            } else {
                resultat.SsidPA = sPaSsid;
            }

            if (sPaUrl.isEmpty()) {
                paUrl.setError("L'URL de la serre sur son réseau WiFi doit être configuré pour utiliser le mode point d'accès");
                bErreurEntree = true;
            } else {
                resultat.UrlPA = sPaUrl;
            }

            if (sPaPort.isEmpty()) {
                paPort.setError("Le port de la serre sur son réseau WiFi doit être configuré pour utiliser le mode point d'accès");
                bErreurEntree = true;
            } else {
                try {
                    resultat.PortPA = Short.parseShort(sPaPort);
                } catch (NumberFormatException e) {
                    paPort.setError("Port invalide");
                    bErreurEntree = true;
                }
            }
        } else {
            // Supprimer toute la configuration du point d'accès de la serre.
            resultat.SsidPA = null;
            resultat.UrlPA = null;
            resultat.PortPA = null;
        }

        // Si une configuration a été entrée pour le réseau local
        if ((sLanSsid.length() + sLanUrl.length() + sLanPort.length()) > 0) {
            if (sLanSsid.isEmpty()) {
                lanSsid.setError("Le nom du réseau WiFi doit être configuré pour utiliser le mode réseau local");
                bErreurEntree = true;
            } else {
                resultat.SsidLAN = sLanSsid;
            }

            if (sLanUrl.isEmpty()) {
                lanUrl.setError("L'URL de la serre doit être configuré pour utiliser le mode réseau local");
                bErreurEntree = true;
            } else {
                resultat.UrlLAN = sLanUrl;
            }

            if (sLanPort.isEmpty()) {
                lanPort.setError("Le port de la serre doit être configuré pour utiliser le mode réseau local");
                bErreurEntree = true;
            } else {
                try {
                    resultat.PortLAN = Short.parseShort(sLanPort);
                } catch (NumberFormatException e) {
                    lanPort.setError("Port invalide");
                    bErreurEntree = true;
                }
            }
        } else {
            // Supprimer toute la configuration du réseau local de la serre.
            resultat.SsidLAN = null;
            resultat.UrlLAN = null;
            resultat.PortLAN = null;
        }

        if (nouveauCert != null) {
            resultat.Certificat = nouveauCert;
        } else if (resultat.Certificat == null && cert != null) {
            cert.setError("Veuillez charger ou obtenir un certificat");
            bErreurEntree = true;
        }

        // S'il y eu une erreur d'entrée,
        // retourner null, sinon retourner le résultat.
        return bErreurEntree ? null : resultat;
    }
}

/**
 * Supprime une serre de la base de données
 */
class TaskSupprimerSerre extends DBTask<Serre, SerreDao, Integer> {
    private WeakReference<Context> mContext;

    TaskSupprimerSerre(SerreDao serreDao, Context context) {
        super(serreDao);
        mContext = new WeakReference<>(context);
    }

    @Override
    protected Integer tache(Serre serre) {
        mDao.supprimer(serre);
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Context context = mContext.get();
        if (integer == null && context != null) {
            Toast.makeText(context,
                    "La serre n'a pas pu être supprimée",
                    Toast.LENGTH_LONG).show();
        }
    }
}

/**
 * Tache pour le dialogue pour modifier ou ajouter une serre
 */
abstract class TaskDlgSerre extends DBTask<Serre, SerreDao, Integer> {
    private WeakReference<Context> mContext;
    private WeakReference<AlertDialog> mDlg;
    private WeakReference<EditText> mNom;
    private WeakReference<EditText> mURL;
    private WeakReference<EditText> mSSID;
    private WeakReference<EditText> mTopic;

    TaskDlgSerre(SerreDao serreDao, Context context, AlertDialog dlg, EditText nom, EditText url, EditText ssid, EditText topic) {
        super(serreDao);
        mContext = new WeakReference<>(context);
        mDlg = new WeakReference<>(dlg);
        mNom = new WeakReference<>(nom);
        mURL = new WeakReference<>(url);
        mSSID = new WeakReference<>(ssid);
        mTopic = new WeakReference<>(topic);
    }

    @Override
    protected Integer erreur(Exception e) {
        // Si c'est une erreur de contrainte.
        if (e instanceof SQLiteConstraintException) {
            String message = e.getMessage();
            // Si le nom est dupliqué
            if (message.contains("serre.nom")) {
                return 1;
            }
            // Sinon si le port et le URL sont invalides.
            else if (message.contains("serre.port") && message.contains("serre.url")) {
                return 2;
            }
            // Sinon si le ssid est dupliqué
            else if (message.contains("serre.ssid_pa")) {
                return 3;
            }
            // Sinon si le topic FCM est dupliqué.
            else if (message.contains("serre.topic_fcm")) {
                return 4;
            }
        }
        return super.erreur(e);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        if (integer != null) {
            // S'il n'y a pas d'erreur
            if (integer == 0) {
                // Fermer le dialogue.
                AlertDialog dlg = mDlg.get();
                if (dlg != null)
                    dlg.dismiss();
            } else if (integer == 1) {
                TextView nom = mNom.get();
                if (nom != null) {
                    nom.setError("Une serre avec ce nom existe déjà");
                }
            } else if (integer == 2) {
                TextView url = mURL.get();
                if (url != null) {
                    url.setError("Cette combinaison de port et d'url est déjà celle d'une autre serre");
                }
            } else if (integer == 3) {
                TextView ssid = mSSID.get();
                if (ssid != null) {
                    ssid.setError("Une serre avec ce SSID existe déjà");
                }
            } else if (integer == 4) {
                TextView topic = mTopic.get();
                if (topic != null) {
                    topic.setError("Une serre avec ce topic existe déjà");
                }
            }
        } else {
            Context context = mContext.get();
            if (context != null) {
                Toast.makeText(context,
                        "La serre n'a pas pu être créée pour une raison inconnue",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}

/**
 * Insère une nouvelle serre dans la base de données
 */
class TaskInsererSerre extends TaskDlgSerre {
    TaskInsererSerre(SerreDao serreDao, Context context, AlertDialog dlg, EditText nom, EditText url, EditText ssid, EditText topic) {
        super(serreDao, context, dlg, nom, url, ssid, topic);
    }

    @Override
    protected Integer tache(Serre serre) {
        mDao.inserer(serre);
        return 0;
    }
}

/**
 * Actualise la serre
 */
class TaskActualiserSerre extends TaskDlgSerre {
    TaskActualiserSerre(SerreDao serreDao, Context context, AlertDialog dlg, EditText nom, EditText url, EditText ssid, EditText topic) {
        super(serreDao, context, dlg, nom, url, ssid, topic);
    }

    @Override
    protected Integer tache(Serre serre) {
        mDao.actualiser(serre);
        return 0;
    }
}

/**
 * Actualise la serre
 */
class MainTaskObtenirCertificat extends TaskObtenirCertificat {
    private WeakReference<Context> mContext;
    private WeakReference<EditText> mEditCertificat;
    public HolderCertificat mHolderCertificat;

    MainTaskObtenirCertificat(Context context, EditText editCertificat, HolderCertificat holderCertificat) {
        mContext = new WeakReference<>(context);
        mEditCertificat = new WeakReference<>(editCertificat);
        mHolderCertificat = holderCertificat;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        // S'il y a eu une erreur
        if (integer == null || integer != 0) {
            Context context = mContext.get();
            if (context != null) {
                Toast.makeText(context,
                        "Le certificat de la serre n'a pas pu être obtenu. Assurez-vous d'y être connecté.",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            EditText editCertificat = mEditCertificat.get();
            if (editCertificat != null) {
                try {
                    editCertificat.setText(dernierCertificat.getSerialNumber().toString(16));
                    mHolderCertificat.Certificat = dernierCertificat;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class HolderCertificat {
        public X509Certificate Certificat;
    }
}
