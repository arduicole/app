package com.arduicole;

import com.arduicole.db.Cycle;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CycleExecute {
    @Expose                                       // Gson
    @SerializedName("cycle")                      // Gson
    public Cycle cycle = new Cycle();
    @Expose                                       // Gson
    @SerializedName("progression")                // Gson
    public Progression progression = new Progression();

    public CycleExecute() {
    }

    public CycleExecute(Cycle pCycle, Progression pProgression) {
        cycle = pCycle;
        progression = pProgression;
    }

    public static String FormatterChauffage(int pChauffageProgression, int pChauffage) {
        return pChauffageProgression + " / " + pChauffage + " m";
    }

    public static String FormatterVentilation(int pVentilationProgression, int pVentilation) {
        return pVentilationProgression + " / " + pVentilation + " m";
    }

    public static String FormatterRepetition(int pRepetitionProgression, int pRepetition) {
        return pRepetitionProgression + " / " + pRepetition + " fois";
    }
}
