package com.arduicole.ctrl_distance;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.arduicole.actionneur.Actionneur;
import com.arduicole.databinding.ActivityCtrlDistanceBinding;
import com.arduicole.db.Mode;
import com.arduicole.db.Serre;
import com.arduicole.net.TaskActionneurs;
import com.arduicole.net.TaskArreterControleDistance;
import com.arduicole.net.TaskControlerActionneurs;
import com.arduicole.net.TaskMode;
import com.arduicole.parametres.Parametres;

import java.lang.ref.WeakReference;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class CtrlDistanceActivity extends AppCompatActivity {
    // Clés pour passer des valeurs à cette activité
    public static final String SERRE_EXTRA = "CmdDistanceActivity.SERRE";

    // Clés pour enregistrer l'état de cette activité et le restauré
    private static final String COMMANDES_STATE = "CmdDistanceActivity.COMMANDES";

    private ActivityCtrlDistanceBinding binding;

    AdapterCommande adapterCommande;
    List<CommandeActionneur> commandes;

    Serre serre;

    Handler handler;

    TaskControlerActionneurs.Callback cbControleActionneurs = new TaskControlerActionneurs.Callback() {
        @Override
        public void onPostExecute(Integer codeErreur) {
            // Réactiver le fab.
            binding.fabEnvoyerCmd.setEnabled(true);

            Log.e("cds.CmdDistanceActivity", "yo on est làs! " + codeErreur);

            if (codeErreur != null) {
                if (codeErreur == 204) {
                    binding.mode.setChecked(true);
                    Log.e("cds.CmdDistanceActivity", "Ça devrait fucking fonctionner!");
                } else if (codeErreur == 409) {
                    Toast.makeText(CtrlDistanceActivity.this,
                            "La serre ne peut pas être contrôlée à distance lorsqu'elle est en mode manuel",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(CtrlDistanceActivity.this,
                            "La serre a retourné une erreur inconnue lors de l'envoi des commandes",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(CtrlDistanceActivity.this,
                        "Il est impossible d'envoyer une commande à la serre pour une raison inconnue",
                        Toast.LENGTH_LONG).show();
            }
        }
    };

    TaskArreterControleDistance.Callback cbTaskArreterControleDistance = new TaskArreterControleDistance.Callback() {
        @Override
        public void onPostExecute(Integer codeErreur) {
            String message = null;
            if (codeErreur == null) {
                message = "Une erreur inconnue est survenue avec la serre pendant l'arrêt du mode de contrôle à distance";
                binding.mode.setChecked(true);
            } else if (codeErreur == 409) {
                message = "Le contrôleur n'était déjà pas en mode de contrôle à distance";
            } else if (codeErreur != 204) {
                message = "La serre a retourné une erreur inconnue lors de l'arrêt du mode de contrôle à distance";
                binding.mode.setChecked(true);
            }

            if (message != null) {
                Toast.makeText(CtrlDistanceActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCtrlDistanceBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtenir la serre.
        serre = (Serre) getIntent().getSerializableExtra(SERRE_EXTRA);
        if (serre == null) {
            throw new InvalidParameterException("Aucune serre n'a été passée en paramètre");
        }

        binding.listeCommandes.setLayoutManager(new LinearLayoutManager(this));
        adapterCommande = new AdapterCommande(this);
        binding.listeCommandes.setAdapter(adapterCommande);

        commandes = new ArrayList<>();

        binding.mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                buttonView.setClickable(isChecked);

                if (!isChecked) {
                    new TaskArreterControleDistance(cbTaskArreterControleDistance).execute(serre);
                }
            }
        });

        binding.fabRafraichir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rafraichirPeriodiquement();
            }
        });
    }

    @Override
    public void onResume() {
        // Planifier les fonctions périodiques.
        rafraichirPeriodiquement();

        super.onResume();
    }

    @Override
    public void onPause() {
        // Arrêter le rafraichissement automatique.
        if (handler != null)
            handler.removeCallbacksAndMessages(null);

        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        adapterCommande.updateCommandes();
        outState.putSerializable(COMMANDES_STATE, commandes.toArray());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Object[] objCommandes = (Object[]) savedInstanceState.getSerializable(COMMANDES_STATE);
        if (objCommandes != null) {
            for (Object commande : objCommandes) {
                commandes.add((CommandeActionneur) commande);
            }
            adapterCommande.setCommandes(commandes);
        }

        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Rafraîchie périodiquement les données dans l'interface
     */
    void rafraichirPeriodiquement() {
        rafraichir();

        if (handler != null) {
            // Arrêter le rafraichissement automatique planifié.
            handler.removeCallbacksAndMessages(null);
        } else {
            handler = new Handler();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Initialiser un interval à 30s.
                int interval = 30_000;

                // S'il faut rafraîchir les données périodiquement
                if (Parametres.Rafraichir()) {
                    rafraichir();

                    // Obtenir l'interval.
                    interval = Parametres.IntervalRafraichissement();
                }

                // Répéter la tache après l'interval.
                handler.postDelayed(this, interval);
            }
        }, Parametres.IntervalRafraichissement());
    }

    /**
     * Obtient le mode de la serre et l'état des actionneurs
     */
    void rafraichir() {
        // Obtenir et afficher le mode d'exécution.
        new TaskModeActionneurs(binding.mode, this).execute(serre);

        // Obtenir et afficher les actionneurs.
        new TaskObtenirActionneurs(commandes, adapterCommande, this)
                .execute(serre);
    }

    public void onFabClick(View view) {
        binding.fabEnvoyerCmd.setEnabled(false);
        adapterCommande.updateCommandes();
        new TaskControlerActionneurs(cbControleActionneurs, commandes).execute(serre);
    }
}

/**
 * Obtient la liste d'actionneurs de la serre
 */
class TaskObtenirActionneurs extends TaskActionneurs {
    List<CommandeActionneur> mCommandes;
    WeakReference<AdapterCommande> mAdapter;
    WeakReference<Context> mContext;

    public TaskObtenirActionneurs(List<CommandeActionneur> commandes, AdapterCommande adapter, Context context) {
        mCommandes = commandes;
        mAdapter = new WeakReference<>(adapter);
        mContext = new WeakReference<>(context);
    }

    @Override
    protected void onPostExecute(Actionneur[] actionneurs) {
        AdapterCommande adapter = mAdapter.get();
        Context context = mContext.get();
        if (adapter != null && mCommandes != null && context != null) {
            if (actionneurs != null) {
                boolean commandesIdentiques = true;
                if (actionneurs.length != mCommandes.size()) {
                    commandesIdentiques = false;
                } else {
                    for (int i = 0; i < actionneurs.length && commandesIdentiques; i++) {
                        Actionneur actionneur = actionneurs[i];
                        CommandeActionneur commande = mCommandes.get(i);

                        commandesIdentiques = commande.Actionneur.type().equals(actionneur.type()) &&
                                commande.Actionneur.getNom().equals(actionneur.getNom());
                    }
                }

                if (commandesIdentiques) {
                    adapter.updateCommandes();
                    for (int i = 0; i < actionneurs.length; i++) {
                        mCommandes.get(i).Actionneur = actionneurs[i];
                        CommandeActionneur cmd = mCommandes.get(i);
                        if (cmd instanceof CommandeMoteur) {
                            Log.e("cds.act", ((CommandeMoteur) cmd).Position + " position moteur");
                        } else if (cmd instanceof CommandeActionneurBool) {
                            Log.e("cds.act", ((CommandeActionneurBool) cmd).Etat + " bool");
                        }
                    }
                    Log.e("cds.waddup", "rien ne s'est passé");
                } else {
                    mCommandes.clear();
                    for (Actionneur actionneur : actionneurs) {
                        mCommandes.add(CommandeActionneur.fromActionneur(actionneur));
                    }
                }
                adapter.setCommandes(mCommandes);
            } else {
                Toast.makeText(context, "Les actionneurs n'ont pas pu être obtenus", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }
}

/**
 * Obtient le mode de la serre
 */
class TaskModeActionneurs extends TaskMode {
    WeakReference<Switch> mSwitch;
    WeakReference<Context> mContext;

    TaskModeActionneurs(Switch sw, Context context) {
        mSwitch = new WeakReference<>(sw);
        mContext = new WeakReference<>(context);
    }

    @Override
    protected void onPostExecute(Mode mode) {
        Switch sw = mSwitch.get();
        Context context = mContext.get();

        if (context != null && sw != null) {
            if (mode != null) {
                sw.setChecked(mode == Mode.ControleDistance);
            } else {
                Toast.makeText(context, "Le mode n'a pas pu être obtenu", Toast.LENGTH_LONG)
                        .show();
            }
        }

        super.onPostExecute(mode);
    }
}
