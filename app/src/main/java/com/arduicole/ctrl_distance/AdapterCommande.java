package com.arduicole.ctrl_distance;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arduicole.R;
import com.arduicole.databinding.FragmentCtrlActionneurBoolBinding;
import com.arduicole.databinding.FragmentCtrlMoteurBinding;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class AdapterCommande extends RecyclerView.Adapter<AdapterCommande.ViewHolder> {
    private LayoutInflater mInflater;
    private List<CommandeActionneur> mCommandes;
    private List<ViewHolder> mViewHolders = new ArrayList<>();

    public AdapterCommande(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public List<CommandeActionneur> getCommandes() {
        return mCommandes;
    }

    public void setCommandes(List<CommandeActionneur> commandes) {
        mCommandes = commandes;
        Log.e("cds.none", "objets: " + commandes.size());
        notifyDataSetChanged();
    }

    /**
     * Met à jour les commandes avec les données entrées par l'utilisateur
     *
     * @return Un code d'erreur
     * @note Pour obtenir les commandes à jour, utiliser {@link #getCommandes()}
     * @retval 0 Toutes les commandes étaient valides
     * @retval * Au moins une des commandes de l'utilisateur était invalide
     */
    public int updateCommandes() {
        int taille = Math.min(getItemCount(), mViewHolders.size());
        int resultat = 0;
        for (int i = 0; i < taille && resultat == 0; i++) {
            ViewHolder vh = mViewHolders.get(i);
            CommandeActionneur cmd = mCommandes.get(i);
            resultat = vh.updateCommande(cmd);
        }
        return resultat;
    }

    @NonNull
    @Override
    public AdapterCommande.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            FragmentCtrlActionneurBoolBinding binding = FragmentCtrlActionneurBoolBinding.inflate(mInflater, parent, false);
            return new ViewHolderBool(binding);
        } else if (viewType == 1) {
            FragmentCtrlMoteurBinding binding = FragmentCtrlMoteurBinding.inflate(mInflater, parent, false);
            return new ViewHolderMoteur(binding);
        } else {
            throw new InvalidParameterException("Le viewType '" + viewType + "' est inconnu");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCommande.ViewHolder holder, int position) {
        if (position == 0) {
            mViewHolders.clear();
        }
        mViewHolders.add(holder);

        holder.setCommande(mCommandes.get(position));
    }

    @Override
    public int getItemCount() {
        return mCommandes != null ? mCommandes.size() : 0;
    }

    /**
     * Retourne le type d'item view pour la commande à la position donnée dans mActionneurs
     *
     * @param position Position de la commande dans mCommandes
     * @return Le type de l'item view à utiliser pour la commande
     * @retval 0 L'actionneur est un CommandeActionneurBool
     * @retval 1 L'actionneur est un CommandeMoteur
     * @retval Integer.MAX_VALUE Le type de commande est inconnu
     */
    @Override
    public int getItemViewType(int position) {
        CommandeActionneur cmd = mCommandes.get(position);
        int resultat = Integer.MAX_VALUE;
        if (cmd instanceof CommandeActionneurBool) {
            resultat = 0;
        } else if (cmd instanceof CommandeMoteur) {
            resultat = 1;
        }
        return resultat;
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        TextView Nom;
        TextView Etat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Nom = itemView.findViewById(R.id.nom);
            Etat = itemView.findViewById(R.id.etat);
        }

        void setCommande(CommandeActionneur commande) {
            Nom.setText(commande.Actionneur.getNom());
            Etat.setText(commande.Actionneur.etat());
        }

        /**
         * Met à jour la commande avec les données entrées par l'utilisateur
         *
         * @param commande Commande qu'on veut mettre à jour
         * @return 1 La commande entrée par l'utilisateur est invalide
         */
        public abstract int updateCommande(CommandeActionneur commande);
    }

    class ViewHolderBool extends ViewHolder {
        FragmentCtrlActionneurBoolBinding binding;

        public ViewHolderBool(@NonNull FragmentCtrlActionneurBoolBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        void setCommande(CommandeActionneur commande) {
            super.setCommande(commande);

            CommandeActionneurBool cmd = (CommandeActionneurBool) commande;
            binding.commande.setChecked(cmd.Etat);
        }

        @Override
        public int updateCommande(CommandeActionneur commande) {
            ((CommandeActionneurBool) commande).Etat = binding.commande.isChecked();
            return 0;
        }
    }

    class ViewHolderMoteur extends ViewHolder {
        private FragmentCtrlMoteurBinding binding;

        public ViewHolderMoteur(@NonNull FragmentCtrlMoteurBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.commande.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    changerTextePourcentageCommande(progress);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        }

        @Override
        void setCommande(CommandeActionneur commande) {
            super.setCommande(commande);

            CommandeMoteur cmd = (CommandeMoteur) commande;
            binding.commande.setProgress(cmd.Position);
            changerTextePourcentageCommande(cmd.Position);
        }

        @Override
        public int updateCommande(CommandeActionneur commande) {
            CommandeMoteur cmd = (CommandeMoteur) commande;
            cmd.Position = binding.commande.getProgress();
            return 0;
        }

        private void changerTextePourcentageCommande(int progress) {
            StringBuilder texte = new StringBuilder(progress + "%");
            while (texte.length() < 4) {
                texte.insert(0, ' ');
            }
            binding.pctCommande.setText(texte.toString());
        }
    }
}
