package com.arduicole.ctrl_distance;

import com.arduicole.actionneur.Moteur;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import java.lang.reflect.Type;

/**
 * Commande à envoyer à un moteur de rollup
 */
public class CommandeMoteur extends CommandeActionneur {
    // Nouvelle position du moteur.
    public int Position;

    public CommandeMoteur(Moteur moteur, int position) {
        super(moteur);
        Position = position;
    }

    public static class Serializer extends CommandeActionneur.Serializer<CommandeMoteur> {
        @Override
        public JsonObject serialize(CommandeMoteur commande, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject obj = super.serialize(commande, typeOfSrc, context);
            obj.addProperty("position", commande.Position);
            return obj;
        }
    }
}
