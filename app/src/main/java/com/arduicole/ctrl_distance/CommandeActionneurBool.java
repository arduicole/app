package com.arduicole.ctrl_distance;

import com.arduicole.actionneur.ActionneurBool;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import java.lang.reflect.Type;

/**
 * Commande à envoyer à un actioneur booléen
 */
public class CommandeActionneurBool extends CommandeActionneur {
    // Nouvel état de l'actionneur.
    boolean Etat;

    public CommandeActionneurBool(ActionneurBool actionneur, boolean etat) {
        super(actionneur);
        Etat = etat;
    }

    public static class Serializer extends CommandeActionneur.Serializer<CommandeActionneurBool> {
        @Override
        public JsonObject serialize(CommandeActionneurBool commande, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject obj = super.serialize(commande, typeOfSrc, context);
            obj.addProperty("etat", commande.Etat);
            return obj;
        }
    }
}
