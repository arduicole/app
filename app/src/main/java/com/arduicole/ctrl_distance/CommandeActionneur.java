package com.arduicole.ctrl_distance;

import com.arduicole.actionneur.Actionneur;
import com.arduicole.actionneur.ActionneurBool;
import com.arduicole.actionneur.Moteur;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.security.InvalidParameterException;

/**
 * Commande à envoyer à un actionneur
 */
public abstract class CommandeActionneur implements Serializable {
    // Actionneur auquel s'adresse la commande.
    public com.arduicole.actionneur.Actionneur Actionneur;

    public CommandeActionneur(Actionneur actionneur) {
        Actionneur = actionneur;
    }

    /**
     * Créer une commande du bon type pour l'actionneur donné
     *
     * @param actionneur L'actionneur pour lequel il faut créer une commande
     * @return La commande pour l'actionneur
     */
    public static CommandeActionneur fromActionneur(Actionneur actionneur) {
        if (actionneur instanceof ActionneurBool) {
            ActionneurBool actionneurBool = (ActionneurBool) actionneur;
            return new CommandeActionneurBool(actionneurBool, actionneurBool.etat);
        } else if (actionneur instanceof Moteur) {
            Moteur moteur = (Moteur) actionneur;
            return new CommandeMoteur(moteur, (int) moteur.getPosition());
        } else {
            throw new InvalidParameterException("Le type de l'actionneur donné ne peut pas être converti en commande");
        }
    }

    public abstract static class Serializer<T extends CommandeActionneur> implements JsonSerializer<T> {
        @Override
        public JsonObject serialize(T commande, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject obj = new JsonObject();
            obj.addProperty("nom", commande.Actionneur.getNom());
            obj.addProperty("type", commande.Actionneur.type());
            return obj;
        }
    }
}
