package com.arduicole.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.arduicole.DetailsSerreActivity;
import com.arduicole.R;
import com.arduicole.background.ReceiverMarquerAlerteConsultee;
import com.arduicole.db.Alerte;
import com.arduicole.db.Serre;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Utilitaire pour envoyer des notifications
 */
public class Notifications {
    private static NotificationManager mNotifMgr;

    private static final String ID_GROUPE_CANAL_ALERTE = "alertes";
    private static NotificationChannelGroup mGroupeCanalNotifAlerte;
    private static final String[] ID_CANAUX_ALERTES = {
            "alertes_basses",
            "alertes_moyennes",
            "alertes_elevees",
    };

    private static final String ID_CHANNEL_ALERTES_ERREUR = "channel_alertes_erreur";
    private static NotificationChannel mChannelAlertesErreur;

    // Préfixe du nom de groupe des notifications d'alertes d'une serre.
    // Il doit être suivi par l'ID de la serre.
    private static final String GROUP_KEY_ALERTES = "cds.ALERTE_";

    // Nombre maximal de lignes qu'on peut ajouter à une NotificationCompat.InboxStyle.
    private static final int INBOX_STYLE_MAX_LINES = 5;

    // ID de l'alerte de sommaire pour toutes les autres alertes
    private static final int ID_SOMMAIRE_ALERTE = 0;
    // ID de l'alerte pour signaler que les alertes n'ont pas pu être obtenues da la serre
    private static final int ID_ERREUR_ALERTE = 1;

    // ID de la prochaine notification.
    private static int idProchaineNotif = 0;

    public static NotificationManager notificationMgr(Context ctx) {
        if (mNotifMgr == null) {
            mNotifMgr = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mNotifMgr;
    }

    private static ReceiverMarquerAlerteConsultee receiverMarquerAlerteConsultee;

    /**
     * Retourne un tag unique pour les notifications reliées aux alertes de la serre donnée
     *
     * @param serre Serre reliée aux notifications à créer
     * @return Le tag de notification
     */
    private static String tagAlerteSerre(Serre serre) {
        return "ALERTE_" + serre.getId();
    }

    /**
     * Notifie l'utilisateur que les notifications de la serre n'ont pas pu être obtenues
     *
     * @param ctx   Contexte
     * @param serre Serre de laquelle les alertes ont été reçues
     */
    public static void notificationErreurAlertes(Context ctx, Serre serre) {
        NotificationManagerCompat mgr = NotificationManagerCompat.from(ctx);
        if (mChannelAlertesErreur == null && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mChannelAlertesErreur = new NotificationChannel(ID_CHANNEL_ALERTES_ERREUR, "Déconnexion de la serre", NotificationManager.IMPORTANCE_HIGH);
            mChannelAlertesErreur.enableLights(true);
            mChannelAlertesErreur.setLightColor(Color.RED);
            mChannelAlertesErreur.enableVibration(true);
            mChannelAlertesErreur.setDescription("Erreur de connexion à une serre pour lire ses alertes");
            mgr.createNotificationChannel(mChannelAlertesErreur);
        }

        // Mettre à jour l'heure de l'alerte.
        if (serre.DebutDeconnexion == null) {
            serre.DebutDeconnexion = Calendar.getInstance().getTimeInMillis();
        }

        // Créer la notification.
        Notification notif =
                new NotificationCompat.Builder(ctx, ID_CHANNEL_ALERTES_ERREUR)
                        .setContentTitle(serre.getNom())
                        .setContentText("Il est impossible d'obtenir les alertes de la serre.")
                        .setSmallIcon(R.drawable.ic_cloud_off)
                        .setOnlyAlertOnce(true)
                        .setWhen(serre.DebutDeconnexion)
                        .build();

        // Publier la notification.
        mgr.notify(tagAlerteSerre(serre), ID_ERREUR_ALERTE, notif);
    }

    /**
     * Supprime la notification qui avertit l'utilisateur que les notifications de la serre
     * n'ont pas pu être obtenues
     *
     * @param ctx   Contexte
     * @param serre Serre pour laquelle il faut supprimer la notification
     */
    public static void supprimerNotifErreurAlertes(Context ctx, Serre serre) {
        serre.DebutDeconnexion = null;
        NotificationManagerCompat.from(ctx)
                .cancel(tagAlerteSerre(serre), ID_ERREUR_ALERTE);
    }

    /**
     * Publie des notifications pour les alertes d'une serre
     *
     * @param ctx     Context
     * @param serres  Liste de toutes les serres
     * @param alertes Liste d'alertes dont il faut notifier l'utilisateur
     */
    public static void notificationsAlertes(Context ctx, List<Serre> serres, List<Alerte> alertes) {
        NotificationManagerCompat mgr = NotificationManagerCompat.from(ctx);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O && mGroupeCanalNotifAlerte == null) {
            mGroupeCanalNotifAlerte = new NotificationChannelGroup(ID_GROUPE_CANAL_ALERTE, "Alertes");
            mgr.createNotificationChannelGroup(mGroupeCanalNotifAlerte);
            for (int i = 0; i < ID_CANAUX_ALERTES.length; i++) {
                final String urgence = Alerte.NiveauUrgence.NIVEAUX_URGENCE[i];
                NotificationChannel canalNotif = new NotificationChannel(ID_CANAUX_ALERTES[i], "Alertes: urgence " + urgence, NotificationManager.IMPORTANCE_HIGH);
                canalNotif.setGroup(ID_GROUPE_CANAL_ALERTE);
                canalNotif.enableLights(true);
                canalNotif.setLightColor(Color.RED);
                canalNotif.enableVibration(true);
                canalNotif.setDescription("Alertes d'urgence " + urgence + " reçues de différentes serres.");
                mgr.createNotificationChannel(canalNotif);
            }
        }

        // Rendre les serres facilement accessibles en fonction de leur ID et les
        // associées à une notification de groupe.
        HashMap<Integer, NotifsSerre> serresNotifs = new HashMap<>();
        for (Serre serre : serres) {
            serresNotifs.put(serre.getId(), new NotifsSerre(serre));
        }

        // Créer un array qui regroupe toutes les alertes du même niveau d'urgence.
        ArrayList<ArrayList<Alerte>> alertesUrgences = new ArrayList<>();
        for (int i = 0; i < Alerte.NiveauUrgence.values().length; i++) {
            alertesUrgences.add(new ArrayList<Alerte>());
        }

        // Créer les notifications des alertes.
        for (Alerte alerte : alertes) {
            NotifsSerre notifSerre = serresNotifs.get(alerte.Serre);

            if (notifSerre.Intent == null) {
                Intent notifIntent = new Intent(ctx, DetailsSerreActivity.class);
                notifIntent.putExtra(DetailsSerreActivity.PARAM_SERRE, notifSerre.Serre);
                notifIntent.putExtra(DetailsSerreActivity.PARAM_ALERTE, alerte);
                notifSerre.Intent = PendingIntent.getActivity(ctx, notifSerre.Serre.getId(), notifIntent, 0);
            }

            if (receiverMarquerAlerteConsultee == null) {
                receiverMarquerAlerteConsultee = new ReceiverMarquerAlerteConsultee();
                ctx.registerReceiver(receiverMarquerAlerteConsultee,
                        new IntentFilter(ReceiverMarquerAlerteConsultee.ACTION));
            }
            Intent deleteIntent = new Intent(ReceiverMarquerAlerteConsultee.ACTION);
            deleteIntent.putExtra(ReceiverMarquerAlerteConsultee.PARAM_ALERTE, alerte.uid);
            PendingIntent deletePendingIntent = PendingIntent.getBroadcast(ctx, alerte.uid, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification notif = new NotificationCompat.Builder(ctx, ID_CANAUX_ALERTES[alerte.Urgence.getValue()])
                    .setSmallIcon(R.drawable.ic_warning)
                    .setContentTitle(notifSerre.Serre.getNom() + ": " + alerte.Titre)
                    .setContentText(alerte.Message)
                    .setWhen(alerte.Date)
                    .setGroup(GROUP_KEY_ALERTES + alerte.Urgence.toString())
                    .setContentIntent(notifSerre.Intent)
                    .setDeleteIntent(deletePendingIntent)
                    .build();
            mgr.notify("cds.ALERTES", alerte.uid, notif);

            alertesUrgences.get(alerte.Urgence.getValue()).add(alerte);
        }

        // Créer les notifications sommaires des niveaux d'alerte.
        for (int i = 0; i < alertesUrgences.size(); i++) {
            final String niveauUrgence = Alerte.NiveauUrgence.NIVEAUX_URGENCE[i];
            ArrayList<Alerte> alertesUrgence = alertesUrgences.get(i);
            if (alertesUrgence.size() > 0) {
                // Créer la notification sommaire.
                NotificationCompat.InboxStyle inboxStyle =
                        new NotificationCompat.InboxStyle()
                                .setSummaryText("Urgences " + niveauUrgence + "s");
                for (int j = 0; j < Math.min(alertesUrgence.size(), INBOX_STYLE_MAX_LINES); j++) {
                    inboxStyle.addLine(alertesUrgence.get(j).Titre);
                }
                int alertesRestantes = alertes.size() - INBOX_STYLE_MAX_LINES;
                if (alertesRestantes > 0) {
                    inboxStyle.setBigContentTitle("+" + alertesRestantes + " autres alertes");
                }
                Notification notif =
                        new NotificationCompat.Builder(ctx, ID_CANAUX_ALERTES[i])
                                .setContentTitle("Alertes d'urgence " + niveauUrgence)
                                .setContentText("Nouvelles alertes")
                                .setSmallIcon(R.drawable.ic_warning)
                                .setStyle(inboxStyle)
                                .setGroup(GROUP_KEY_ALERTES + niveauUrgence)
                                .setGroupSummary(true)
                                .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_CHILDREN)
                                .build();

                // Publier la notification sommaire en réutilisant le même ID que la dernière
                // notification sommaire de la serre, si elle existe.
                mgr.notify("SOMMAIRE_ALERTES_" + niveauUrgence, ID_SOMMAIRE_ALERTE, notif);
            }
        }
    }

    /**
     * Publie la notification donnée avec un nouvel ID unique
     *
     * @param notif Notification à publier
     * @param ctx   Contexte duquel le NotificationManagerCompat est créé
     * @return l'id de la notification publiée
     */
    public static int notifier(@NonNull Notification notif, @NonNull Context ctx) {
        return notifier(notif, NotificationManagerCompat.from(ctx));
    }

    /**
     * Publie la notification donnée avec un nouvel ID unique
     *
     * @param notif Notification à publier
     * @param mgr
     * @return l'id de la notification publiée
     */
    public static int notifier(@NonNull Notification notif, @NonNull NotificationManagerCompat mgr) {
        int id = idNotif();
        mgr.notify(id, notif);
        return id;
    }

    /**
     * Retourne un ID unique pour une nouvelle notification.
     * Ceci retourne la valeur de idProchaineNotif puis retourne la variable.
     *
     * @return le nouvel ID
     */
    public static int idNotif() {
        int resultat = idProchaineNotif;
        idProchaineNotif++;
        return resultat;
    }
}

class NotifsSerre {
    Serre Serre;
    // Intent pour ouvrir l'activité de la serre.
    PendingIntent Intent;

    NotifsSerre(Serre serre) {
        Serre = serre;
    }
}
