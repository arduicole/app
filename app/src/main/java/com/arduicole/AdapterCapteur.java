package com.arduicole;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arduicole.databinding.FragmentCapteurBinding;

import java.util.List;

public class AdapterCapteur extends RecyclerView.Adapter<AdapterCapteur.ViewHolder> {
    private LayoutInflater mInflater;
    private List<Capteur> mCapteurs;

    public AdapterCapteur(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public List<Capteur> getCapteurs() {
        return mCapteurs;
    }

    public void setCapteurs(List<Capteur> capteurs) {
        mCapteurs = capteurs;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterCapteur.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FragmentCapteurBinding binding = FragmentCapteurBinding.inflate(mInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCapteur.ViewHolder holder, int position) {
        holder.setCapteur(mCapteurs.get(position));
    }

    @Override
    public int getItemCount() {
        return mCapteurs != null ? mCapteurs.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        FragmentCapteurBinding binding;

        public ViewHolder(@NonNull FragmentCapteurBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void setCapteur(Capteur capteur) {
            binding.nomCapteur.setText(capteur.nom);

            String mesure = Capteur.FormatterMesure(capteur.mesure, capteur.unite);
            binding.mesureCapteur.setText(mesure);
            binding.mesureCapteur.setError(mesure == null ? "" : null);
        }
    }
}
