package com.arduicole;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.arduicole.databinding.ListviewAlerteBinding;
import com.arduicole.db.Alerte;
import com.arduicole.db.AlerteDao;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.DBTask;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class AdapterAlertes extends RecyclerView.Adapter<AdapterAlertes.AlerteViewHolder> {
    private static final DateFormat FORMAT_HEURE = new SimpleDateFormat("H:mm:ss.SSS");

    private List<Alerte> alertes;
    private Context mContext;

    private LayoutInflater mInflater;

    public AdapterAlertes(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public AlerteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ListviewAlerteBinding binding = ListviewAlerteBinding.inflate(mInflater, viewGroup, false);
        return new AlerteViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlerteViewHolder alerteViewHolder, int i) {
        Alerte alerte = alertes.get(i);
        boolean premiere = i == 0;
        if (!premiere) {
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c2.setTimeInMillis(alerte.Date);
            c1.setTimeInMillis(alertes.get(i - 1).Date);

            premiere = c1.get(Calendar.DAY_OF_YEAR) != c2.get(Calendar.DAY_OF_YEAR) ||
                    c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR);
        }
        alerteViewHolder.setAlerte(alerte, premiere);
    }

    @Override
    public int getItemCount() {
        return alertes != null ? alertes.size() : 0;
    }

    public void setAlertes(List<Alerte> alertes) {
        this.alertes = alertes;
        notifyDataSetChanged();
    }

    public Alerte get(int i) {
        return alertes.get(i);
    }

    class AlerteViewHolder extends RecyclerView.ViewHolder {
        private ListviewAlerteBinding binding;
        private Alerte mAlerte;

        public AlerteViewHolder(@NonNull ListviewAlerteBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        /**
         * Change l'alerte à afficher
         *
         * @param alerte          Alerte à afficher
         * @param premiereJournee Si c'est la première alerte de la journée à afficher
         */
        public void setAlerte(Alerte alerte, boolean premiereJournee) {
            // Assigner la'alerte donnée à celle de l'objet.
            mAlerte = alerte;

            // Afficher les informations de l'alerte dans le layout.
            binding.marqueConsultee.setVisibility(mAlerte.Consultee ? View.GONE : View.VISIBLE);
            binding.Titre.setText(mAlerte.Titre);
            binding.Heure.setText(FORMAT_HEURE.format(new java.util.Date(mAlerte.Date)));
            binding.Message.setText(mAlerte.Message);
            int vis = View.GONE;
            if (premiereJournee) {
                vis = View.VISIBLE;
                DateFormat formatDate = DateFormat.getDateInstance(DateFormat.FULL);
                binding.Date.setText(formatDate.format(new java.util.Date(mAlerte.Date)));
            }
            binding.Date.setVisibility(vis);

            binding.Carte.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Créer et afficher le dialogue pour supprimer une serre.
                    AlertDialog.Builder dlgBuilder = new AlertDialog.Builder(itemView.getContext())
                            .setTitle(mAlerte.Titre)
                            .setMessage(mAlerte.Message)
                            .setCancelable(true)
                            .setPositiveButton("Fermer", null);
                    if (!mAlerte.Consultee) {
                        dlgBuilder.setNegativeButton("Marquer comme vue", null);
                    }
                    AlertDialog dlg = dlgBuilder.show();

                    if (!mAlerte.Consultee) {
                        final Button btnPositif = dlg.getButton(AlertDialog.BUTTON_NEGATIVE);
                        btnPositif.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!mAlerte.Consultee) {
                                    mAlerte.Consultee = true;
                                    new TaskActualiserAlerteConsultee(mContext, btnPositif)
                                            .execute(mAlerte);
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}

class TaskActualiserAlerteConsultee extends DBTask<Alerte, AlerteDao, Integer> {
    WeakReference<Context> mContext;
    WeakReference<Button> mBtnPositif;

    public TaskActualiserAlerteConsultee(@NonNull Context context, Button btnPositif) {
        super(AppDatabase.instance(context).alerteDao());
        mContext = new WeakReference<>(context);
        mBtnPositif = new WeakReference<>(btnPositif);
    }

    @Override
    protected Integer tache(Alerte alerte) {
        try {
            mDao.actualiser(alerte);
        } catch (Exception e) {
            alerte.Consultee = false;
            throw e;
        }
        return 0;
    }

    @Override
    protected void onPostExecute(Integer codeErreur) {
        Context context = mContext.get();
        Button btnPositif = mBtnPositif.get();

        int vis = View.INVISIBLE;
        if (codeErreur == null || codeErreur != 0) {
            vis = View.VISIBLE;
            if (context != null) {
                Toast.makeText(context,
                        "La modification à l'alerte n'a pas pue être enregistrée",
                        Toast.LENGTH_LONG).show();
            }
        }
        if (btnPositif != null) {
            btnPositif.setVisibility(vis);
        }

        super.onPostExecute(codeErreur);
    }
}
