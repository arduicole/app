package com.arduicole.apropos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arduicole.databinding.ListviewProjetsBinding;

import java.util.List;

public class AdapterProjets extends RecyclerView.Adapter<AdapterProjets.ProjetViewHolder> {
    private List<Projet> projets;
    private Context mContext;

    private LayoutInflater mInflater;

    public AdapterProjets(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ProjetViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ListviewProjetsBinding binding = ListviewProjetsBinding.inflate(mInflater, viewGroup, false);
        return new ProjetViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjetViewHolder alerteViewHolder, int i) {
        alerteViewHolder.setProjet(projets.get(i));
    }

    @Override
    public int getItemCount() {
        return projets != null ? projets.size() : 0;
    }

    public void setProjets(List<Projet> projets) {
        this.projets = projets;
        notifyDataSetChanged();
    }

    public Projet get(int i) {
        return projets.get(i);
    }

    class ProjetViewHolder extends RecyclerView.ViewHolder {
        private final ListviewProjetsBinding binding;
        private Projet mProjet;

        public ProjetViewHolder(@NonNull ListviewProjetsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.btnLien.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ouvrirLien(mProjet.Lien, mContext);
                }
            });
            binding.btnLicence.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ouvrirLien(mProjet.Licence, mContext);
                }
            });
        }

        public void setProjet(Projet projet) {
            // Assigner le projet donné à celui de l'objet.
            mProjet = projet;

            // Afficher les informations du projet dans le layout.
            binding.nom.setText(projet.Nom);
            binding.description.setText(projet.Description);
        }

        /**
         * Ouvre le lien donné dans un navigateur
         *
         * @param lien Lien à ouvrir dans un navigateur
         */
        void ouvrirLien(@NonNull String lien, Context context) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(lien));
            context.startActivity(browserIntent);
        }
    }
}
