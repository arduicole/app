package com.arduicole.apropos;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.arduicole.R;

public class AProposActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a_propos);
    }

    /**
     * Ouvre lien pour afficher le manuel d'usager dans un navigateur
     */
    public void onClickManuel(View view) {
        ouvrirLien("https://arduicole.gitlab.io/manuel-usager");
    }

    /**
     * Ouvre lien pour rapporter un bogue avec l'application mobile
     */
    public void onClickBogueApp(View view) {
        ouvrirLien("https://gitlab.com/arduicole/app/issues/new");
    }

    /**
     * Ouvre lien pour rapporter un bogue avec le contrôleur de serre
     */
    public void onClickBogueControleur(View view) {
        ouvrirLien("https://gitlab.com/arduicole/ControleurSerre/issues/new");
    }

    /**
     * Ouvre l'activité pour afficher les licences du projet
     */
    public void onClickLicences(View view) {
        Intent intent = new Intent(this, LicencesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    /**
     * Ouvre lien lien pour afficher la liste des contributeurs
     */
    public void onClickContributeurs(View view) {
        ouvrirLien("https://gitlab.com/arduicole/app/-/graphs/master");
    }

    /**
     * Ouvre le lien donné dans un navigateur
     *
     * @param lien Lien à ouvrir dans un navigateur
     */
    void ouvrirLien(@NonNull String lien) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(lien));
        startActivity(browserIntent);
    }
}
