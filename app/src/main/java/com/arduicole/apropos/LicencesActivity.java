package com.arduicole.apropos;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.arduicole.R;
import com.arduicole.databinding.ActivityLicencesBinding;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class LicencesActivity extends AppCompatActivity {
    private ActivityLicencesBinding binding;
    Projet[] projets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLicencesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        projets = Projet.lireProjets(this, R.raw.projets);

        AdapterProjets adapter = new AdapterProjets(this);
        adapter.setProjets(Arrays.asList(projets));

        binding.listeProjets.setLayoutManager(new LinearLayoutManager(this));
        binding.listeProjets.setAdapter(adapter);
    }
}

class Projet {
    // Nom du projet
    @SerializedName("nom")
    public String Nom;
    // Description du projet
    @SerializedName("description")
    public String Description;
    // URL du site web du projet
    @SerializedName("lien")
    public String Lien;
    // Lien vers la licence du projet
    @SerializedName("licence")
    public String Licence;

    public Projet(String nom, String description, String lien, String licence) {
        Nom = nom;
        Description = description;
        Lien = lien;
        Licence = licence;
    }

    /**
     * Lit les licences de la ressource donnée
     *
     * @param resource Resource du fichier JSON à lire
     * @return Les licences lues du fichier
     */
    public static Projet[] lireProjets(Context context, int resource) {
        Gson gson = new Gson();
        InputStream stream = context.getResources().openRawResource(resource);
        InputStreamReader reader = new InputStreamReader(stream);
        return gson.fromJson(reader, Projet[].class);
    }
}
