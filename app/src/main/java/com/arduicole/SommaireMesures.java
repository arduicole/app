package com.arduicole;

import java.util.ArrayList;

/**
 * Sommaire des mesures de capteurs pour une unité donnée.
 */
public class SommaireMesures {
    public double Moyenne;
    public double Maximum;
    public double Minimum;

    // Noms des capteurs qui ne fonctionnent pas.
    ArrayList<String> NomsCapteursHorsService;

    /**
     * Calcul le sommaire des mesures de capteurs pour l'unité donnée.
     * Si le nombre de capteur est 0 ou capteurs est nul,
     * Maximum = Double.MIN_VALUE, Moyenne = Double.NaN et Minimum = Double.MAX_VALUE.
     */
    public SommaireMesures(Capteur[] capteurs, Capteur.Unite unite) {
        Maximum = Double.MIN_VALUE;
        Moyenne = 0.0;
        Minimum = Double.MAX_VALUE;

        int compte = 0;
        if (capteurs != null) {
            for (Capteur capteur : capteurs) {
                if (capteur.unite == unite) {
                    if (Math.abs(capteur.mesure) <= Double.MAX_VALUE) {
                        // Incrémenter le nb de capteurs.
                        compte++;

                        // Ajouter la mesure à la moyenne.
                        Moyenne += capteur.mesure;

                        // Assigner la mesure au maximum si elle est plus grande.
                        if (capteur.mesure > Maximum)
                            Maximum = capteur.mesure;

                        // Assigner la mesure au maximum si elle est plus petite.
                        if (capteur.mesure < Minimum)
                            Minimum = capteur.mesure;
                    } else {
                        if (NomsCapteursHorsService == null)
                            NomsCapteursHorsService = new ArrayList<>();

                        NomsCapteursHorsService.add(capteur.nom);
                    }
                }
            }
        }

        // Diviser la somme des mesures des capteurs par le nb de mesures.
        Moyenne /= compte;
    }

    /**
     * Créer et retourne un message d'erreur pour le sommaire des mesures.
     *
     * @return  {@code null} Aucune erreur n'est survenue, tous les capteurs fonctionnent;
     *          {@code !null} Message d'erreur décrivant quels capteurs ne fonctionnent pas.
     */
    public String messageErreur() {
        String resultat = null;
        if (NomsCapteursHorsService == null) {
            if (Double.isNaN(Moyenne)) {
                resultat = "La liste de capteurs n'a pas pue être obtenue";
            }
        } else {
            StringBuilder sb = new StringBuilder("Les capteurs suivants ne fonctionnent pas: ");
            for (String nom : NomsCapteursHorsService) {
                sb.append(nom);
                sb.append(' ');
            }
            resultat = sb.toString();
        }

        return resultat;
    }
}
