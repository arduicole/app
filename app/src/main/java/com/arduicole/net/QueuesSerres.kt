package com.arduicole.net

import com.arduicole.db.Serre
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.ConcurrentHashMap
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

/**
 * Gère la liste des clients HTTP pour les serres.
 * Chaque serre possède un seul client HTTP capable de faire
 * les requêtes les unes après les autres.
 */
class QueuesSerres private constructor() {
    companion object {
        @JvmStatic
        val instance by lazy {
            QueuesSerres()
        }
    }

    // Liste des clients associés à chaque serre.
    // La clé est l'ID de la serre et la valeur est le client pour la serre.
    //private var queues: HashMap<Int, QueueSerre> = HashMap()

    private var queues: ConcurrentHashMap<Int, QueueSerre> = ConcurrentHashMap()

    /**
     * Créer un client pour la [serre] donnée avec le [TrustManager] donné
     * ou un [TrustManager] par défaut.
     *
     * Si [trustMgr] n'est pas nul, un nouveau client est toujours
     * créé et possèdera le trust manager donné.
     * Sinon, un [TrustManager] qui accepte uniquement le certificat de la serre sera créé.
     *
     * @param serre La serre pour laquelle créer le client
     * @param trustMgr Le trust manager à donné au client ou nul
     * @param timeout Le trust manager à donné au client ou nul
     * @return Le client pour faire des requêtes à la serre
     */
    fun client(serre: Serre, trustMgr: X509TrustManager? = null, timeout: Int): HttpClient {
        return if (trustMgr == null) {
            val queue = queues.getOrPut(serre.id) {
                val client = creerClient(TrustOneCertificate(serre.Certificat), timeout)
                QueueSerre(client, serre.Certificat, timeout)
            }
            if (queue.certificat != serre.Certificat || queue.timeout != timeout) {
                queue.client = creerClient(TrustOneCertificate(serre.Certificat), timeout)
                queue.certificat = serre.Certificat
                queue.timeout = timeout
            }
            queue.client
        } else {
            creerClient(trustMgr, timeout)
        }
    }
}

/**
 * Créer un client avec le trust manager donné qui ne maintient qu'une connection à la fois à une serre
 */
private fun creerClient(trustMgr: X509TrustManager, timeout: Int) = HttpClient(Apache) {
    engine {
        connectTimeout = timeout

        customizeClient {
            setMaxConnTotal(1)
            setMaxConnPerRoute(1)

            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, arrayOf(trustMgr), null)
            setSSLContext(sslContext)
            setSSLHostnameVerifier { _, _ -> true }
        }
    }
}

/**
 * Représente une queue pour une serre.
 * Ceci a pour but de s'assurer que .
 */
internal data class QueueSerre(var client: HttpClient, var certificat: X509Certificate, var timeout: Int)

/**
 * Accepte tous les issuers de certificats de serveurs et
 * refuse d'authentifier les clients.
 * Il est fait pour qu'on y ajoute une implémentation de [checkServerTrusted]
 * pour valider le certificat d'un serveur
 */
abstract class TrustAllIssuersServer : X509TrustManager {
    override fun getAcceptedIssuers(): Array<X509Certificate> {
        return arrayOf()
    }

    override fun checkClientTrusted(certs: Array<X509Certificate>, authType: String) {
        throw UnsupportedOperationException("Le trust manager est seulement fait pour accepter les serveurs.")
    }
}

/**
 * Accepte un seul certificat donné provenant de n'importe quel issuer.
 *
 * @property certificat certificat à accepter
 */
private class TrustOneCertificate(
        private var certificat: X509Certificate) : TrustAllIssuersServer() {
    @Throws(CertificateException::class)
    override fun checkServerTrusted(certs: Array<X509Certificate>, authType: String) {
        if (certs.isNotEmpty()) {
            if (certs[0] != certificat) throw CertificateException("Le certificat retourné par le contrôleur de " +
                    "la serre ne correspond pas à celui configuré. Il se peut que " +
                    "quelqu'un tente de copier l'identité de la serre pour voler votre mot " +
                    "de passe. Si vous vous trouvez sur le même réseau que votre serre, " +
                    "vous pouvez modifier la serre pour obtenir son nouveau certificat. " +
                    "Assurez-vous d'être sur le même réseau que votre serre pour effectuer " +
                    "cette opération.")
        } else {
            throw CertificateException("La serre n'a retourné aucun certificat. Elle ne semble pas être configuré pour HTTPS.")
        }
    }
}
