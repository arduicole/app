package com.arduicole.net

import android.content.Context
import android.net.wifi.WifiManager
import android.os.AsyncTask
import android.util.Log
import com.arduicole.db.Serre
import com.arduicole.parametres.Parametres
import io.ktor.client.request.header
import io.ktor.client.request.host
import io.ktor.client.request.request
import io.ktor.client.statement.HttpResponse
import io.ktor.client.utils.EmptyContent
import io.ktor.http.HttpMethod
import io.ktor.http.URLProtocol
import kotlinx.coroutines.runBlocking
import java.io.IOException
import javax.net.ssl.X509TrustManager

/**
 * Envoie une requête en arrière plan à une serre
 *
 * @param Resultat Type retourné par la tâche
 */
abstract class NetTask<Resultat> constructor(
        private val mChemin: String,
        private val mMethode: HttpMethod = HttpMethod.Get,
        private val mAuthentification: Boolean = true) : AsyncTask<Serre, Void?, Resultat?>() {

    /**
     * Envoie des requêtes à toutes les serres données
     *
     * @param serres Serre à qui envoyer des requêtes
     * @return Le résultat de l'opération
     */
    override fun doInBackground(vararg serres: Serre): Resultat? {
        var resultat: Resultat? = null

        // Obtenir le mot de passe décrypté.
        val mdp = Parametres.MotDePasse()
        val authorization = String.format("Basic %s", mdp)
        val intervalRafraichissement = Parametres.IntervalRafraichissement()
        var tempsMort = Parametres.TempsMort()
        // NOTE: Lorsque le temps mort est inférieur à l'interval de rafraîchissement et qu'il
        // y a des erreurs de connexion, les requêtes s'accumulent et ça empêche l'utilisateur
        // de supprimer la serre.
        if (intervalRafraichissement < tempsMort) tempsMort = intervalRafraichissement
        for (serre in serres) {
            var hote = serre.url
            var port = serre.port
            val ssidWifi = ssidWifi()
            if (serre.SsidPA != null && serre.SsidPA == ssidWifi) {
                hote = serre.UrlPA
                port = serre.PortPA
            } else if (serre.SsidLAN != null && serre.SsidLAN == ssidWifi) {
                hote = serre.UrlLAN
                port = serre.PortLAN
            }

            runBlocking {
                val client = QueuesSerres.instance.client(serre, trustManager(serre), tempsMort)
                try {
                    val reponse = client.request<HttpResponse> {
                        url {
                            protocol = URLProtocol.HTTPS
                            host = hote
                            this.port = port.toInt()
                            encodedPath = chemin(serre)
                        }
                        method = mMethode
                        if (mAuthentification) {
                            header(AUTHORIZATION_HEADER, authorization)
                        }
                        body = corpsRequete(serre)
                    }
                    resultat = reponse(serre, reponse)
                } catch (t: Throwable) {
                    resultat = onException(t)
                }
            }
        }

        return resultat
    }

    /**
     * Est appelé lorsque n'importe quelle exception survient dans [.doInBackground]
     * Par defaut, imprime l'erreur sur le Log.e avec le tag [.TAG]
     *
     * @param e Exception survenue
     * @return null
     */
    protected open fun onException(e: Throwable): Resultat? {
        Log.e(TAG, e.message, e)
        return null
    }

    protected open fun trustManager(serre: Serre): X509TrustManager? {
        return null
    }

    /**
     * @return Le chemin de la requête pour la serre. Ex.: /capteurs
     * Par défaut mChemin est retourné.
     */
    protected open fun chemin(serre: Serre): String {
        return mChemin
    }

    /**
     * Envoyer le corps de la requête sur la connexion.
     * Cette méthode est vide par défaut.
     * Ne pas l'implémenter pour une requête sans corps.
     */
    @Throws(IOException::class)
    protected open fun corpsRequete(serre: Serre): Any {
        return EmptyContent
    }

    /**
     * Décoder le résultat à partir du corps.
     *
     * @return Le résultat décodé
     */
    @Throws(Exception::class)
    protected open fun reponse(serre: Serre, reponse: HttpResponse): Resultat? {
        return null
    }

    /**
     * Obtient le SSID du réseau WiFi auquel l'appareil est connecté
     * Cette fonction nécessite d'avoir la permission ACCESS_FINE_LOCATION
     *
     * @return Le SSID du réseau WiFi ou null si
     */
    private fun ssidWifi(): String? {
        if (mManager!!.isWifiEnabled) {
            val wifiInfo = mManager!!.connectionInfo
            if (wifiInfo != null) {
                var ssid = wifiInfo.ssid
                if (ssid != null && ssid != "<unknown ssid>") {
                    // Si le nom du réseau est encadré de guillemets
                    if (ssid.length > 2 && ssid[0] == '"' && ssid[ssid.length - 1] == '"') // Retirer les guillemets.
                        ssid = ssid.substring(1, ssid.length - 1)

                    // Retourner le nom du réseau.
                    return ssid
                }
            }
        }
        return null
    }

    companion object {
        const val TAG = "cds.NetTask"
        const val AUTHORIZATION_HEADER = "Authorization"
        private var mManager: WifiManager? = null

        /**
         * Obtient un WiFiManager afin de pouvoir obtenir le SSID du réseau WiFi
         */
        @JvmStatic
        @Synchronized
        fun init(context: Context) {
            val mgr = mManager
                    ?: context.applicationContext
                            .getSystemService(Context.WIFI_SERVICE) as WifiManager
            if (mManager == null) {
                mManager = mgr
            }
        }
    }
}
