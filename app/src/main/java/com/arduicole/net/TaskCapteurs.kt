package com.arduicole.net

import com.arduicole.Capteur
import com.arduicole.Capteur.Unite
import com.arduicole.db.Serre
import com.google.gson.*
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.jvm.javaio.toInputStream
import java.lang.reflect.Type

/**
 * Envoi une requête pour obtenir la liste des capteurs et de leurs mesures à /capteurs
 */
abstract class TaskCapteurs : NetTask<Array<Capteur>?>("/capteurs", HttpMethod.Get) {
    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Array<Capteur> {
        // S'il n'y a pas eu d'erreurs et qu'il y a une connexion pour recevoir des données
        if (reponse.status == HttpStatusCode.OK) {
            val gson = GsonBuilder()
                    .registerTypeAdapter(Capteur::class.java, CapteurDeserializer())
                    .create()
            return gson.fromJson(reponse.content.toInputStream().reader(), Array<Capteur>::class.java)
        } else {
            throw Exception("Les capteurs n'ont pas pu être obtenus.")
        }
    }

    private inner class CapteurDeserializer : JsonDeserializer<Capteur> {
        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Capteur {
            val obj = json.asJsonObject
            val mesureJson = obj["mesure"]
            val mesure = if (mesureJson.isJsonNull) Double.NaN else mesureJson.asDouble
            val nom = obj["nom"].asString
            val unite = parseUnite(obj["unite"].asString)
            return Capteur(nom, mesure, unite)
        }
    }

    companion object {
        // Degrés celsius en JSON
        private const val UNITE_CELSIUS = "C"

        // Humidité relative en JSON
        private const val UNITE_HR = "HR"

        /**
         * Désérialise une unité d'un enum d'unité
         *
         * @return L'unité désérialisée
         * @throws JsonParseException
         */
        @Throws(JsonParseException::class)
        private fun parseUnite(unite: String): Unite {
            return when (unite) {
                UNITE_CELSIUS -> Unite.Celsius
                UNITE_HR -> Unite.PourcentageHumiditeRelative
                else -> throw JsonParseException(String.format(
                        "L'unité d'un capteur devrait être \"%s\" ou \"%s\", mais \"%s\" a été reçu.",
                        UNITE_HR, UNITE_CELSIUS,
                        unite
                ))
            }
        }
    }
}