package com.arduicole.net

import com.arduicole.CycleExecute
import com.arduicole.Etape
import com.arduicole.Progression
import com.arduicole.db.Cycle
import com.arduicole.db.Serre
import com.google.gson.*
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.jvm.javaio.toInputStream
import java.lang.reflect.Type

abstract class TaskObtenirCycle : NetTask<CycleExecute?>("/cycle") {
    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): CycleExecute? {
        // S'il n'y a pas eu d'erreurs
        return if (reponse.status == HttpStatusCode.OK) {
            val gson = GsonBuilder()
                    .registerTypeAdapter(Etape::class.java, EtapeDeserializer())
                    .excludeFieldsWithoutExposeAnnotation()
                    .create()
            val obj = gson.fromJson(reponse.content.toInputStream().reader(), JsonObject::class.java)
            val cycle = gson.fromJson(obj["cycle"], Cycle::class.java)
            val progression = gson.fromJson(obj["progression"], Progression::class.java)
            CycleExecute(cycle, progression)
        } else if (reponse.status == HttpStatusCode.NotFound) {
            return null
        } else {
            throw Exception("Le cycle n'a pas pu être obtenu.")
        }
    }

    /**
     * Désérialize un jour reçu pour une consigne.
     */
    private inner class EtapeDeserializer : JsonDeserializer<Etape> {
        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Etape {
            val etape = json.asString
            val i = Etape.PROPS.indexOf(etape)
            if (i >= 0) {
                return Etape(i)
            } else {
                throw JsonParseException("L'étape reçue est invalide: $etape")
            }
        }
    }
}
