package com.arduicole.net

import com.arduicole.db.Serre
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.X509TrustManager

/**
 * Envoi une requête pour avoir le type de jour utiliser pour la consigne de la serre
 */
abstract class TaskObtenirCertificat : NetTask<Int?>("/", HttpMethod.Get, false) {
    @JvmField
    protected var dernierCertificat: X509Certificate? = null

    override fun trustManager(serre: Serre): X509TrustManager? {
        return TrustAllCertificates(serre)
    }

    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Int {
        return if (reponse.status == HttpStatusCode.NoContent) 0 else 1
    }

    /**
     * Accepte n'importe quel certificat pour n'importe quel issuer et l'enregistre dans la serre donnée.
     */
    private inner class TrustAllCertificates internal constructor(var mSerre: Serre) : TrustAllIssuersServer() {
        @Throws(CertificateException::class)
        override fun checkServerTrusted(certs: Array<X509Certificate>, authType: String) {
            // Si le serveur nous a donné un certificat
            if (certs.isNotEmpty()) {
                // L'assigner à la serre.
                mSerre.Certificat = certs[0]
                dernierCertificat = mSerre.Certificat
            } else {
                throw CertificateException("La serre n'a retourné aucun certificat. Elle ne semble pas être configuré pour HTTPS.")
            }
        }
    }
}
