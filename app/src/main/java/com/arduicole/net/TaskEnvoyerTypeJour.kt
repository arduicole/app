package com.arduicole.net

import com.arduicole.db.Serre
import com.arduicole.db.TypeJour
import io.ktor.client.statement.HttpResponse
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import java.io.IOException

/**
 * Envoi une requête avec un nouveau type de jour pour la serre
 */
abstract class TaskEnvoyerTypeJour(@JvmField protected var typeJour: TypeJour) : NetTask<Int?>("/type-jour", HttpMethod.Post) {
    /**
     * Envoyer le corps de la requête sur la connexion.
     * Cette méthode est vide par défaut.
     * Ne pas l'implémenter pour une requête sans corps.
     */
    @Throws(IOException::class)
    override fun corpsRequete(serre: Serre): Any {
        val typeJour = TypeJour.TYPES_JOURS[typeJour.value]
        return TextContent(typeJour, contentType = ContentType.Text.Plain)
    }

    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Int? {
        // S'il n'y a pas eu d'erreurs
        if (reponse.status == HttpStatusCode.NoContent) {
            return 0
        } else {
            throw Exception("Le type de jour de la serre n'a pas pu être changé")
        }
    }
}
