package com.arduicole.net

import com.arduicole.db.Serre
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpMethod
import java.lang.ref.WeakReference

/**
 * Désactive le mode de contrôle à distance
 */
class TaskArreterControleDistance(cb: Callback) : NetTask<Int?>("/actionneurs", HttpMethod.Delete) {
    protected var mCb: WeakReference<Callback> = WeakReference(cb)

    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Int {
        return reponse.status.value
    }

    override fun onPostExecute(codeErreur: Int?) {
        val cb = mCb.get()
        cb?.onPostExecute(codeErreur)
    }

    interface Callback {
        fun onPostExecute(codeErreur: Int?)
    }
}
