package com.arduicole.net

import com.arduicole.db.Mode
import com.arduicole.db.Serre
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.jvm.javaio.toInputStream

/**
 * Envoi une requête à une serre pour obtenir son mode
 */
abstract class TaskMode : NetTask<Mode?>("/mode") {
    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Mode {
        // S'il n'y a pas eu d'erreurs
        if (reponse.status == HttpStatusCode.OK) {
            val mode = reponse.content.toInputStream().bufferedReader().readLine()
            for (i in Mode.MODES.indices) {
                if (Mode.MODES[i] == mode)
                    return Mode.fromInt(i)
            }
            throw Exception("Le mode reçu de la serre est invalide")
        } else {
            throw Exception("Le mode de la serre n'a pas pu être obtenu.")
        }
    }
}
