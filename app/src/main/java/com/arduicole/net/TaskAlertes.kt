package com.arduicole.net

import com.arduicole.db.Alerte
import com.arduicole.db.Alerte.NiveauUrgence
import com.arduicole.db.Serre
import com.google.gson.*
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.jvm.javaio.toInputStream
import java.lang.reflect.Type

/**
 * Envoi une requête pour obtenir la liste des alertes survenue depuis la date donnée
 */
class TaskAlertes : NetTask<Array<Alerte>?>("/alertes") {
    /**
     * Génère le URL de la requête pour obtenir toutes les alertes suivant
     * la date de la dernière alerte survenue pour la serre donnée
     *
     * @return Le URL de la requête à envoyer
     */
    override fun chemin(serre: Serre): String {
        // Convertir la date/heure de ms en secondes.
        return "/alertes?depuis=" + serre.DerniereAlerte
    }

    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Array<Alerte> {
        // S'il n'y a pas eu d'erreurs et qu'il y a une connexion pour recevoir des données
        if (reponse.status == HttpStatusCode.OK) {
            val gson = GsonBuilder()
                    .registerTypeAdapter(NiveauUrgence::class.java, NiveauUrgenceDeserializer())
                    .create()
            val alertes = gson.fromJson(reponse.content.toInputStream().reader(), Array<Alerte>::class.java)
            for (alerte in alertes) {
                alerte.Serre = serre.id
            }
            return alertes
        } else {
            throw Exception("Les alertes n'ont pas pu être obtenues.")
        }
    }
}

class NiveauUrgenceDeserializer : JsonDeserializer<NiveauUrgence> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): NiveauUrgence {
        val niveau = json.asString
        val i = NiveauUrgence.NIVEAUX_URGENCE.indexOf(niveau)
        return if (i >= 0) {
            NiveauUrgence.fromInt(i)
        } else {
            throw JsonParseException("Le niveau d'urgence reçu est invalide: $niveau")
        }
    }
}
