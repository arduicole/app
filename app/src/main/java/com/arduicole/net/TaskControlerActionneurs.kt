package com.arduicole.net

import com.arduicole.ctrl_distance.CommandeActionneur
import com.arduicole.ctrl_distance.CommandeActionneurBool
import com.arduicole.ctrl_distance.CommandeMoteur
import com.arduicole.db.Serre
import com.google.gson.GsonBuilder
import io.ktor.client.statement.HttpResponse
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import java.io.IOException
import java.lang.ref.WeakReference

/**
 * Envoi des commandes aux actionneurs
 */
class TaskControlerActionneurs(cb: Callback,
                               var mCommandes: List<CommandeActionneur>) :
        NetTask<Int?>("/actionneurs", HttpMethod.Post) {
    var mCb: WeakReference<Callback> = WeakReference(cb)

    @Throws(IOException::class)
    override fun corpsRequete(serre: Serre): Any {
        val gson = GsonBuilder()
                .registerTypeAdapter(CommandeActionneurBool::class.java, CommandeActionneurBool.Serializer())
                .registerTypeAdapter(CommandeMoteur::class.java, CommandeMoteur.Serializer())
                .create()
        val commandes = gson.toJson(mCommandes)
        return TextContent(commandes, contentType = ContentType.Application.Json)
    }

    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Int {
        return reponse.status.value
    }

    override fun onPostExecute(codeErreur: Int?) {
        val cb = mCb.get()
        cb?.onPostExecute(codeErreur)
    }

    interface Callback {
        fun onPostExecute(codeErreur: Int?)
    }
}
