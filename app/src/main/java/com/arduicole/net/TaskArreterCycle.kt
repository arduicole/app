package com.arduicole.net

import com.arduicole.db.Serre
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode

abstract class TaskArreterCycle : NetTask<Boolean?>("/cycle", HttpMethod.Delete) {
    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Boolean {
        return when (reponse.status) {
            HttpStatusCode.NoContent -> true // Le cycle a été arrêté.
            HttpStatusCode.Conflict -> false // Aucun cycle n'était en exécution
            else -> throw Exception("Le cycle n'a pas pu être effacé.")
        }
    }
}
