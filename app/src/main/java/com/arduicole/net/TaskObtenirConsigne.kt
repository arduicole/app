package com.arduicole.net

import com.arduicole.db.Consigne
import com.arduicole.db.Jour
import com.arduicole.db.Serre
import com.google.gson.*
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.jvm.javaio.toInputStream
import java.lang.reflect.Type

/**
 * Envoi une requête avec de nouvelles consignes à une serre
 */
abstract class TaskObtenirConsigne : NetTask<Consigne?>("/consignes") {
    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Consigne {
        // S'il n'y a pas eu d'erreurs et qu'il y a des données à lire
        return if (reponse.status == HttpStatusCode.OK) {
            val gson = GsonBuilder()
                    .registerTypeAdapter(Jour::class.java, JourDeserializer())
                    .excludeFieldsWithoutExposeAnnotation()
                    .create()
            gson.fromJson(reponse.content.toInputStream().reader(), Consigne::class.java)
        } else throw Exception("La consigne de la serre n'a pas pue être obtenue.")
    }

    /**
     * Désérialize un jour reçu pour une consigne.
     */
    private inner class JourDeserializer : JsonDeserializer<Jour> {
        @Throws(JsonParseException::class)
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Jour {
            val obj = json.asJsonObject
            val jour = Jour()
            for (i in Jour.PROPS.indices)
                jour.temperatures[i] = obj[Jour.PROPS[i]].asDouble
            return jour
        }
    }
}
