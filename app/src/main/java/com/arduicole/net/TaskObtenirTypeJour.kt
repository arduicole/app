package com.arduicole.net

import com.arduicole.db.Serre
import com.arduicole.db.TypeJour
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection

/**
 * Envoi une requête pour avoir le type de jour utiliser pour la consigne de la serre
 */
abstract class TaskObtenirTypeJour : NetTask<TypeJour?>("/type-jour") {
    @Throws(Exception::class)
    protected open fun reponse(serre: Serre?, connexion: HttpURLConnection): TypeJour? {
        val responseCode = connexion.responseCode
        val stream = connexion.inputStream

        // S'il n'y a pas eu d'erreurs
        if (responseCode == HttpURLConnection.HTTP_OK && stream != null) {
            val typeJour = BufferedReader(InputStreamReader(stream)).readLine()
            for (i in TypeJour.TYPES_JOURS.indices) {
                if (TypeJour.TYPES_JOURS[i] == typeJour) return TypeJour.values()[i]
            }
            throw Exception("Le type de jour retourné par la serre est invalide.")
        } else {
            throw Exception("Le type de jour de la serre n'a pas pu être obtenu.")
        }
    }
}
