package com.arduicole.net

import com.arduicole.db.Consigne
import com.arduicole.db.Jour
import com.arduicole.db.Serre
import com.google.gson.*
import io.ktor.client.statement.HttpResponse
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import java.io.IOException
import java.lang.reflect.Type

/**
 * Envoi une requête avec de nouvelles consignes à une serre
 */
abstract class TaskEnvoyerConsigne(@JvmField protected var consigne: Consigne) : NetTask<Int?>("/consignes", HttpMethod.Post) {

    /**
     * Envoyer le corps de la requête sur la connexion.
     * Cette méthode est vide par défaut.
     * Ne pas l'implémenter pour une requête sans corps.
     */
    @Throws(IOException::class)
    override fun corpsRequete(serre: Serre): Any {
        val gson = GsonBuilder()
                .registerTypeAdapter(Jour::class.java, JourSerializer())
                .excludeFieldsWithoutExposeAnnotation()
                .create()
        val consigne = gson.toJson(this.consigne)
        return TextContent(consigne, contentType = ContentType.Application.Json)
    }

    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Int {
        return reponse.status.value
    }

    private inner class JourSerializer : JsonSerializer<Jour> {
        override fun serialize(src: Jour, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
            val obj = JsonObject()
            for (i in Jour.PROPS.indices) obj.add(Jour.PROPS[i], JsonPrimitive(src.temperatures[i]))
            return obj
        }
    }
}
