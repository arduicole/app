package com.arduicole.net

import com.arduicole.actionneur.Actionneur
import com.arduicole.actionneur.ActionneurBool
import com.arduicole.actionneur.Moteur
import com.arduicole.actionneur.Moteur.Etat
import com.arduicole.db.Serre
import com.google.gson.*
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import io.ktor.utils.io.jvm.javaio.toInputStream
import java.lang.reflect.Type

/**
 * Envoi une requête pour obtenir la liste des capteurs et de leurs mesures à /capteurs
 */
abstract class TaskActionneurs : NetTask<Array<Actionneur>?>("/actionneurs") {
    override fun reponse(serre: Serre, reponse: HttpResponse): Array<Actionneur>? {
        // S'il n'y a pas eu d'erreurs et qu'il y a une connexion pour recevoir des données
        if (reponse.status == HttpStatusCode.OK) {
            val gson = GsonBuilder()
                    .registerTypeAdapter(Actionneur::class.java, ActionneurDeserializer())
                    .create()

            return gson.fromJson(reponse.content.toInputStream().reader(), Array<Actionneur>::class.java)
        } else {
            throw Exception("L'état de tous les actionneurs n'a pas pu être obtenu.")
        }
    }
}

private class ActionneurDeserializer : JsonDeserializer<Actionneur> {
    /**
     * Désérialise un Moteur ou un ActionneurBool
     *
     * @return L'actionneur désérialisé
     */
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Actionneur {
        val obj = json.asJsonObject
        val type = obj[PROP_TYPE].asString
        val nom = obj[PROP_NOM].asString
        val elementEtat = obj[PROP_ETAT]
        return if (type == TYPE_MOTEUR) {
            val sEtat = elementEtat.asString
            val etat: Etat
            etat = when (sEtat) {
                ETAT_NEUTRE -> Etat.Neutre
                ETAT_OUVRIR -> Etat.Ouvrir
                ETAT_FERMER -> Etat.Fermer
                else -> throw JsonParseException(String.format(
                        "L'état d'un moteur devrait être \"%s\", \"%s\" ou \"%s\", mais \"%s\" a été reçu.",
                        ETAT_NEUTRE, ETAT_OUVRIR, ETAT_FERMER,
                        sEtat
                ))
            }
            val position = obj[PROP_POSITION].asDouble
            Moteur(nom, etat, position)
        } else {
            val etat = elementEtat.asBoolean
            ActionneurBool(nom, type, etat)
        }
    }

    companion object {
        // Propriétés possible d'un actionneur
        private const val PROP_TYPE = "type"
        private const val PROP_NOM = "nom"
        private const val PROP_ETAT = "etat"
        private const val PROP_POSITION = "position"

        // Type d'un moteur
        private const val TYPE_MOTEUR = "moteur"

        // Variante de l'état du moteur
        private const val ETAT_NEUTRE = "neutre"
        private const val ETAT_OUVRIR = "ouvrir"
        private const val ETAT_FERMER = "fermer"
    }
}
