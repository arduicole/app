package com.arduicole.net

import com.arduicole.Etape
import com.arduicole.db.Cycle
import com.arduicole.db.Serre
import com.google.gson.*
import io.ktor.client.statement.HttpResponse
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import java.io.IOException
import java.lang.reflect.Type

abstract class TaskEnvoyerCycle(private val mCycle: Cycle) : NetTask<Int?>("/cycle", HttpMethod.Post) {

    /**
     * Envoyer le corps de la requête sur la connexion.
     * Cette méthode est vide par défaut.
     * Ne pas l'implémenter pour une requête sans corps.
     */
    @Throws(IOException::class)
    override fun corpsRequete(serre: Serre): Any {
        val gson = GsonBuilder()
                .registerTypeAdapter(Etape::class.java, EtapeSerializer())
                .excludeFieldsWithoutExposeAnnotation()
                .create()
        val cycle = gson.toJson(mCycle)
        return TextContent(cycle, contentType = ContentType.Application.Json)
    }

    @Throws(Exception::class)
    override fun reponse(serre: Serre, reponse: HttpResponse): Int {
        return reponse.status.value
    }

    private inner class EtapeSerializer : JsonSerializer<Etape> {
        override fun serialize(src: Etape, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
            return JsonPrimitive(Etape.PROPS[src.etape])
        }
    }
}
