package com.arduicole.background;

import android.util.Log;

import androidx.annotation.NonNull;

import com.arduicole.db.Alerte;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.Serre;
import com.arduicole.db.SerreDao;
import com.arduicole.net.NiveauUrgenceDeserializer;
import com.arduicole.utils.Notifications;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Service pour recevoir les messages FCM du contrôleur contenant les alertes.
 */
public class FcmService extends FirebaseMessagingService {
    private static String TOPIC_PREFIX = "/topics/";

    public void onMessageReceived(@NonNull RemoteMessage msg) {
        if (!msg.getFrom().startsWith(TOPIC_PREFIX))
            return;

        // Obtenir une référence à la base de données.
        AppDatabase db = AppDatabase.instance(getApplicationContext());
        SerreDao serreDao = db.serreDao();

        // Déterminer la serre du message.
        String from = msg.getFrom();
        if (!from.startsWith(TOPIC_PREFIX))
            return;
        final String topic = from.substring(TOPIC_PREFIX.length());
        Serre serre = serreDao.trouverTopic(topic);
        if (serre == null) {
            // Se désabonner du topic qui ne correspond à aucune serre.
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("FcmService",
                                    "Impossible de se désabonner du topic \"" + topic + "\"", e);
                        }
                    });
            return;
        }

        // Décoder la liste d'alertes.
        String json = msg.getData().get("alertes");
        if (json == null)
            return;
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Alerte.NiveauUrgence.class, new NiveauUrgenceDeserializer())
                .create();
        ArrayList<Alerte> alertes;
        try {
            alertes = gson.fromJson(json, new TypeToken<ArrayList<Alerte>>() {
            }.getType());
        } catch (Exception ignored) {
            return;
        }
        for (Alerte alerte : alertes) {
            alerte.Serre = serre.getId();
        }

        WorkerAlertes.traiterAlertes(getApplicationContext(), serre, alertes, serreDao, db.alerteDao());
        Notifications.notificationsAlertes(getApplicationContext(), Arrays.asList(serre), alertes);
    }
}
