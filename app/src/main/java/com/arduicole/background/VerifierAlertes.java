package com.arduicole.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.arduicole.db.Alerte;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.Serre;
import com.arduicole.utils.Notifications;

import java.util.List;

/**
 * Broadcast receiver utilisé pour démarrer la vérification des alertes
 */
public class VerifierAlertes extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        WorkerAlertes.planifierVerification(context, true);

        // Créer des notifications pour les alertes non-consultées.
        AppDatabase db = AppDatabase.instance(context);
        List<Serre> serres = db.serreDao().toutes();
        List<Alerte> alertesNonConsultees = db.alerteDao().alertesNonConsultees();
        Notifications.notificationsAlertes(context, serres, alertesNonConsultees);
    }
}
