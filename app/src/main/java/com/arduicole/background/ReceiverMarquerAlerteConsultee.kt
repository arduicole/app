package com.arduicole.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.arduicole.db.AppDatabase

/**
 * Marque l'alerte donnée comme lue.
 */
class ReceiverMarquerAlerteConsultee : BroadcastReceiver() {
    companion object {
        // Action à laquelle répond cet intent.
        const val ACTION: String = "com.arduicole.MARQUER_ALERTE_LUE"

        // Clé de l'extra à passer dans l'intent qui doit contenir l'ID de l'alerte à marquer comme consultée.
        const val PARAM_ALERTE: String = "ALERTE"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        try {
            AppDatabase.instance(context!!)
                    .alerteDao()
                    .marquerConsultee(intent!!.extras!!.getInt(PARAM_ALERTE))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
