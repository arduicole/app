package com.arduicole.background;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.arduicole.alarme.AlarmeAlertesActivity;
import com.arduicole.alarme.FiltreDnd;
import com.arduicole.db.Alerte;
import com.arduicole.db.AlerteDao;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.Serre;
import com.arduicole.db.SerreDao;
import com.arduicole.net.NetTask;
import com.arduicole.net.TaskAlertes;
import com.arduicole.parametres.Encryption;
import com.arduicole.parametres.Parametres;
import com.arduicole.utils.Notifications;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class WorkerAlertes extends Worker {
    private static final String TAG = "cds.WorkerAlertes";
    public static final String WORKER_ALERTES = "com.arduicole.background.WorkerAlertes";

    public WorkerAlertes(Context ctx, WorkerParameters params) {
        super(ctx, params);
    }

    @NonNull
    @Override
    public Result doWork() {
        // Obtenir une référence à la base de données.
        AppDatabase db = AppDatabase.instance(getApplicationContext());
        SerreDao serreDao = db.serreDao();
        AlerteDao alerteDao = db.alerteDao();

        try {
            Parametres.Initialiser(getApplicationContext());
            NetTask.init(getApplicationContext());
            Encryption.Initialiser(getApplicationContext());
        } catch (Exception e) {
            Log.e("cds.VerifierAlertes", "Le service de vérification d'alertes n'a pas pu être démarré", e);
            return Result.success();
        }

        // Démarrer des tâches pour envoyer une requête à chaque serre.
        List<Serre> serres = serreDao.toutes();
        TaskAlertes[] taches = new TaskAlertes[serres.size()];
        for (int i = 0; i < taches.length; i++) {
            final Serre serre = serres.get(i);
            final TaskAlertes tache = new TaskAlertes();
            taches[i] = tache;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    tache.execute(serre);
                }
            });
        }

        ArrayList<Alerte> listeAlertes = new ArrayList<>();
        for (int i = 0; i < taches.length; i++) {
            Serre serre = serres.get(i);
            ArrayList<Alerte> alertesSerre = null;
            try {
                Alerte[] resultat = taches[i].get();
                if (resultat != null) {
                    alertesSerre = new ArrayList<>();
                    Collections.addAll(alertesSerre, resultat);
                }
            } catch (Exception ignored) {
            }
            traiterAlertes(getApplicationContext(), serre, alertesSerre, serreDao, alerteDao);
            listeAlertes.addAll(alertesSerre);
        }

        // Afficher des notifications pour les alertes.
        Notifications.notificationsAlertes(getApplicationContext(), serres, listeAlertes);

        // Réexécuter le worker.
        // Comme le worker est en cours d'exécution, nous sommes obligé de
        // remplacer celui qui est en cours, car sinon celui que nous
        // voulons planifier, ne sera tout simplement pas planifié.
        planifierVerification(getApplicationContext(), true);

        return Result.success();
    }

    public static synchronized void traiterAlertes(@NonNull Context context, @NonNull Serre serre, ArrayList<Alerte> alertes, @NonNull SerreDao serreDao, @NonNull AlerteDao alerteDao) {
        boolean actualiserSerre = false;
        if (alertes != null) {
            Log.i(TAG, alertes.size() + " alertes reçues pour la serre \"" + serre.getNom() + '"');

            // Supprimer les alertes qui sont déjà dans la base de données.
            for (int i = 0; i < alertes.size(); i++) {
                Alerte alerte = alertes.get(i);
                if (alerteDao.alerteCorrespondante(alerte)) {
                    alertes.remove(i);
                    i--;
                }
            }

            // Traiter les nouvelles alertes.
            if (alertes.size() > 0) {
                final long heureMs = Calendar.getInstance().getTimeInMillis();
                for (Alerte alerte : alertes) {
                    alerte.Serre = serre.getId();
                    alerte.DateRecue = heureMs;

                    if (alerte.Date == 0) {
                        // Si une alerte n'a pas de date, l'heure du MCU n'a pas encore été
                        // synchronisée, donc utiliser l'heure actuelle comme heure
                        // pour cette alerte.
                        alerte.Date = heureMs;
                    } else if (alerte.Date > serre.DerniereAlerte) {
                        serre.DerniereAlerte = alerte.Date;
                    }
                }

                // S'assurer que l'heure de la dernière alerte ne dépasse pas l'heure
                // actuelle, sinon aucune nouvelle alerte ne pourra être reçue.
                serre.DerniereAlerte = Math.min(serre.DerniereAlerte, heureMs);

                actualiserSerre = true;
            }

            if (serre.DebutDeconnexion != null) {
                actualiserSerre = true;
                Notifications.supprimerNotifErreurAlertes(context, serre);
            }

            // Insérer les alertes dans la base de données.
            // Et déterminer le niveau d'urgence maximal des alertes.
            int niveauUrgenceMax = Alerte.NiveauUrgence.Basse.getValue();
            long[] idAlertes = alerteDao.inserer(alertes);
            for (int j = 0; j < alertes.size(); j++) {
                Alerte alerte = alertes.get(j);
                alerte.uid = (int) idAlertes[j];

                if (alerte.Urgence.getValue() > niveauUrgenceMax) {
                    niveauUrgenceMax = alerte.Urgence.getValue();
                }
            }

            if (niveauUrgenceMax == Alerte.NiveauUrgence.Elevee.getValue() &&
                    new FiltreDnd(context).activerAlarme()) {
                AlarmeAlertesActivity.demarrerAlarme(context);
                Intent intent = new Intent(context, AlarmeAlertesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        } else {
            actualiserSerre = serre.DebutDeconnexion == null;
            Notifications.notificationErreurAlertes(context, serre);
        }

        if (actualiserSerre) {
            serreDao.actualiser(serre);
        }
    }

    /**
     * Planifie l'exécution de cette classe immédiatement
     * Si cette tâche est déjà planifiée, la tâche planifié sera pas remplacée.
     *
     * @param ctx Contexte utiliser pour planifier la tâche
     */
    public static void planifierVerificationImmediate(Context ctx) {
        planifierVerification(ctx, true, 0);
    }

    /**
     * Planifie l'exécution de cette classe dans le nombre de secondes
     * retourné par Parametres.IntervalAlertes.
     * Si cette tâche est déjà planifiée, la tâche planifié ne sera pas remplacée.
     *
     * @param ctx Contexte utiliser pour planifier la tâche
     */
    public static void planifierVerification(Context ctx) {
        planifierVerification(ctx, false);
    }

    /**
     * Planifie l'exécution de cette classe dans le nombre de secondes
     * retourné par Parametres.IntervalAlertes.
     *
     * @param ctx       Contexte utiliser pour planifier la tâche
     * @param remplacer S'il faut remplacer la tâche déjà planifiée
     */
    public static void planifierVerification(Context ctx, boolean remplacer) {
        planifierVerification(ctx, remplacer, Parametres.IntervalVerificationAlertes(ctx));
    }

    /**
     * Planifie l'exécution de cette classe dans le nombre de secondes
     * retourné par Parametres.IntervalAlertes.
     * Si cette tâche est déjà planifiée, la tâche planifié ne sera pas remplacée.
     *
     * @param ctx       Contexte utiliser pour planifier la tâche
     * @param remplacer S'il faut remplacer la tâche si elle existe déjà
     * @param delai     Délai en secondes avant l'exécution de cette classe
     */
    private static void planifierVerification(Context ctx, boolean remplacer, long delai) {
        WorkManager mgr = WorkManager.getInstance(ctx);
        OneTimeWorkRequest.Builder workRequest = new OneTimeWorkRequest.Builder(WorkerAlertes.class);
        if (delai > 0) {
            workRequest.setInitialDelay(delai, TimeUnit.SECONDS);
        }
        mgr.enqueueUniqueWork(
                WORKER_ALERTES,
                remplacer ? ExistingWorkPolicy.REPLACE : ExistingWorkPolicy.KEEP,
                workRequest.build()
        );
    }
}
