package com.arduicole;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.arduicole.databinding.ListviewSerreBinding;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.Serre;
import com.arduicole.net.TaskCapteurs;

import java.lang.ref.WeakReference;
import java.util.List;

public class AdapterSerre extends RecyclerView.Adapter<AdapterSerre.SerreViewHolder> {
    // Images associées aux types de jours
    @LayoutRes
    private static int[] IMAGES_JOURS = {
            R.drawable.soleil,
            R.drawable.mi_nuageux,
            R.drawable.nuageux,
    };

    private List<Serre> serres;
    private Context mContext;

    private LayoutInflater mInflater;

    public AdapterSerre(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public SerreViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ListviewSerreBinding binding = ListviewSerreBinding.inflate(mInflater, viewGroup, false);
        return new SerreViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SerreViewHolder serreViewHolder, int i) {
        serreViewHolder.setSerre(serres.get(i));
    }

    @Override
    public int getItemCount() {
        return serres != null ? serres.size() : 0;
    }

    public void setSerres(List<Serre> serres) {
        this.serres = serres;
        notifyDataSetChanged();
    }

    public Serre get(int i) {
        return serres.get(i);
    }

    class SerreViewHolder extends RecyclerView.ViewHolder
            implements AdapterView.OnClickListener {
        private final ListviewSerreBinding binding;
        private Serre mSerre;

        public SerreViewHolder(@NonNull final ListviewSerreBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            itemView.setOnClickListener(this);
        }

        public void setSerre(Serre serre) {
            // Assigner la serre donnée à celle de l'objet.
            mSerre = serre;

            // Afficher les informations de la serre dans le layout.
            binding.Nom.setText(mSerre.getNom());
            binding.Ensoleillement.setImageResource(IMAGES_JOURS[mSerre.TypeJour.getValue()]);

            // Afficher le nombre d'alertes non consultées.
            binding.marqueConsultee.setVisibility(View.GONE);
            AppDatabase db = AppDatabase.instance(mContext);
            db.alerteDao()
                    .aAlerteNonConsultee(mSerre.getId())
                    .observe((LifecycleOwner) binding.getRoot().getContext(),
                            new Observer<Boolean>() {
                                @Override
                                public void onChanged(Boolean aAlerteNonConsultee) {
                                    binding.marqueConsultee.setVisibility(aAlerteNonConsultee ? View.VISIBLE : View.GONE);
                                }
                            });

            // Obtenir les mesures de la serre et les afficher.
            new TaskMesureSerre(binding.Temperature, binding.Humidite).execute(mSerre);
        }

        @Override
        public void onClick(View v) {
            // Afficher les détails de la serre.
            Intent intent = new Intent(mContext, DetailsSerreActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(DetailsSerreActivity.PARAM_SERRE, mSerre);
            mContext.startActivity(intent);
        }
    }
}

class TaskMesureSerre extends TaskCapteurs {
    private WeakReference<TextView> Temperature;
    private WeakReference<TextView> Humidite;

    TaskMesureSerre(TextView temperature, TextView humidite) {
        Temperature = new WeakReference<>(temperature);
        Humidite = new WeakReference<>(humidite);
    }

    @Override
    protected void onPostExecute(Capteur[] capteurs) {
        TextView temperature = Temperature.get();
        TextView humidite = Humidite.get();

        if (humidite != null && temperature != null) {
            SommaireMesures sommaireCelsius = new SommaireMesures(capteurs, Capteur.Unite.Celsius);
            SommaireMesures sommaireHumidite = new SommaireMesures(capteurs, Capteur.Unite.PourcentageHumiditeRelative);

            temperature.setText(Capteur.FormatterMesure(sommaireCelsius.Moyenne, Capteur.Unite.Celsius));
            temperature.setError(sommaireCelsius.messageErreur());

            humidite.setText(Capteur.FormatterMesure(sommaireHumidite.Moyenne, Capteur.Unite.PourcentageHumiditeRelative));
            humidite.setError(sommaireHumidite.messageErreur());
        }

        super.onPostExecute(capteurs);
    }
}
