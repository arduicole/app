package com.arduicole;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Adapter;
import android.widget.LinearLayout;

/**
 * LinearLayout avec un adapteur pour agir comme un
 * ListView sans scroll et qui affiche tous ses éléments
 */
public class LinearList extends LinearLayout {
    private Adapter mAdapter;

    public LinearList(Context context) {
        super(context);
    }

    public LinearList(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LinearList(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Adapter getAdapter() {
        return mAdapter;
    }

    public void setAdapter(Adapter adapter) {
        mAdapter = adapter;

        // Supprimer tous les enfants.
        removeAllViews();

        if (adapter != null) {
            for (int i = 0; i < adapter.getCount(); i++)
                addView(adapter.getView(i, null, this));
        }
    }
}
