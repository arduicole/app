package com.arduicole;

public class Etape {

    // Noms de toutes les propriétés en JSON.
    public static final String[] PROPS = {"chauffage", "ventilation"};

    public enum EtapeCycle {CHAUFFAGE, VENTILATION}

    int Etape;

    public Etape() {
    }

    public Etape(int etape) {
        Etape = etape;
    }

    public void setEtape(int etape) {
        Etape = EtapeCycle.values()[etape].ordinal();
    }

    public int getEtape() {
        return Etape;
    }
}
