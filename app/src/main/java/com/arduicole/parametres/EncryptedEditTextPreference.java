package com.arduicole.parametres;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

import androidx.preference.EditTextPreference;

public class EncryptedEditTextPreference extends EditTextPreference {

    public EncryptedEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public EncryptedEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EncryptedEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EncryptedEditTextPreference(Context context) {
        super(context);
    }

    @Override
    protected String getPersistedString(String defaultReturnValue) {
        String s = super.getPersistedString(null);
        if (s == null) {
            return defaultReturnValue;
        } else {
            try {
                return Encryption.Decrypter(s);
            } catch (Exception e) {
                Log.e(Encryption.TAG, e.getMessage(), e);
                Toast.makeText(getContext(),
                        "Le texte n'a pas pu être décrypté",
                        Toast.LENGTH_SHORT).show();
                return defaultReturnValue;
            }
        }
    }

    @Override
    protected boolean persistString(String value) {
        if (value != null) {
            try {
                return super.persistString(Encryption.Encrypter(value));
            } catch (Exception e) {
                Log.e(Encryption.TAG, null, e);
                Toast.makeText(getContext(),
                        "Le texte n'a pas pu être encrypté",
                        Toast.LENGTH_LONG).show();
            }
        }
        return true;
    }
}
