package com.arduicole.parametres;

import android.content.Context;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Permet d'encrypter et de décrypter le mot de passe avec une clé symmétrique
 * <b>Si l'algorithme d'encryption est changé, il faudra s'assurer de décrypter le mot de passe avec l'ancienne clé, puis de le réencrypter avec la nouvelle</b>
 */
public class Encryption {
    public static final String TAG = "serre.Encryption";
    private static final String ANDROID_KEY_STORE = "AndroidKeyStore";

    private static final String ALIAS_MDP = "MDP";

    private static SecretKey cleMdp;
    private static final String ALGORITHME = "AES";

    private static final String AES_MODE_23P = "AES/GCM/NoPadding";
    private static final String AES_MODE_U23 = "AES/CBC/PKCS5Padding";

    public static void Initialiser(Context context) throws
            java.security.GeneralSecurityException,
            java.io.IOException {
        if (cleMdp == null) {
            ObtenirCles();
            if (cleMdp == null)
                GenererCles();
        }
    }

    /**
     * Génère de nouvelles clés d'encryption et l'enregistre
     */
    public static void GenererCles() throws
            GeneralSecurityException,
            IOException {
        // Si le KeyStore existe et que
        // les clés secrètes peuvent y être enregistrées
        if (Build.VERSION.SDK_INT >= 23) {
            KeyStore keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            keyStore.load(null);

            KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHME, ANDROID_KEY_STORE);
            keyGenerator.init(
                    new KeyGenParameterSpec.Builder(ALIAS_MDP,
                            KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                            .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                            .setRandomizedEncryptionRequired(false)
                            .build());
            cleMdp = keyGenerator.generateKey();
        } else {
            // Générer la clé.
            byte[] cle = new byte[128 / Byte.SIZE];
            new SecureRandom().nextBytes(cle);
            cleMdp = new SecretKeySpec(cle, ALGORITHME);

            // Enregistrer la clés dans les préférences.
            String cleEncode = Encoder64(cleMdp.getEncoded());
            Parametres.CleMdp(cleEncode);
        }
    }

    /**
     * Obtient les clés d'encryption enregistrées
     *
     * @return la clé enregistrée ou null s'il n'y en avait aucune
     * @throws java.io.UnsupportedEncodingException
     * @throws java.security.GeneralSecurityException
     */
    public static void ObtenirCles() throws
            java.io.IOException,
            java.security.GeneralSecurityException {
        // Si le KeyStore existe et qu'on peut y enregistrer des clés symmétriques
        if (Build.VERSION.SDK_INT >= 23) {
            // Obtenir le key store.
            KeyStore keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            keyStore.load(null);

            // Obtenir la clé du keystore.
            cleMdp = (SecretKey) keyStore.getKey(ALIAS_MDP, null);
        } else {
            // Obtenir la clé des préférences.
            String cle = Parametres.CleMdp();

            // Si la clé est dans les préférences
            if (cle != null)
                cleMdp = new SecretKeySpec(Decoder64(cle), ALGORITHME);
        }
    }

    /**
     * Encrypte le mot de passe donné
     *
     * @param mdp Mot de passe à encrypter
     * @return Le mot de passe encrypté
     * @throws java.security.GeneralSecurityException
     */
    public static String Encrypter(String mdp) throws
            java.security.GeneralSecurityException {
        // Générer le IV.
        byte[] iv = new byte[Build.VERSION.SDK_INT >= 23 ? 12 : 16];
        new SecureRandom().nextBytes(iv);

        // Obtenir le cipher.
        Cipher c = cipher(Cipher.ENCRYPT_MODE, iv);

        // Encrypter le mot de passe.
        byte[] cipher = c.doFinal(mdp.getBytes());

        // Encoder le mot de passe et le iv et les retourner séparés par un ':'.
        return Encoder64(cipher) + ':' + Encoder64(iv);
    }

    /**
     * Décrypte le mot de passe donné
     *
     * @param mdp Mot de passe à décrypter
     * @return Le mot de passe décrypté
     * @throws java.security.GeneralSecurityException
     */
    public static String Decrypter(String mdp) throws
            java.security.GeneralSecurityException {
        // Décoder les bytes et le IV.
        String[] parts = mdp.split(":");
        byte[] bytes = Decoder64(parts[0]);
        byte[] iv = Decoder64(parts[1]);

        // Obtenir le cipher.
        Cipher c = cipher(Cipher.DECRYPT_MODE, iv);

        // Décoder le mot de passe.
        byte[] decodedBytes = c.doFinal(bytes);

        // Retourner le mot de passe décodé.
        return new String(decodedBytes);
    }

    private static Cipher cipher(int mode, byte[] iv) throws
            GeneralSecurityException {
        AlgorithmParameterSpec spec;
        String transformation;
        if (Build.VERSION.SDK_INT >= 23) {
            spec = new GCMParameterSpec(128, iv);
            transformation = AES_MODE_23P;
        } else {
            spec = new IvParameterSpec(iv);
            transformation = AES_MODE_U23;
        }

        // Obtenir le cipher.
        Cipher c = Cipher.getInstance(transformation);

        // Initialiser le cipher avec la clé et le IV.
        c.init(mode, cleMdp, spec);

        // Retourner le cipher.
        return c;
    }

    public static String Encoder64(byte[] b) {
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    public static byte[] Decoder64(String s) {
        return Base64.decode(s, Base64.NO_WRAP);
    }
}
