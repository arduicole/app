package com.arduicole.parametres;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.preference.PreferenceManager;

import androidx.core.net.ConnectivityManagerCompat;

import java.util.Collections;
import java.util.Set;

public class Parametres {
    // Interval de rafraîchissement sur une connexion illimitée
    public static final String CLE_INTERVAL_ILLIMITE = "interval_rafraichissement_illimite";
    public static final int INTERVAL_ILLIMITE_DEFAUT = 30;

    // S'il faut ou non rafraîchir les données de la serre sure une connexion limitée
    public static final String CLE_RAFRAICHIR_LIMITE = "rafraichir_limite";
    public static final boolean RAFRAICHIR_LIMITE_DEFAUT = true;

    // Interval de rafraîchissement sur une connexion limitée
    public static final String CLE_INTERVAL_LIMITE = "interval_rafraichissement_limite";
    public static final int INTERVAL_LIMITE_DEFAUT = 60;

    // Temps limite de connexion à une serre.
    public static final String CLE_TEMPS_MORT = "temps_mort";
    public static final int TEMPS_MORT_DEFAUT = 1000;

    // Mot de passe pour que les requêtes envoyées au contrôleur de serre soient acceptées
    public static final String CLE_MDP = "mot_de_passe";
    public static final String MDP_DEFAUT = "Changer ce mot de passe!";

    // Clé AES encryptée pour le mot de passe.
    // N'est utilisée que pour Android < 23
    public static final String CLE_CLE_MDP = "cle_mot_de_passe";

    public static final String CLE_INTERVAL_ALERTES = "interval_verification_alertes";
    public static final int INTERVAL_ALERTES_DEFAUT = 120;

    public static final String CLE_ACTIVER_ALARME_ALERTES_URGENTES = "activer_alarme_alertes_urgentes";
    public static final String CLE_SONNERIE_ALERTES_URGENTES = "sonnerie_alertes_urgentes";
    public static final String CLE_ACTIVER_ALARME_ALERTES_URGENTES_DND = "activer_alarme_alertes_urgentes_dnd";

    private static ConnectivityManager connMgr = null;
    private static SharedPreferences preferences = null;

    /**
     * Initialise les paramètres
     */
    public static void Initialiser(Context context) {
        if (connMgr == null || preferences == null) {
            connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    /**
     * Détermine si la les données des serres doivent être rafraîchies ou en
     * fonction des paramètres et du type de connexion.
     *
     * @return Si les données des serres doivent être rafraîchies ou non
     */
    public static boolean Rafraichir() {
        // Si la connexion n'est pas limitée ou
        // si l'utilisateur veut que les données soient rafraîchies même sur un réseau limité.
        return !ConnectivityManagerCompat.isActiveNetworkMetered(connMgr)
                || preferences.getBoolean(CLE_RAFRAICHIR_LIMITE, RAFRAICHIR_LIMITE_DEFAUT);
    }

    /**
     * @return L'interval de rafraîchissement des données de serres en ms
     */
    public static int IntervalRafraichissement() {
        String cle;
        int valeurDefaut;
        if (ConnectivityManagerCompat.isActiveNetworkMetered(connMgr)) {
            cle = CLE_INTERVAL_LIMITE;
            valeurDefaut = INTERVAL_LIMITE_DEFAUT;
        } else {
            cle = CLE_INTERVAL_ILLIMITE;
            valeurDefaut = INTERVAL_ILLIMITE_DEFAUT;
        }
        return preferences.getInt(cle, valeurDefaut) * 1000;
    }

    /**
     * @return Le temps limite en ms pour se connecter à une serre
     */
    public static int TempsMort() {
        return preferences.getInt(CLE_TEMPS_MORT, TEMPS_MORT_DEFAUT) * 1000;
    }

    /**
     * @return Le mot de passe pour accéder aux serres
     */
    public static String MotDePasse() throws
            java.security.GeneralSecurityException {
        // Obtenir le mot de passe encrypté.
        String mdp = preferences.getString(CLE_MDP, null);
        if (mdp != null) {
            // Décrypter le mot de passe et le retourner.
            return Encryption.Decrypter(mdp);
        } else {
            // Retourner le mot de passe par défaut.
            return MDP_DEFAUT;
        }
    }

    /**
     * @return La clé pour le mot de passe, ou null si elle n'est pas enregistrée
     */
    public static String CleMdp() {
        return preferences.getString(CLE_CLE_MDP, null);
    }

    /**
     * Modifie la clé encryptée pour décrypter le mot de passe
     */
    public static void CleMdp(String cle) {
        preferences.edit().putString(CLE_CLE_MDP, cle).apply();
    }

    /**
     * @return L'interval de vérification des alertes des serres en secondes
     */
    public static int IntervalVerificationAlertes(Context ctx) {
        Initialiser(ctx);
        return preferences.getInt(CLE_INTERVAL_ALERTES, INTERVAL_ALERTES_DEFAUT);
    }

    public static boolean ActiverAlarmeAlertesUrgentes(Context ctx) {
        Initialiser(ctx);
        return preferences.getBoolean(CLE_ACTIVER_ALARME_ALERTES_URGENTES, true);
    }

    /**
     * Retourne la sonnerie des l'alertes urgentes
     *
     * @param ctx
     * @return Le URI de la sonnerie de l'alerte
     */
    public static Uri SonnerieAlertesUrgentes(Context ctx) {
        Initialiser(ctx);
        String sUri = preferences.getString(CLE_SONNERIE_ALERTES_URGENTES, null);
        Uri uri = null;
        if (sUri != null) {
            try {
                uri = Uri.parse(sUri);
            } catch (Exception ignored) {
            }
        }
        if (uri == null) {
            uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        }
        return uri;
    }

    /**
     * Change la sonnerie des l'alertes urgentes
     *
     * @param ctx
     * @param uri URI de la sonnerie
     * @return Le URI de la sonnerie de l'alerte
     */
    public static void SonnerieAlertesUrgentes(Context ctx, String uri) {
        Initialiser(ctx);
        preferences.edit()
                .putString(CLE_SONNERIE_ALERTES_URGENTES, uri)
                .apply();
    }

    /**
     * @param ctx
     * @return La liste de mode ne pas déranger pour lesquels l'alarme doit être activée
     */
    public static Set<String> ActiverAlarmeAlertesUrgentesDnd(Context ctx) {
        Initialiser(ctx);
        return preferences.getStringSet(
                CLE_ACTIVER_ALARME_ALERTES_URGENTES_DND,
                Collections.<String>emptySet());
    }
}
