package com.arduicole.tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.arduicole.db.Serre;

import java.io.Serializable;

public abstract class TabDetailsSerre extends Fragment {
    private static final String SERRE = "TabDetailSerre.serre";
    protected Serre serre;

    public abstract int layout();

    public void setSerre(Serre serre) {
        Bundle args = new Bundle();
        args.putSerializable(SERRE, serre);
        setArguments(args);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtenir la serre.
        Serializable s = getArguments().getSerializable(SERRE);
        if (s == null)
            throw new NullPointerException("La serre n'a pas été passée à l'onglet");
        serre = (Serre) s;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(layout(), container, false);
    }
}
