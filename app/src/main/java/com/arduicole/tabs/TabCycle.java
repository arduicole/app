package com.arduicole.tabs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.arduicole.Etape;
import com.arduicole.R;
import com.arduicole.databinding.DlgNomBinding;
import com.arduicole.databinding.FragmentTabCycleBinding;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.Cycle;
import com.arduicole.db.CycleDao;
import com.arduicole.net.TaskEnvoyerCycle;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class TabCycle extends TabDetailsSerre {
    private CycleDao cycleDao;
    private LiveData<List<Cycle>> lsCycles;
    private Cycle cycle;

    private FragmentTabCycleBinding binding;

    @Override
    public int layout() {
        return R.layout.fragment_tab_cycle;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentTabCycleBinding.bind(view);

        if (cycleDao == null) {
            // Obtenir les consignes et le consigneDao.
            AppDatabase db = AppDatabase.instance(getContext());
            cycleDao = db.cycleDao();
            lsCycles = cycleDao.toutes();
        }

        binding.btnActualiserCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cycle != null) {
                    afficher(cycle);
                }
            }
        });

        lsCycles.observe(this.getViewLifecycleOwner(), new Observer<List<Cycle>>() {
            @Override
            public void onChanged(@Nullable List<Cycle> cycles) {
                // Obtenir les noms des cycles.
                ArrayList<String> nomsCycles = new ArrayList<>();

                int selection = cycles.size();

                selection -= 1;

                if (cycles.size() != 0) {
                    for (Cycle cycle : cycles)
                        nomsCycles.add(cycle.getNom().toString());

                    if (cycle != null) {
                        // Sélectionner le cycle.
                        for (int i = 0; i < cycles.size(); i++) {
                            if (cycles.get(i).getId() == cycle.getId()) {
                                selection = i;
                                break;
                            }
                        }
                    }
                } else {
                    // Ajouter un cycle vide aux noms de cycle.
                    nomsCycles.add("<aucune>");
                }

                // Ajouter les noms de cycles au spinner.
                binding.nomsCycles.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, nomsCycles));

                // Sélectionner le cycle.
                binding.nomsCycles.setSelection(selection);

                // S'il y a des cycles, permettre l'édition des cycles.
                // Sinon l'empêcher.
                boolean etat = cycles.size() != 0;
                binding.btnSupprimerCycle.setEnabled(etat);
                binding.btnRenommerCycle.setEnabled(etat);
                binding.btnExecuterCycle.setEnabled(etat);
                binding.btnEnregistrerCycle.setEnabled(etat);
            }

        });
        binding.nomsCycles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cycle = selection();
                afficher(cycle != null ? cycle : new Cycle());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                afficher(new Cycle());
            }
        });

        binding.btnAjouterCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifierCycle(null);
            }
        });
        binding.btnRenommerCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifierCycle(cycle);
            }
        });

        binding.btnEnregistrerCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cycle.setDureeChauffage(Integer.parseInt(binding.dureeChauffage.getText().toString()));
                cycle.setDureeVentilation(Integer.parseInt(binding.dureeVentilation.getText().toString()));
                cycle.setRepetition(Integer.parseInt(binding.repetitionsCycle.getText().toString()));

                Etape etape = new Etape();
                //Si le chauffage est sélectionné
                if (binding.rgPremiereEtape.getCheckedRadioButtonId() == R.id.rb_chaffage) {
                    etape.setEtape(Etape.EtapeCycle.CHAUFFAGE.ordinal());
                }
                //si la ventilation est sélectionnée
                else {
                    etape.setEtape(Etape.EtapeCycle.VENTILATION.ordinal());
                }
                cycle.PremiereEtape = etape;

                cycleDao.actualiser(cycle);
                afficher(cycle);
                afficherToast("Cycle enregistré");
            }
        });

        binding.btnSupprimerCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dialogue de confirmation pour supprimer une serre
                AlertDialog.Builder builderSupprimer = new AlertDialog.Builder(getContext());
                builderSupprimer.setTitle("Supprimer");
                builderSupprimer.setMessage("Voulez-vous supprimer le cycle: " + cycle.getNom())
                        .setCancelable(false)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //supprimer le cycle
                                cycleDao.supprimer(cycle);
                                afficher(new Cycle());
                            }
                        })
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                // Créer et afficher le dialogue pour supprimer un cycle
                AlertDialog alertDialogSupprimer = builderSupprimer.create();
                alertDialogSupprimer.show();
            }
        });

        binding.btnExecuterCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TaskEnvoyerCycleImpl(cycle, getContext()).execute(serre);
            }
        });
    }

    //Ajouter ou modifier un cycle
    void modifierCycle(final Cycle cycleModifie) {
        Context context = getContext();

        final DlgNomBinding bindingPrompt = DlgNomBinding.inflate(LayoutInflater.from(context));
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context)
                .setView(bindingPrompt.getRoot())
                .setCancelable(false)
                .setPositiveButton("Sauvegarder", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Ceci doit être implémenté pour ne pas fermer le dialog après un clique positif
                    }
                })
                .setNegativeButton("Annuler",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // Créer et afficher le dialogue pour ajouter ou modifier une serre
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        // si un cycle est sélectionné
        if (cycleModifie != null) {
            bindingPrompt.dlgEditNom.setText(cycleModifie.getNom());
        }

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sNom = bindingPrompt.dlgEditNom.getText().toString().trim();

                //Si le nom est invalide
                if (sNom.isEmpty()) {
                    bindingPrompt.dlgEditNom.setError("Nom invalide");
                }
                //si le nom est valide
                else {
                    //Si on ajoute un cycle
                    if (cycleModifie == null) {
                        //si le nom existe
                        if (cycleDao.existeNom(sNom)) {
                            bindingPrompt.dlgEditNom.setError("Ce nom de cycle existe déjà");
                        }
                        //si le nom existe pas
                        else {
                            Cycle cycle = new Cycle();
                            cycle.setNom(sNom);
                            cycleDao.inserer(cycle);
                            afficher(cycle);
                            alertDialog.dismiss();
                        }
                    }
                    //si on modifie un cycle
                    else {
                        if (cycleDao.existeNomModifier(cycleModifie.getId(), sNom)) {
                            bindingPrompt.dlgEditNom.setError("Ce nom de cycle existe déjà");
                        } else {
                            cycleModifie.setNom(sNom);
                            cycleDao.actualiser(cycleModifie);
                            afficher(cycleModifie);
                            alertDialog.dismiss();
                        }
                    }

                }
            }
        });
    }

    /**
     * Affiche la consigne donnée dans les champs
     */
    void afficher(Cycle cycle) {
        this.cycle = cycle;
        binding.dureeChauffage.setText(Integer.toString(cycle.getDureeChauffage()));
        binding.dureeVentilation.setText(Integer.toString(cycle.getDureeVentilation()));
        binding.repetitionsCycle.setText(Integer.toString(cycle.getRepetition()));
        binding.rgPremiereEtape.check(cycle.PremiereEtape.getEtape() == Etape.EtapeCycle.CHAUFFAGE.ordinal() ? R.id.rb_chaffage : R.id.rb_ventilation);
    }

    /**
     * @return La consigne sélectionnée ou null s'il n'y en a pas
     */
    Cycle selection() {
        // Obtenir l'index du cycle.
        int pos = binding.nomsCycles.getSelectedItemPosition();

        // Si l'index fait partie des cycle, retourner le cycle
        // Sinon retourner null.
        List<Cycle> cycles = lsCycles.getValue();
        return pos >= 0 && pos < cycles.size() ? cycles.get(pos) : null;
    }

    //Afficher un message avec toast
    void afficherToast(String message) {
        Context context = getContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}

/**
 * Envoi un nouveau cycle à exécuter à la serre
 */
class TaskEnvoyerCycleImpl extends TaskEnvoyerCycle {
    private WeakReference<Context> mContext;

    TaskEnvoyerCycleImpl(Cycle cycle, Context context) {
        super(cycle);
        mContext = new WeakReference<>(context);
    }

    @Override
    protected void onPostExecute(Integer codeErreur) {
        Context context = mContext.get();
        if (context != null) {
            if (codeErreur == 204 || codeErreur == 409) {
                String message = "Le cycle est en exécution";
                if (codeErreur == 409)
                    message = "La serre est en mode manuel, elle ne peut donc pas exécuter de cycle";

                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            } else {
                new AlertDialog.Builder(context)
                        .setTitle("Erreur avec la serre")
                        .setMessage("Le cycle a été refusée par la serre. Assurez-vous " +
                                "que vous pouvez bien vous connecter à la serre et que " +
                                "le logiciel de la serre et l'application sont à jour et compatibles.")
                        .setNeutralButton("Ok", null)
                        .show();
            }
        }

        super.onPostExecute(codeErreur);
    }
}
