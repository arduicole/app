package com.arduicole.tabs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arduicole.AdapterAlertes;
import com.arduicole.R;
import com.arduicole.background.WorkerAlertes;
import com.arduicole.databinding.FragmentTabAlertesBinding;
import com.arduicole.db.Alerte;
import com.arduicole.db.AlerteDao;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.DBTask;

import java.lang.ref.WeakReference;
import java.util.List;

public class TabAlertes extends TabDetailsSerre {
    private AlerteDao alerteDao;
    private LiveData<List<Alerte>> alertes;
    private FragmentTabAlertesBinding binding;
    private AdapterAlertes adapter;

    @Override
    public int layout() {
        return R.layout.fragment_tab_alertes;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentTabAlertesBinding.bind(view);

        DividerItemDecoration divListeAlertes = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        divListeAlertes.setDrawable(getResources().getDrawable(R.drawable.divider_recyclerview));
        binding.listeAlertes.addItemDecoration(divListeAlertes);

        adapter = new AdapterAlertes(getContext());
        registerForContextMenu(binding.listeAlertes);
        binding.listeAlertes.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.listeAlertes.setAdapter(adapter);

        alerteDao = AppDatabase.instance(getContext()).alerteDao();
        alertes = alerteDao.alertesSerre(serre.getId());

        alertes.observe(this.getViewLifecycleOwner(), new Observer<List<Alerte>>() {
            @Override
            public void onChanged(@Nullable List<Alerte> alertes) {
                adapter.setAlertes(alertes);
            }
        });

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, final int direction) {
                final Alerte alerte = adapter.get(viewHolder.getAdapterPosition());

                // Créer et afficher le dialogue pour supprimer une serre.
                String message = direction == ItemTouchHelper.RIGHT ?
                        "supprimer cette alerte et toutes les alertes antérieures?" :
                        "supprimer cette alerte?";
                new AlertDialog.Builder(getContext())
                        .setTitle("Supprimer")
                        .setMessage("Êtes vous sûr de vouloir " + message)
                        .setCancelable(false)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Si on veut supprimer l'alerte
                                new TaskSupprimerAlertes(
                                        alerteDao,
                                        direction == ItemTouchHelper.RIGHT,
                                        getContext()
                                ).execute(alerte);
                            }
                        }).setNegativeButton("Non", null).show();

                // Recharger les serres pour effacer la couleur après un swipe.
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    int height = itemView.getBottom() - itemView.getTop();
                    int width = height / 3;

                    int backgroundRes;
                    RectF backgroundRect;
                    int iconeRes;
                    Rect iconeRect;

                    // S'il swipe gauche (modifier)
                    if (dX > 0) {
                        backgroundRes = R.color.colorModifierSerre;
                        backgroundRect = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());

                        iconeRes = R.drawable.ic_access_time;
                        iconeRect = new Rect(itemView.getLeft() + width, itemView.getTop() + width, itemView.getLeft() + 2 * width, itemView.getBottom() - width);
                    }
                    // S'il swipe droite (supprimer)
                    else {
                        backgroundRes = R.color.colorSupprimerSerre;
                        backgroundRect = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());

                        iconeRes = R.drawable.ic_delete;
                        iconeRect = new Rect(itemView.getRight() - 2 * width, itemView.getTop() + width, itemView.getRight() - width, itemView.getBottom() - width);
                    }

                    // Obtenir les ressources.
                    Resources resources = getContext().getResources();

                    // Afficher le background.
                    Paint paint = new Paint();
                    int couleur = ResourcesCompat.getColor(resources, backgroundRes, null);
                    paint.setColor(couleur);
                    c.drawRect(backgroundRect, paint);

                    // Afficher l'icone.
                    Drawable icone = ResourcesCompat.getDrawable(resources, iconeRes, null);
                    icone.setBounds(iconeRect);
                    icone.draw(c);

                    // Appliquer les changement.
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }
        });
        itemTouchHelper.attachToRecyclerView(binding.listeAlertes);

        binding.fabMaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkerAlertes.planifierVerificationImmediate(getContext());
            }
        });
        binding.fabMarquerLues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TaskMarquerConsultees(alerteDao, getContext()).execute(serre.getId());
            }
        });
    }
}

/**
 * Supprime une serre de la base de données
 */
class TaskSupprimerAlertes extends DBTask<Alerte, AlerteDao, Integer> {
    private WeakReference<Context> mContext;
    private boolean mSupprimerAnterieure;

    /**
     * @param alerteDao
     * @param supprimerAnterieure S'il faut supprimer toutes les alertes antérieures à celle donnée
     * @param context
     */
    TaskSupprimerAlertes(AlerteDao alerteDao, boolean supprimerAnterieure, Context context) {
        super(alerteDao);
        mContext = new WeakReference<>(context);
        mSupprimerAnterieure = supprimerAnterieure;
    }

    @Override
    protected Integer tache(Alerte alerte) {
        if (mSupprimerAnterieure) {
            mDao.supprimer(alerte.Date, alerte.Serre);
        } else {
            mDao.supprimer(alerte);
        }
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Context context = mContext.get();
        if (integer == null && context != null) {
            Toast.makeText(context,
                    mSupprimerAnterieure ? "Les alertes n'ont pas pu être supprimées" : "L'alerte n'a pas pu être supprimée",
                    Toast.LENGTH_LONG).show();
        }
    }
}

/**
 * Marque toutes les alertes d'une serre comme consultées
 */
class TaskMarquerConsultees extends DBTask<Integer, AlerteDao, Integer> {
    private WeakReference<Context> mContext;

    TaskMarquerConsultees(AlerteDao dao, Context context) {
        super(dao);
        mContext = new WeakReference<>(context);
    }

    @Override
    protected Integer tache(Integer serre) {
        mDao.toutesMarquerConsultees(serre);
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Context context = mContext.get();
        if (integer == null && context != null) {
            Toast.makeText(context,
                    "Les alertes n'ont pas toutes pues être marquées comme lues",
                    Toast.LENGTH_LONG).show();
        }
    }
}
