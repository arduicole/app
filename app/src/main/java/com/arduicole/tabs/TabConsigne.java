package com.arduicole.tabs;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.arduicole.R;
import com.arduicole.databinding.DlgNomBinding;
import com.arduicole.databinding.FragmentTabConsigneBinding;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.Consigne;
import com.arduicole.db.ConsigneDao;
import com.arduicole.db.DBTask;
import com.arduicole.db.HeuresConsigne;
import com.arduicole.db.Jour;
import com.arduicole.db.Serre;
import com.arduicole.db.SerreDao;
import com.arduicole.net.TaskEnvoyerConsigne;
import com.arduicole.net.TaskObtenirConsigne;

import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import io.ktor.client.statement.HttpResponse;

public class TabConsigne extends TabDetailsSerre {
    private ConsigneDao consigneDao;
    private SerreDao serreDao;

    // Liste complète des consignes
    private LiveData<List<Consigne>> consignes;

    // Nombre de consignes précédent.
    int prevNbConsignes = 0;

    FragmentTabConsigneBinding binding;
    EditText[][] temperatures;
    TextView[] heures;

    TaskVerifierConsigne.Callback cbVerifierConsigne = new TaskVerifierConsigne.Callback() {
        @Override
        public void onPostExecute(final Consigne consigne, final List<Consigne> consignesEquivalentes) {
            if (getContext() == null)
                return;

            if (consigne == null) {
                Toast.makeText(getContext(),
                        "La consigne de la serre n'a pas pu être obtenue",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if (consignesEquivalentes == null) {
                Toast.makeText(getContext(),
                        "Les consignes équivalentes à celle de la serre n'ont pas pues être obtenues de la base de données.",
                        Toast.LENGTH_LONG).show();
                return;
            }

            final Integer consigneSerre = serre.getConsigne();
            // Si la consigne correspond à celle de la serre,
            // ne rien faire.
            if (consigneSerre != null) {
                for (Consigne c : consignesEquivalentes) {
                    if (c.uid == consigneSerre)
                        return;
                }
            }

            // Permettre à l'utilisateur de choisir parmi les consignes équivalentes.
            // Permettre à l'utilisateur de créer une nouvelle consigne équivalente à celle de la serre.
            // Permettre à l'utilisateur de changer la consigne de la serre pour celle donnée.
            String message = "La consigne de la serre ne correspond pas à celle enregistrée dans l'application. Vous pouvez:\n" +
                    "- Créer une nouvelle consigne équivalente à celle de la serre\n";
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                    .setTitle("Consigne de la serre")
                    .setCancelable(false)
                    .setPositiveButton("Nouvelle", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            insererConsigne(consigne, false, true);
                        }
                    });
            if (consigneSerre != null) {
                message += "- Modifier la consigne actuelle pour qu'elle soit la même que celle de la serre\n";
                builder.setNegativeButton("Modifier", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new ModifierConsigneSerre(consigneDao, consigne, getContext()).execute(consigneSerre);
                    }
                });
            }
            if (consignesEquivalentes.size() > 0) {
                message += "- Choisir parmi les consignes déjà enregistrées qui correspondent à celle de la serre";
                builder.setNeutralButton("Choisir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Obtenir les noms des consignes équivalentes.
                        String[] nomsConsignes = new String[consignesEquivalentes.size()];
                        for (int i = 0; i < consignesEquivalentes.size(); i++)
                            nomsConsignes[i] = consignesEquivalentes.get(i).Nom;

                        new AlertDialog.Builder(getContext())
                                .setTitle("Choisir parmi ces serres")
                                .setCancelable(false)
                                .setItems(nomsConsignes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        final int idConsigne = consignesEquivalentes.get(which).uid;
                                        new TaskActualiserConsigneSerre(serreDao, idConsigne, getContext()).execute(serre);
                                    }
                                }).show();
                    }
                });
            }
            builder.setMessage(message).show();
        }
    };

    @Override
    public int layout() {
        return R.layout.fragment_tab_consigne;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentTabConsigneBinding.bind(view);
        temperatures = new EditText[][]{
                new EditText[]{binding.prejourSoleil, binding.jourSoleil, binding.prenuitSoleil, binding.nuitSoleil},
                new EditText[]{binding.prejourMiNuageux, binding.jourMiNuageux, binding.prenuitMiNuageux, binding.nuitMiNuageux},
                new EditText[]{binding.prejourNuageux, binding.jourNuageux, binding.prenuitNuageux, binding.nuitNuageux},
        };
        heures = new TextView[]{
                binding.prejourHeure, binding.jourHeure,
                binding.prenuitHeure, binding.nuitHeure,
        };

        if (consigneDao == null) {
            // Obtenir les consignes et le consigneDao.
            AppDatabase db = AppDatabase.instance(getContext());
            consigneDao = db.consigneDao();
            serreDao = db.serreDao();
            consignes = consigneDao.toutes();
        }

        consignes.observe(this.getViewLifecycleOwner(), new Observer<List<Consigne>>() {
            @Override
            public void onChanged(@Nullable List<Consigne> consignes) {
                // Obtenir les noms de consignes.
                ArrayList<String> nomsConsignes = new ArrayList<>();
                for (Consigne consigne : consignes)
                    nomsConsignes.add(consigne.Nom);

                // Initialiser une sélection à la sélection actuelle.
                int selection = binding.nomsConsignes.getSelectedItemPosition();
                if (selection == -1)
                    selection = 0;

                // Si une consigne a été supprimé
                if (prevNbConsignes > consignes.size()) {
                    // Sélectionner la consigne de la serre.
                    Integer consigneSerre = serre.getConsigne();
                    if (consigneSerre != null) {
                        // Sélectionner la consigne de la serre.
                        for (int i = 0; i < consignes.size(); i++) {
                            if (consignes.get(i).uid == consigneSerre) {
                                selection = i;
                                break;
                            }
                        }
                    }
                }
                // Sinon si une consigne a été ajouté
                else if (prevNbConsignes < consignes.size()) {
                    // Si plus d'une consigne a été ajouté
                    if (prevNbConsignes + 1 < consignes.size()) {
                        // Sélectionner la consigne de la serre.
                        Integer consigneSerre = serre.getConsigne();
                        if (consigneSerre != null) {
                            // Sélectionner la consigne de la serre.
                            for (int i = 0; i < consignes.size(); i++) {
                                if (consignes.get(i).uid == consigneSerre) {
                                    selection = i;
                                    break;
                                }
                            }
                        }
                    }
                    // Sinon
                    else {
                        // Sélectionner la dernière consigne.
                        selection = consignes.size() - 1;
                    }
                }
                // S'il n'y a aucune consigne
                else if (prevNbConsignes == 0) {
                    // Ajouter un nom de consigne vide.
                    nomsConsignes.add("<aucune>");
                }

                // Ajouter les noms de consignes au spinner.
                //TODO afficher la consigne qqch de spécial pour la consigne de la serre dans le spinner.
                binding.nomsConsignes.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, nomsConsignes));

                // Appliquer la sélection.
                binding.nomsConsignes.setSelection(selection);

                prevNbConsignes = consignes.size();

                // S'il y a des consignes, permettre l'édition des consignes.
                // Sinon l'empêcher.
                boolean etat = consignes.size() != 0;
                binding.btnSupprimerConsigne.setEnabled(etat);
                binding.btnModifierConsigne.setEnabled(etat);
                binding.btnAppliquerConsigne.setEnabled(etat);
            }
        });
        binding.nomsConsignes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Consigne consigne = selection();
                afficher(consigne != null ? consigne : new Consigne());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                afficher(new Consigne());
            }
        });

        binding.btnAjouterConsigne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insererConsigne(null, true, false);
            }
        });
        binding.btnEnregistrerConsigne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Consigne consigne = selection();
                if (consigne != null) {
                    if (!decoder(consigne)) {
                        new TaskActualiserConsigne(consigneDao, getContext()).execute(consigne);
                        edition(false);
                    }
                } else {
                    edition(false);
                }
            }
        });
        binding.btnSupprimerConsigne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Consigne consigne = selection();
                if (consigne != null) {
                    // Si la consigne à supprimée n'est pas celle sélectionnée
                    if (serre.getConsigne() == null || serre.getConsigne() != consigne.uid) {
                        new AlertDialog.Builder(getContext())
                                .setCancelable(true)
                                .setMessage("Êtes vous sûr de vouloir supprimer la consigne " + consigne.Nom + "?")
                                .setNegativeButton("Annuler", null)
                                .setPositiveButton("Supprimer", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new TaskSupprimerConsigne(consigneDao, getContext()).execute(consigne);
                                    }
                                }).show();
                    } else {
                        Toast.makeText(getContext(),
                                "La consigne de cette serre ne peut pas être supprimée",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        binding.btnAnnulerConsigne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edition(false);
                Consigne consigne = selection();
                if (consigne != null)
                    afficher(consigne);
            }
        });
        binding.btnModifierConsigne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Activer l'édition.
                edition(true);
            }
        });

        binding.btnAppliquerConsigne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Consigne consigne = selection();
                if (consigne != null) {
                    new TaskChangerConsigneSerre(consigne, serreDao, serre, getContext()).execute(serre);
                }
            }
        });

        // Configurer l'édition des heures des périodes.
        for (int i = 0; i < heures.length; i++) {
            final int j = i;
            TextView tvHeure = heures[i];
            tvHeure.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus)
                        return;
                    modifierHeure(j);
                }
            });
            tvHeure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    modifierHeure(j);
                }
            });
            tvHeure.setClickable(false);
            tvHeure.setFocusable(false);
            tvHeure.setFocusableInTouchMode(false);
        }

        new TaskVerifierConsigne(consigneDao, cbVerifierConsigne).execute(serre);
    }

    void modifierHeure(final int i) {
        Consigne sel = selection();
        final Consigne consigne = sel != null ? sel : new Consigne();
        final short[] sHeures = consigne.Heures.toArray();
        short heure = sHeures[i];
        TimePickerDialog dlg = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int heure, int minute) {
                sHeures[i] = HeuresConsigne.calculerHeure((short) heure, (short) minute);
                consigne.Heures = new HeuresConsigne(sHeures);
                heures[i].setText(String.format("%2d:%02d", heure, minute));
            }
        }, HeuresConsigne.heures(heure), HeuresConsigne.minutes(heure), DateFormat.is24HourFormat(getContext()));
        dlg.show();
    }

    /**
     * Affiche la consigne donnée dans les champs
     */
    void afficher(Consigne consigne) {
        Jour[] jours = consigne.toArray();
        for (int i = 0; i < jours.length; i++) {
            double[] temperatures = jours[i].temperatures;
            for (int j = 0; j < temperatures.length; j++)
                this.temperatures[i][j].setText(Double.toString(temperatures[j]));
        }

        short iHeures[] = consigne.Heures.toArray();
        for (int i = 0; i < iHeures.length; i++) {
            short heure = iHeures[i];
            heures[i].setText(String.format("%2d:%02d", HeuresConsigne.heures(heure), HeuresConsigne.minutes(heure)));
        }

        binding.variationAcceptable.setText(Double.toString(consigne.VariationAcceptable));
        binding.variationCritique.setText(Double.toString(consigne.VariationCritique));
    }

    /**
     * Décode la consigne à partir des champs
     *
     * @return true s'il y a une erreur et false sinon
     */
    boolean decoder(Consigne consigne) {
        boolean erreur = false;

        // Obtenir les consignes des champs.
        Jour[] jours = consigne.toArray();
        try {
            for (int i = 0; i < jours.length; i++) {
                double[] temperatures = jours[i].temperatures;
                for (int j = 0; j < temperatures.length; j++) {
                    double temperature = lireDouble(this.temperatures[i][j]);
                    if (Math.abs(temperature) > Double.MAX_VALUE) {
                        throw new Exception("Une des températures est invalide");
                    }
                    temperatures[j] = temperature;
                }
            }

            short[] iHeures = consigne.Heures.toArray();
            for (int i = 0; i < heures.length; i++) {
                TextView tvHeure = heures[i];
                try {
                    String[] sHeures = tvHeure.getText().toString().split(":");
                    iHeures[i] = HeuresConsigne.calculerHeure(
                            Short.parseShort(sHeures[0]), Short.parseShort(sHeures[1]));
                } catch (Exception ignored) {
                }

                if (i > 0) {
                    if (iHeures[i - 1] >= iHeures[i]) {
                        throw new Exception("Les heures des périodes de la journée doivent être croissantes");
                    }
                }
            }
            consigne.Heures = new HeuresConsigne(iHeures);

            consigne.VariationAcceptable = lireDouble(binding.variationAcceptable);
            consigne.VariationCritique = lireDouble(binding.variationCritique);
        } catch (Exception e) {
            String message;
            if (e instanceof NumberFormatException) {
                message = "Une des températures est invalide";
            } else {
                message = e.getMessage();
            }
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

            erreur = true;
        }
        return erreur;
    }

    /**
     * Affiche le dialogue pour créer une nouvelle consigne
     *
     * @param base          Consigne de base. Si elle est nulle, une nouvelle consigne est créée et on copie
     *                      ce qu'il y a dans les champs d'édition de consigne à l'intérieur.
     * @param annuler       Si le dialogue peut être annulé ou non
     * @param assignerSerre Si cette consigne doit être assigner à la serre
     */
    void insererConsigne(final Consigne base, boolean annuler, final boolean assignerSerre) {
        final DlgNomBinding bindingDlg = DlgNomBinding.inflate(LayoutInflater.from(getContext()));
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setView(bindingDlg.getRoot())
                .setCancelable(annuler)
                .setPositiveButton("Sauvegarder", null);

        // Ajouter le bouton annuler si le dialogue peut être annulé.
        if (annuler)
            builder.setNegativeButton("Annuler", null);

        // Afficher le dialogue.
        final AlertDialog dlg = builder.show();

        // Lorsque le bouton positif est appuyé.
        dlg.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Créer une nouvelle consigne avec le nom donné et les propriétés de la consigne sélectionnée.
                String nom = bindingDlg.dlgEditNom.getText().toString().trim();
                if (!nom.isEmpty()) {
                    TabConsigne tab = null;
                    // Assigner la consigne de base à la nouvelle consigne.
                    Consigne consigne = base;
                    // Si la consigne est nulle
                    if (consigne == null) {
                        // Créer une nouvelle consigne et décoder les données des champs et les mettre dedans.
                        consigne = new Consigne();
                        decoder(consigne);

                        tab = TabConsigne.this;
                    }
                    // Assigner le nom à la consigne.
                    consigne.Nom = nom;


                    new TaskInsererConsigne(consigneDao, serreDao, serre, assignerSerre,
                            getContext(), tab, dlg, bindingDlg.dlgEditNom)
                            .execute(consigne);
                } else {
                    bindingDlg.dlgEditNom.setError("Le nom de la consigne ne peut pas être vide");
                }
            }
        });
    }

    private static double lireDouble(EditText et) {
        try {
            return Double.parseDouble(et.getText().toString());
        } catch (NumberFormatException e) {
            et.setError("Le nombre est invalide");
            throw e;
        }
    }

    /**
     * @return La consigne sélectionnée ou null s'il n'y en a pas
     */
    Consigne selection() {
        // Obtenir l'index de la consigne.
        int pos = binding.nomsConsignes.getSelectedItemPosition();

        // Si l'index fait partie des consignes, retourner la consigne
        // Sinon retourner null.
        List<Consigne> _consignes = consignes.getValue();
        return pos >= 0 && pos < _consignes.size() ? _consignes.get(pos) : null;
    }

    /**
     * Active/désactive l'édition des consignes
     *
     * @param editer si la consigne est éditée ou non
     */
    void edition(boolean editer) {
        // Permettre/empêcher le focus sur les champs.
        for (EditText[] colonne : temperatures) {
            for (EditText champs : colonne) {
                champs.setFocusable(editer);
                champs.setFocusableInTouchMode(editer);
            }
        }
        binding.variationAcceptable.setFocusable(editer);
        binding.variationAcceptable.setFocusableInTouchMode(editer);
        binding.variationCritique.setFocusable(editer);
        binding.variationCritique.setFocusableInTouchMode(editer);

        // Permettre/empêcher la modification des heures.
        for (TextView tv : heures) {
            tv.setClickable(editer);
            tv.setFocusable(editer);
            tv.setFocusableInTouchMode(editer);
        }

        // Montrer/cacher les boutons.
        int vis = editer ? View.GONE : View.VISIBLE;
        binding.editionListeConsigne.setVisibility(vis);
        binding.btnAppliquerConsigne.setVisibility(vis);
        binding.editionConsigne.setVisibility(editer ? View.VISIBLE : View.GONE);

        // Activer/désactiver le spinner des consignes.
        binding.nomsConsignes.setEnabled(!editer);
    }
}

class TaskInsererConsigne extends DBTask<Consigne, ConsigneDao, Integer> {
    private SerreDao mSerreDao;
    private Serre mSerre;
    private boolean mAssignerConsigneSerre;

    private WeakReference<Context> mContext;
    private WeakReference<TabConsigne> mTab;
    private WeakReference<AlertDialog> mDlg;
    private WeakReference<TextView> mNom;

    TaskInsererConsigne(ConsigneDao consigneDao, SerreDao serreDao, Serre serre, boolean assignerConsigneSerre, Context context, TabConsigne tab, AlertDialog dlg, TextView nom) {
        super(consigneDao);
        mSerreDao = serreDao;
        mSerre = serre;
        mAssignerConsigneSerre = assignerConsigneSerre;

        mContext = new WeakReference<>(context);
        mDlg = new WeakReference<>(dlg);
        mNom = new WeakReference<>(nom);
        mTab = new WeakReference<>(tab);
    }

    @Override
    protected Integer erreur(Exception e) {
        // Si l'erreur est due au nom de la consigne qui est déjà utilisé.
        if (e instanceof SQLiteConstraintException)
            return -1;

        return super.erreur(e);
    }

    @Override
    protected Integer tache(Consigne consigne) {
        int codeErreur = 0;
        consigne.uid = (int) mDao.inserer(consigne);
        if (mAssignerConsigneSerre) {
            try {
                mSerreDao.actualiserConsigne(mSerre.getId(), consigne.uid);
                mSerre.setConsigne(consigne.uid);
            } catch (Exception e) {
                codeErreur = -2;
                Log.e("TaskInsererConsigne", "La consigne d'une serre n'a pas pu être changée", e);
            }
        }
        return codeErreur;
    }

    @Override
    protected void onPostExecute(Integer codeErreur) {
        if (codeErreur == null) {
            Context context = mContext.get();
            if (context != null)
                Toast.makeText(context,
                        "La consigne n'a pas pu être créée pour une raison inconnue",
                        Toast.LENGTH_LONG).show();
        } else if (codeErreur == -1) {
            TextView nom = mNom.get();
            if (nom != null)
                nom.setError("Une serre avec ce nom existe déjà");
        } else if (codeErreur == -2) {
            Context context = mContext.get();
            if (context != null) {
                Toast.makeText(context,
                        "La consigne de la serre n'a pas pu être changée",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            AlertDialog dlg = mDlg.get();
            TabConsigne tab = mTab.get();

            if (tab != null) {
                // Activer l'édition.
                tab.edition(true);
            }

            if (dlg != null) {
                // Fermer le dialogue.
                dlg.dismiss();
            }
        }
    }
}

/**
 * Change les propriétés de la consigne pour celles données
 */
class TaskActualiserConsigne extends DBTask<Consigne, ConsigneDao, Integer> {
    WeakReference<Context> mContext;

    TaskActualiserConsigne(ConsigneDao dao, Context context) {
        super(dao);
        mContext = new WeakReference<>(context);
    }

    @Override
    protected Integer tache(Consigne consigne) {
        mDao.actualiser(consigne);
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Context context = mContext.get();
        if (integer != 0 && context != null) {
            Toast.makeText(context,
                    "La modification faite n'a pas pu être appliquée",
                    Toast.LENGTH_LONG).show();
        }
        super.onPostExecute(integer);
    }
}

/**
 * Modifie la consigne à l'ID donné pour qu'elle soit la même que la serre.
 */
class ModifierConsigneSerre extends DBTask<Integer, ConsigneDao, Boolean> {
    Consigne mConsigne;
    WeakReference<Context> mContext;

    ModifierConsigneSerre(ConsigneDao dao, @NonNull Consigne consigne, Context context) {
        super(dao);
        mContext = new WeakReference<>(context);
        mConsigne = consigne;
    }

    @Override
    protected Boolean tache(Integer uid) {
        mConsigne.uid = uid;
        mDao.actualiser(mConsigne);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean bool) {
        Context context = mContext.get();
        if (bool == null && context != null) {
            Toast.makeText(context,
                    "La consigne n'a pas pu être modifiée. " +
                            "Veuillez la modifier manuellement.",
                    Toast.LENGTH_LONG);
        }
    }
}

/**
 * Change la consigne de la serre pour celle donnée et l'enregistre dans la base de données
 */
class TaskActualiserConsigneSerre extends DBTask<Serre, SerreDao, Integer> {
    private WeakReference<Context> mContext;
    private int mConsigneUid;

    TaskActualiserConsigneSerre(SerreDao dao, int consigneUid, Context context) {
        super(dao);
        mConsigneUid = consigneUid;
        mContext = new WeakReference<>(context);
    }

    @Override
    protected Integer tache(Serre serre) {
        mDao.actualiserConsigne(serre.getId(), mConsigneUid);
        serre.setConsigne(mConsigneUid);
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Context context = mContext.get();
        // S'il y a eu une erreur.
        if (integer == null && context != null) {
            Toast.makeText(context,
                    "La consigne de la serre n'a pas pu être changée pour celle sélectionnée." +
                            "Veuillez réessayer de la changer manuellement.",
                    Toast.LENGTH_LONG).show();
        }
    }
}

/**
 * Supprime la consigne donnée de la base de données.
 */
class TaskSupprimerConsigne extends DBTask<Consigne, ConsigneDao, Integer> {
    WeakReference<Context> mContext;

    TaskSupprimerConsigne(ConsigneDao dao, Context context) {
        super(dao);
        mContext = new WeakReference<>(context);
    }

    @Override
    protected Integer erreur(Exception e) {
        // Si l'erreur est due au fait que la consigne à supprimer est utilisée par une serre
        if (e instanceof SQLiteConstraintException) {
            // Retourner le code de consigne utilisée par une serre.
            return 1;
        }

        return super.erreur(e);
    }

    @Override
    protected Integer tache(Consigne consigne) {
        mDao.supprimer(consigne);
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Context context = mContext.get();
        if (context != null) {
            if (integer == null) {
                Toast.makeText(context,
                        "La consigne n'a pas pu être supprimée pour une raison inconnue",
                        Toast.LENGTH_LONG).show();
            } else if (integer == 1) {
                //TODO Afficher un dialogue avec les noms de serres en conflit.
                Toast.makeText(context,
                        "La consigne est utilisée par une serre, elle ne peut donc pas être supprimée",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}

/**
 * Obtient la consigne de la serre pour s'assurer qu'elle correspond à la consigne de la serre dans la base de données
 */
class TaskVerifierConsigne extends TaskObtenirConsigne {
    ConsigneDao mConsigneDao;
    WeakReference<Callback> mCb;

    List<Consigne> mConsignesEquivalentes;

    public TaskVerifierConsigne(ConsigneDao consigneDao, Callback cb) {
        mConsigneDao = consigneDao;
        mCb = new WeakReference<>(cb);

        mConsignesEquivalentes = null;
    }

    @Override
    protected Consigne doInBackground(Serre... serres) {
        Consigne consigne = super.doInBackground(serres);

        // Obtenir les consignes équivalentes à la consigne donnée.
        if (consigne != null) {
            try {
                mConsignesEquivalentes = mConsigneDao.consignesEquivalentes(consigne);
            } catch (Exception e) {
                Log.e("TaskVerifierConsigne", "Impossible d'obtenir les consignes équivalentes à celle de la serre dans la base de données", e);
            }
        }

        return consigne;
    }

    @Override
    protected void onPostExecute(final Consigne consigne) {
        Callback cb = mCb.get();
        if (cb != null) {
            cb.onPostExecute(consigne, mConsignesEquivalentes);
        }
    }

    interface Callback {
        void onPostExecute(Consigne consigne, List<Consigne> consignesEquivalentes);
    }
}

/**
 * Envoi une consigne à une serre puis change la consigne de la serre donnée si l'envoi a fonctionné
 */
class TaskChangerConsigneSerre extends TaskEnvoyerConsigne {
    WeakReference<Context> mContext;
    SerreDao mDao;
    Serre mSerre;

    public TaskChangerConsigneSerre(Consigne consigne, SerreDao dao, Serre serre, Context context) {
        super(consigne);
        mDao = dao;
        mSerre = serre;
        mContext = new WeakReference<>(context);
    }

    @Override
    protected Integer reponse(Serre serre, HttpResponse reponse) throws Exception {
        Integer codeErreur = super.reponse(serre, reponse);

        if (codeErreur == HttpURLConnection.HTTP_NO_CONTENT) {
            try {
                mDao.actualiserConsigne(serre.getId(), consigne.uid);
            } catch (Exception e) {
                codeErreur = -1;
            }
        }

        return codeErreur;
    }

    @Override
    protected void onPostExecute(Integer codeErreur) {
        Context context = mContext.get();

        if (context != null) {
            String titre = null;
            String message = null;

            if (codeErreur == null) { // Erreur de réseau
                titre = "Erreur de réseau";
                message = "Une connexion à la serre n'a pas pu être établie. " +
                        "Veuillez vous assurez de pouvoir vous connecter à la serre avant de réessayer.";
            } else if (codeErreur == HttpURLConnection.HTTP_NO_CONTENT) { // Aucune erreur
                mSerre.setConsigne(consigne.uid);
                Toast.makeText(context,
                        "La consigne a été appliquée",
                        Toast.LENGTH_LONG).show();
            } else if (codeErreur == -1) { // Erreur de base de données
                titre = "Erreur de base de données interne";
                message = "La consigne a été enregistrée par la serre, mais pas dans " +
                        "la base de données locale. Veuillez s'il vous plait réessayer.";
            } else { // Refus de la serre
                titre = "Refus de la serre";
                message = "La consigne a été refusée par la serre. " +
                        "Assurez-vous que le logiciel de la serre et l'application sont à jour et compatibles.";
            }

            if (titre != null) {
                new AlertDialog.Builder(context)
                        .setTitle(titre)
                        .setMessage(message)
                        .setNeutralButton("Ok", null)
                        .show();
            }
        }
    }
}
