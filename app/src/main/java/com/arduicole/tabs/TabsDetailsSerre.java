package com.arduicole.tabs;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.arduicole.R;
import com.arduicole.db.Serre;

public class TabsDetailsSerre extends FragmentStatePagerAdapter {
    @StringRes
    private static final int[] nomsTabs = {
            R.string.tab_general,
            R.string.tab_consigne,
            R.string.tab_cycle,
            R.string.tab_alertes,
    };
    private Context mContext;
    private Serre mSerre;

    public TabsDetailsSerre(Context context, FragmentManager fm, Serre serre) {
        super(fm);
        mContext = context;
        mSerre = serre;
    }

    @Override
    public int getCount() {
        return nomsTabs.length;
    }

    @Override
    public Fragment getItem(int i) {
        TabDetailsSerre tab = null;
        switch (i) {
            case 0:
                tab = new TabGeneral();
                break;
            case 1:
                tab = new TabConsigne();
                break;
            case 2:
                tab = new TabCycle();
                break;
            case 3:
                tab = new TabAlertes();
                break;
        }
        if (tab != null)
            tab.setSerre(mSerre);
        return tab;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(nomsTabs[position]);
    }
}
