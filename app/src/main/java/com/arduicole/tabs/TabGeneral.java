package com.arduicole.tabs;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;

import com.arduicole.Capteur;
import com.arduicole.CapteursActivity;
import com.arduicole.CycleExecute;
import com.arduicole.LinearList;
import com.arduicole.R;
import com.arduicole.SommaireMesures;
import com.arduicole.actionneur.Actionneur;
import com.arduicole.actionneur.AdapterActionneur;
import com.arduicole.ctrl_distance.CtrlDistanceActivity;
import com.arduicole.databinding.FragmentTabGeneralBinding;
import com.arduicole.db.AppDatabase;
import com.arduicole.db.Consigne;
import com.arduicole.db.Mode;
import com.arduicole.db.Serre;
import com.arduicole.db.SerreDao;
import com.arduicole.db.TypeJour;
import com.arduicole.net.TaskActionneurs;
import com.arduicole.net.TaskArreterCycle;
import com.arduicole.net.TaskCapteurs;
import com.arduicole.net.TaskEnvoyerTypeJour;
import com.arduicole.net.TaskMode;
import com.arduicole.net.TaskObtenirCycle;
import com.arduicole.net.TaskObtenirTypeJour;
import com.arduicole.parametres.Parametres;

import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;

import io.ktor.client.statement.HttpResponse;

public class TabGeneral extends TabDetailsSerre {
    private FragmentTabGeneralBinding binding;
    private ImageButton[] boutonsJours = new ImageButton[3];

    Handler handler;

    SerreDao dao;

    TypeJour jourSelectionne;

    // Consigne de la serre.
    Consigne consigneSerre;

    @Override
    public int layout() {
        return R.layout.fragment_tab_general;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentTabGeneralBinding.bind(view);

        boutonsJours = new ImageButton[]{
                binding.imageSoleil,
                binding.imageMinuageux,
                binding.imageNuageux,
        };

        // Obtenir le dao de serre, si on ne l'a pas déjà.
        if (dao == null)
            dao = AppDatabase.instance(getContext()).serreDao();

        // Afficher la consigne de température.
        dao.consigne(serre.getId()).observe(this.getViewLifecycleOwner(), new Observer<Consigne>() {
            @Override
            public void onChanged(@Nullable Consigne consigne) {
                consigneSerre = consigne;
                afficherConsigne();
            }
        });

        // Afficher le type de jour.
        for (int i = 0; i < boutonsJours.length; i++) {
            final int j = i;
            boutonsJours[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    envoyerTypeJour(TypeJour.fromInt(j));
                }
            });
        }
        selectionnerTypeJour(serre.TypeJour);

        // Obtenir le type de jour sélectionné dans la serre.
        new TaskObtenirEnregistrerTypeJour(this, dao).execute(serre);

        // Appliquer le type de jour à toutes les serres lorsque le bouton est appuyé.
        binding.appliquerToutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AppliquerTypeJourToutes(getContext(), dao, jourSelectionne)
                        .execute(serre);
            }
        });

        binding.btnDetailsTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Détails de capteur de température.
                detailsCapteur(Capteur.Unite.Celsius);
            }
        });
        binding.btnDetailsHumid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Détails de capteur d'humidité.
                detailsCapteur(Capteur.Unite.PourcentageHumiditeRelative);
            }
        });

        binding.btnCtrlDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Passer le type de capteur et la serre à l'activité de capteur.
                Intent intent = new Intent(getContext(), CtrlDistanceActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(CtrlDistanceActivity.SERRE_EXTRA, serre);
                startActivity(intent);
            }
        });

        binding.btnArreterCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TaskArreterCycleGeneral(getContext(), binding.btnArreterCycle)
                        .execute(serre);
            }
        });

        binding.fabRafraichir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rafraichirPeriodiquement();
            }
        });
    }

    @Override
    public void onResume() {
        // Planifier les fonctions périodiques.
        rafraichirPeriodiquement();

        super.onResume();
    }

    @Override
    public void onPause() {
        // Arrêter le rafraichissement automatique.
        if (handler != null)
            handler.removeCallbacksAndMessages(null);

        super.onPause();
    }

    public void detailsCapteur(Capteur.Unite unite) {
        // Passer le type de capteur et la serre à l'activité de capteur.
        Intent intent = new Intent(getContext(), CapteursActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(CapteursActivity.SERRE, serre);
        intent.putExtra(CapteursActivity.UNITE, unite);
        startActivity(intent);
    }

    void selectionnerTypeJour(TypeJour tj) {
        for (int i = 0; i < 3; i++) {
            ImageButton bouton = boutonsJours[i];
            // Activer le bouton si ce n'est pas le type sélectionné, sinon le désactiver.
            bouton.setEnabled(tj.getValue() != i);
        }
        jourSelectionne = tj;
    }

    void envoyerTypeJour(final TypeJour tj) {
        selectionnerTypeJour(tj);
        new TaskEnvoyerEnregistrerTypeJour(dao, getContext(), this, tj)
                .execute(serre);
    }

    void afficherConsigne() {
        String text = consigneSerre != null ? Capteur.FormatterMesure(
                consigneSerre.temperatureActuelle(serre.TypeJour),
                Capteur.Unite.Celsius
        ) : "!";
        binding.consigneTemperatureActuelle.setText(text);
    }

    /**
     * Rafraîchi périodiquement les données de la serre dans l'interface
     */
    void rafraichirPeriodiquement() {
        rafraichir();

        if (handler != null) {
            // Arrêter le rafraichissement automatique planifié.
            handler.removeCallbacksAndMessages(null);
        } else {
            handler = new Handler();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Initialiser un interval à 30s.
                int interval = 30_000;

                // S'il faut rafraîchir les données périodiquement
                if (Parametres.Rafraichir()) {
                    rafraichir();

                    // Obtenir l'interval.
                    interval = Parametres.IntervalRafraichissement();
                }

                // Répéter la tache après l'interval.
                handler.postDelayed(this, interval);
            }
        }, Parametres.IntervalRafraichissement());
    }

    /**
     * Obtient les données de capteurs, d'actionneurs et de cycle de la serre et les affiche
     */
    void rafraichir() {
        // Obtenir et afficher le mode d'exécution.
        new TaskModeGeneral(binding.mode).execute(serre);

        // Obtenir et afficher l'état des capteurs.
        new TaskCapteursGeneral(binding.mesureTemperature, binding.mesureHumidite, binding.mesureHumiditeProgression)
                .execute(serre);

        // Obtenir et afficher les actionneurs.
        new TaskActionneursGeneral(getContext(), binding.listeActionneurs)
                .execute(serre);

        // Obtenir et afficher le cycle en exécution
        new TaskCycleGeneral(binding.afficherEtatCycle, binding.afficherChauffage,
                binding.afficherVentilation, binding.afficherRepetition,
                binding.btnArreterCycle).execute(serre);

        afficherConsigne();
    }
}

/**
 * Obtient et affiche le mode d'exécution
 */
class TaskModeGeneral extends TaskMode {
    private static final String[] NOMS_MODES = {
            "Manuel",
            "Calibration",
            "Contrôle à distance",
            "Cycle",
            "Asservissement",
    };

    WeakReference<TextView> mMode;

    TaskModeGeneral(TextView mode) {
        mMode = new WeakReference<>(mode);
    }

    @Override
    protected void onPostExecute(Mode mode) {
        TextView tvMode = mMode.get();
        if (tvMode != null) {
            if (mode != null) {
                tvMode.setText(NOMS_MODES[mode.getValue()]);
            } else {
                tvMode.setText("!");
            }
        }
    }
}

/**
 * Obtient et affiche l'état des actionneurs
 */
class TaskActionneursGeneral extends TaskActionneurs {
    WeakReference<Context> mContext;
    WeakReference<LinearList> mListe;

    TaskActionneursGeneral(Context context, LinearList liste) {
        mContext = new WeakReference<>(context);
        mListe = new WeakReference<>(liste);
    }

    @Override
    protected void onPostExecute(Actionneur[] actionneurs) {
        Context context = mContext.get();
        LinearList liste = mListe.get();
        if (liste != null && context != null) {
            AdapterActionneur adapter = null;
            if (actionneurs != null) {
                ArrayList<Actionneur> alActionneurs = new ArrayList<>(Arrays.asList(actionneurs));
                adapter = new AdapterActionneur(context, alActionneurs);
            }
            liste.setAdapter(adapter);
        }
    }
}

/**
 * Obtient et affiche les données des capteurs
 */
class TaskCapteursGeneral extends TaskCapteurs {
    WeakReference<TextView> mTemperature;
    WeakReference<TextView> mTxtHumidite;
    WeakReference<ProgressBar> mProgressHumidite;

    TaskCapteursGeneral(TextView temperature, TextView txtHumidite, ProgressBar progressHumidite) {
        mTemperature = new WeakReference<>(temperature);
        mTxtHumidite = new WeakReference<>(txtHumidite);
        mProgressHumidite = new WeakReference<>(progressHumidite);
    }

    @Override
    protected void onPostExecute(Capteur[] capteurs) {
        TextView temperature = mTemperature.get();
        TextView txtHumidite = mTxtHumidite.get();
        ProgressBar progressHumidite = mProgressHumidite.get();
        if (temperature != null && txtHumidite != null && progressHumidite != null) {
            afficherMoyenne(temperature, capteurs, Capteur.Unite.Celsius);

            double humidite = afficherMoyenne(txtHumidite, capteurs, Capteur.Unite.PourcentageHumiditeRelative);
            if (humidite < 0.0)
                humidite = 0.0;
            else if (humidite > 100.0)
                humidite = 100.0;
            progressHumidite.setProgress((int) humidite);
        }
    }

    /**
     * Affiche la moyenne des capteurs pour l'unité donné dans tv
     * ou ! si capteurs est nul.
     *
     * @return La moyenne des capteurs pour l'unité donnée
     */
    private static double afficherMoyenne(TextView tv, Capteur[] capteurs, Capteur.Unite unite) {
        SommaireMesures sommaireMesures = new SommaireMesures(capteurs, unite);

        tv.setText(Capteur.FormatterMesure(sommaireMesures.Moyenne, unite));
        tv.setError(sommaireMesures.messageErreur());
        return sommaireMesures.Moyenne;
    }
}

/**
 * Obtient les données sur l'exécution du cycle dans la serre
 */
class TaskCycleGeneral extends TaskObtenirCycle {
    private WeakReference<TextView> EtatCycle;
    private WeakReference<TextView> Chauffage;
    private WeakReference<TextView> Ventilation;
    private WeakReference<TextView> Repetition;
    private WeakReference<Button> ArreterCycle;

    TaskCycleGeneral(TextView etatCycle, TextView chauffage, TextView ventilation, TextView repetition, Button arreterCycle) {
        EtatCycle = new WeakReference<>(etatCycle);
        Chauffage = new WeakReference<>(chauffage);
        Ventilation = new WeakReference<>(ventilation);
        Repetition = new WeakReference<>(repetition);
        ArreterCycle = new WeakReference<>(arreterCycle);
    }

    @Override
    protected void onPostExecute(CycleExecute cycleExecute) {
        TextView etatCycle = EtatCycle.get();
        TextView chauffage = Chauffage.get();
        TextView ventilation = Ventilation.get();
        TextView repetition = Repetition.get();
        TextView arreterCycle = ArreterCycle.get();
        if (etatCycle != null && chauffage != null && ventilation != null &&
                repetition != null && arreterCycle != null) {
            String txtEtatCycle = "!";
            String txtChauffage = "!";
            String txtVentilation = "!";
            String txtRepetition = "!";

            if (cycleExecute != null) {
                txtEtatCycle = cycleExecute.progression.getEtape();
                txtChauffage = CycleExecute.FormatterChauffage(cycleExecute.progression.getDureeChauffage(), cycleExecute.cycle.getDureeChauffage());
                txtVentilation = CycleExecute.FormatterVentilation(cycleExecute.progression.getDureeVentilation(), cycleExecute.cycle.getDureeVentilation());
                txtRepetition = CycleExecute.FormatterRepetition(cycleExecute.progression.getRepetition(), cycleExecute.cycle.getRepetition());
                arreterCycle.setEnabled(true);
            }

            etatCycle.setText(txtEtatCycle);
            chauffage.setText(txtChauffage);
            ventilation.setText(txtVentilation);
            repetition.setText(txtRepetition);
        }
    }
}

/**
 * Obtient le type de jour de la serre, l'enregistre dans la base de données et l'affiche.
 */
class TaskObtenirEnregistrerTypeJour extends TaskObtenirTypeJour {
    WeakReference<TabGeneral> mTab;
    SerreDao mDao;

    TaskObtenirEnregistrerTypeJour(TabGeneral tab, SerreDao dao) {
        mTab = new WeakReference<>(tab);
        mDao = dao;
    }

    @Override
    protected TypeJour reponse(Serre serre, HttpURLConnection connexion) throws Exception {
        serre.TypeJour = super.reponse(serre, connexion);
        mDao.actualiserTypeJour(serre.getId(), serre.TypeJour);
        return serre.TypeJour;
    }

    @Override
    protected void onPostExecute(TypeJour tj) {
        if (tj != null) {
            TabGeneral tab = mTab.get();
            if (tab != null) {
                tab.selectionnerTypeJour(tj);
            }
        }
        //else Ne rien faire, car d'autres indices montrent déjà que la serre est déconnectée.
    }
}

/**
 * Envoie le type de jour à la serre et l'enregistre si l'envoi à réussi
 */
class TaskEnvoyerEnregistrerTypeJour extends TaskEnvoyerTypeJour {
    SerreDao mDao;
    WeakReference<Context> mContext;
    WeakReference<TabGeneral> mTab;

    public TaskEnvoyerEnregistrerTypeJour(SerreDao dao, Context context, TabGeneral tab, @NonNull TypeJour tj) {
        super(tj);
        mDao = dao;
        mContext = new WeakReference<>(context);
        mTab = new WeakReference<>(tab);
    }

    @Override
    public Integer doInBackground(Serre... serres) {
        return super.doInBackground(serres);
    }

    @Override
    protected Integer onException(Throwable e) {
        super.onException(e);
        return 1;
    }

    @Override
    protected Integer reponse(Serre serre, HttpResponse reponse) throws Exception {
        // Obtenir la réponse de la serre.
        int resultat = super.reponse(serre, reponse);

        // Enregistrer le type de jour pour la serre.
        mDao.actualiserTypeJour(serre.getId(), typeJour);

        // Retourner le résultat de la réponse.
        return resultat;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        TabGeneral tab = mTab.get();
        Context context = mContext.get();
        if (tab != null && context != null) {
            if (integer == null) {
                Toast.makeText(context,
                        "Le type de jour a pu être envoyé à la serre, mais il n'a " +
                                "pas pu être enregistré dans la base de données interne." +
                                "Veuillez s'il vous plaît réessayer.",
                        Toast.LENGTH_LONG).show();
                // Sélectionner l'ancien type de jour.
                tab.selectionnerTypeJour(tab.serre.TypeJour);
            } else if (integer == 1) {
                Toast.makeText(context,
                        "Le type de jour n'a pas pu être envoyé à la serre",
                        Toast.LENGTH_LONG).show();
                // Sélectionner l'ancien type de jour.
                tab.selectionnerTypeJour(tab.serre.TypeJour);
            } else {
                // Assigner le type de jour à celui de la serre.
                tab.serre.TypeJour = typeJour;
            }
        }
    }
}

/**
 * Arrête le cycle en cours dans la serre
 */
class TaskArreterCycleGeneral extends TaskArreterCycle {
    WeakReference<Context> mContext;
    WeakReference<Button> mBtnArreterCycle;

    TaskArreterCycleGeneral(Context context, Button btnArreterCycle) {
        mContext = new WeakReference<>(context);
        mBtnArreterCycle = new WeakReference<>(btnArreterCycle);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        Context context = mContext.get();
        if (context != null) {
            if (aBoolean != null) {
                Button btnArreterCycle = mBtnArreterCycle.get();
                if (btnArreterCycle != null) {
                    String message;
                    if (aBoolean)
                        message = "Le cycle a été arrêté";
                    else
                        message = "Aucun cycle n'est en exécution";

                    Toast.makeText(context,
                            message,
                            Toast.LENGTH_LONG).show();
                    btnArreterCycle.setEnabled(!aBoolean);
                }
            } else {
                new AlertDialog.Builder(context)
                        .setTitle("Erreur avec la serre")
                        .setMessage("Le cycle a été refusée par la serre. Assurez-vous " +
                                "que vous pouvez bien vous connecter à la serre et que " +
                                "le logiciel de la serre et l'application sont à jour et compatibles.")
                        .setCancelable(true)
                        .setNeutralButton("Ok", null)
                        .show();
            }
        }
    }
}

/**
 * Applique le type de jour toutesLive les serres
 */
class AppliquerTypeJourToutes extends AsyncTask<Serre, Void, ArrayList<AppliquerTypeJourToutes.Resultat>> {
    WeakReference<Context> mContext;
    SerreDao mDao;
    TypeJour mTypeJour;

    public AppliquerTypeJourToutes(Context context, SerreDao dao, @NonNull TypeJour tj) {
        mContext = new WeakReference<>(context);
        mDao = dao;
        mTypeJour = tj;
    }

    @Override
    protected ArrayList<Resultat> doInBackground(Serre... serres) {
        TaskEnvoyerEnregistrerTypeJour tache = new TaskEnvoyerEnregistrerTypeJour(mDao, null, null, mTypeJour);
        ArrayList<Resultat> resultats = new ArrayList<>();
        for (Serre serre : mDao.toutes()) {
            Resultat resultat = new Resultat();

            resultat.serre = serre;
            resultat.codeErreur = tache.doInBackground(serre);

            resultats.add(resultat);
        }
        return resultats;
    }

    @Override
    protected void onPostExecute(ArrayList<Resultat> resultats) {
        Context context = mContext.get();
        if (context != null) {
            for (Resultat resultat : resultats) {
                if (resultat.codeErreur == null) {
                    Toast.makeText(context,
                            "Le type de jour a pu être envoyé à la serre, " + resultat.serre.getNom() +
                                    ", mais il n'a pas pu être enregistré dans la base de données interne." +
                                    "Veuillez s'il vous plaît réessayer.",
                            Toast.LENGTH_LONG).show();
                } else if (resultat.codeErreur == 1) {
                    Toast.makeText(context,
                            "Le type de jour n'a pas pu être envoyé à la serre " + resultat.serre.getNom(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    class Resultat {
        public Serre serre;
        public Integer codeErreur;
    }
}
