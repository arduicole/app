package com.arduicole.actionneur;

public abstract class Actionneur {
    protected String nom;

    public abstract String etat();

    public abstract String type();

    public String getNom() {
        return nom;
    }

    public Actionneur(String nom) {
        this.nom = nom;
    }
}
