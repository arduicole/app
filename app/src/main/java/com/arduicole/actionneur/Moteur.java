package com.arduicole.actionneur;

import java.security.InvalidParameterException;

public class Moteur extends Actionneur {
    // Position du moteur en pourcentage.
    private double position;
    // Etat du moteur.
    public Etat etat;

    public double getPosition() {
        return position;
    }

    public Moteur(String nom, Etat etat, double position) {
        super(nom);
        this.etat = etat;
        if (position >= 0.0 && position <= 100.0)
            this.position = position;
        else
            throw new InvalidParameterException("Le propriété position d'un moteur doit être entre 0.0 et 100.0");
    }

    @Override
    public String type() {
        return "moteur";
    }

    @Override
    public String etat() {
        return String.format("%s, %.0f%%", etat.toString(), position);
    }

    public enum Etat {
        Neutre(0),
        Ouvrir(1),
        Fermer(2);
        private static final String[] ETATS = {
                "Neutre",
                "Ouverture",
                "Fermeture",
        };

        private final int value;

        public int getValue() {
            return value;
        }

        private Etat(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return ETATS[value];
        }
    }
}
