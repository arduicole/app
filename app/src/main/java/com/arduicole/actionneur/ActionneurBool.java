package com.arduicole.actionneur;

public class ActionneurBool extends Actionneur {
    private String type;
    public boolean etat;

    public ActionneurBool(String nom, String type, boolean etat) {
        super(nom);
        this.etat = etat;
        this.type = type;
    }

    @Override
    public String etat() {
        return etat ? "Allumé" : "Éteint";
    }

    @Override
    public String type() {
        return type;
    }
}
