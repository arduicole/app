package com.arduicole.actionneur;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.arduicole.databinding.FragmentActionneurBinding;

import java.util.ArrayList;

public class AdapterActionneur extends ArrayAdapter<Actionneur> {
    private LayoutInflater mInflater;

    public AdapterActionneur(Context context, ArrayList<Actionneur> capteurs) {
        super(context, 0, capteurs);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        FragmentActionneurBinding binding;
        if (convertView == null) {
            binding = FragmentActionneurBinding.inflate(mInflater);
            convertView = binding.getRoot();
        } else {
            binding = FragmentActionneurBinding.bind(convertView);
        }

        // Obtenir l'actionneur.
        Actionneur actionneur = this.getItem(position);

        // Assigner les données de l'actionneur au layout.
        binding.nomActionneur.setText(actionneur.getNom());
        binding.etatActionneur.setText(actionneur.etat());

        return convertView;
    }
}
