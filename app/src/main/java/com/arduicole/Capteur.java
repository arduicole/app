package com.arduicole;

import java.text.NumberFormat;
import java.util.Locale;

public class Capteur {
    public String nom;
    public double mesure;
    public Unite unite;

    public Capteur(String nom, double mesure, Unite unite) {
        this.nom = nom;
        this.mesure = mesure;
        this.unite = unite;
    }

    /**
     * Formatte la mesure et l'unité dans une chaîne de caractères de ce format "%.1f%s".
     * Si la mesure n'est pas un nombre fini, {@code null} est retourné.
     *
     * @return La mesure formattée
     */
    public static String FormatterMesure(double mesure, Unite unite) {
        String resultat = null;

        if (Math.abs(mesure) <= Double.MAX_VALUE) {
            NumberFormat fmt = NumberFormat.getInstance();
            fmt.setMaximumFractionDigits(1);
            fmt.setMinimumFractionDigits(1);
            resultat = fmt.format(mesure) + unite.toString();
        }

        return resultat;
    }

    public enum Unite {
        Celsius(0),
        PourcentageHumiditeRelative(1);

        private static final String[] UNITES = {"°C", "%"};

        private int value;

        public int getValue() {
            return value;
        }

        private Unite(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return UNITES[value];
        }
    }
}
