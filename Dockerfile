# https://gitlab.com/gitlab-org/project-templates/android/blob/master/Dockerfile
# This Dockerfile creates a static build image for CI

FROM openjdk:8-jdk

# Just matched `app/build.gradle`
ENV ANDROID_COMPILE_SDK "28"
# Just matched `app/build.gradle`
ENV ANDROID_BUILD_TOOLS "28.0.3"

ENV ANDROID_HOME /android-sdk-linux
ENV ANDROID_SDK_ROOT /android-sdk-linux
ENV PATH="${PATH}:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/cmdline-tools/tools/bin"

# Install OS packages
RUN apt-get --quiet update --yes && apt-get -q install -y \
    wget \
    apt-utils \
    tar \
    unzip \
    lib32stdc++6 \
    lib32z1 \
    build-essential \
    ruby \
    ruby-dev \
    vim-common # We use this for xxd hex->binary

# Install Android SDK
# See https://developer.android.com/studio#cmdline-tools to get the latest version
RUN wget -q -Ocmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-6514223_latest.zip
RUN unzip cmdline-tools.zip && \
    rm cmdline-tools.zip && \
    mkdir -p ${ANDROID_HOME}/cmdline-tools/ && \
    mv tools ${ANDROID_HOME}/cmdline-tools/
RUN yes | sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}"
RUN yes | sdkmanager platform-tools
RUN yes | sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}"
RUN yes | sdkmanager "extras;google;m2repository"
RUN yes | sdkmanager "extras;google;google_play_services"
RUN yes | sdkmanager "extras;android;m2repository"
